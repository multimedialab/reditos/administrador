import React from 'react'
import "../AdminCategorias/Categorias/styleAdmin.css";

export default function Welcome() {
    return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center',width: '100%', height: '100%'}}>
            <div>
                <h2 id="title">
                    Bienvenido Admin!
                </h2>
            </div>
        </div>
    )
};


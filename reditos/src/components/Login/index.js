import React, { useState } from "react";
import "./styleLogin.css";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

import Swal from "sweetalert2";

import { signIn } from "../AdminCategorias/Services/login";

const Login = (props) => {
  const initialState = {
    email: "",
    password: "",
  };

  const [values, setValues] = useState(initialState);
  const [auth, setAuth] = useState(false);
  const [respLogin, setRespLogin] = useState([]);

  const handleOnChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = () => {};

  const go = async () => {
    const resp = await signIn(values.email, values.password);

    setRespLogin(resp);

    if (resp !== undefined) {
      if (resp.length !== 0 && resp[0].rol === 0) {
        sessionStorage.setItem("user", true);
        props.history.push("/dashboard/welcome");
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Usuario y/o contraseña incorrectos!",
        });
      }
    }
  };

  return (
    <div className="containerHigh">
      <div className="backgroundImgLogin">
        <div className="containerLogin">
          <div className="Fondo">
            <img
              src={require("../../assets/1x/titleLogin.png")}
              className="titleLogin"
            ></img>
            <div className="formularioLogin">
              <Form>
                <Form.Group controlId="formEmail" className="formGroup">
                  <img
                    src={require("../../assets/1x/iconoUsuario.png")}
                    className="icon"
                  ></img>
                  <Form.Control
                    name="email"
                    onChange={handleOnChange}
                    type="email"
                    id="label"
                  />
                  <Form.Label className="lbUsuario">Usuario</Form.Label>
                </Form.Group>
                <Form.Group controlId="formPassword" className="formGroup">
                  <img
                    src={require("../../assets/1x/iconoContraseña.png")}
                    className="icon"
                  ></img>
                  <Form.Control
                    name="password"
                    onChange={handleOnChange}
                    type="password"
                    id="label"
                  />
                  <Form.Label className="lbUsuario">Contraseña</Form.Label>
                </Form.Group>
                <Form.Text className="forgetPassword">
                  <Link to="/forget" className="txtForget">
                    <i>¿Olvidaste tu contraseña?</i>{" "}
                  </Link>
                </Form.Text>

                {/* {values.email == 'reditosadmin@reditos.com' && values.password == 'admin1234' ? 
                                            
                                            //setAuth(true)
                                            go()
                                     : null
                                        
                                    } */}

                <Button onClick={go} className="btnIngresar">
                  Ingresar
                </Button>

                {/* <Link to='/dashboard/administrar-categorias' className='txtIngresar'>
                                        </Link> */}
              </Form>
            </div>
          </div>
          <div className="logo2">
            <img
              src={require("../../assets/LogosWhite.png")}
              className="logos"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;

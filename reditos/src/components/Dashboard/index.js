import React, { useState } from "react";
import "./styleDashboard.css";
import Menu from "../Menu/index";
import SwipeableTemporaryDrawer from "../Drawer/DrawerMenu";
import { SwipeableDrawer } from "@material-ui/core";
import { CSSTransition } from "react-transition-group";
import Dinamic from "../Navbar/Dinamic";

function Dashboard(props) {
  const [state, setState] = useState(false);

  return (
    <>
      <div className="backgroundImgDashboard">
        <div className="row fila">
          <div className="col-xl-11 col-lg-11 col-md-11 col-sm-11 col-xs-11 opciones">
            {props.children}
          </div>
          <div className="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 mt-2">
            <div className="row">
              <div className="col">
                <Dinamic  />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Dashboard;

import React from "react";
import { Route, Redirect } from "react-router-dom";
import Dashboard from "./Dashboard";

export default function ProtectedRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) =>
        sessionStorage.getItem("user") ? (
          <Dashboard>
            <Component {...props} />
          </Dashboard>
        ) : (
          <Redirect to="/" />
        )
      }
    />
  );
}

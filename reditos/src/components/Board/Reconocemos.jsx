import React, { useState, useEffect } from "react";
import "./board-style.scss";

import CheckIcon from "../../assets/icons-board/CheckIcon.png";
import HandIcon from "../../assets/icons-board/HandIcon.png";
import GiftIcon from "../../assets/icons-board/GiftIcon.png";

import { Modal, Button, Form } from "react-bootstrap";

import ImageUploading from "react-images-uploading";

import Pagination from "react-js-pagination";


import {
  createWeRecognizeCard,
  getWeRecognizeCard,
  deleteWeRecognizeCard,
  updateWeRecognizeCard
} from "./services/apiServices";

const Reconocemos = () => {

  const [page, setPage] = useState(0);
  const [countTotal, setCountTotal] = useState(0);

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const [isUpdate, setIsUpdate] = useState(false);

  const [cardName, setCardName] = useState("");
  const [cardEmoji, setCardEmoji] = useState("");

  const [cards, setCards] = useState([]);
  const [idCard, setIdCard] = useState(0);


  const [images, setImages] = useState([]);
  const maxNumber = 69;

  const onChange = (imageList, addUpdateIndex) => {
    // data for submit
    console.log(imageList, addUpdateIndex);
    setImages(imageList);

    console.log("Array Images Uploaded =>", imageList);
    console.log("Image => ", imageList[0].data_url);

    setCardEmoji(imageList[0].data_url);
  };

  const handleOnChange = (e) => {
    setCardName(e);
    console.log("name card =>", e);
  };

  const onUpdate = async () => {
    await updateWeRecognizeCard(idCard, {name: cardName, emoji: cardEmoji});
    handleClose();
    getWeRecognizeCards(page);
    setImages([]);
  };

  const onSubmit = async () => {
    await createWeRecognizeCard({ name: cardName, emoji: cardEmoji });
    handleClose();
    getWeRecognizeCards(page);
    setImages([]); 
 };

  const handleShow = () => {
    setShow(true);
    setCardName("");
    setCardEmoji("");
    setIsUpdate(false);
  };

  useEffect(() => {
    getWeRecognizeCards(1);
  }, []);

  const getWeRecognizeCards = async page => {
    const resp = await getWeRecognizeCard(page);
    setCards(resp.results);
    setCountTotal(resp.count);
  };

  const updateCard = item => {
    console.log(item);
    handleShow();
    setCardName(item.name);
    setCardEmoji(item.emoji);

    setIsUpdate(true);

    setIdCard(item.id);
  }

  const handleDelete = async item => {
    await deleteWeRecognizeCard(item.id);
    getWeRecognizeCards(page);
  }

  const handlePageChange = e => {
    console.log(e);
    setPage(e);
    getWeRecognizeCards(e);
    window.scroll(0,0);
  }

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          {isUpdate ? (
            <Modal.Title>Editar Tarjeta</Modal.Title>
          ) : (
            <Modal.Title>Nueva Tarjeta</Modal.Title>
          )}
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="formGroupEmail">
              <Form.Label>Titulo de Tarjeta</Form.Label>
              <Form.Control
                value={cardName}
                name="name"
                onChange={(e) => handleOnChange(e.target.value)}
                type="name"
                placeholder="Nombre"
              />
            </Form.Group>

            {/* IMAGE UPLOAD */}

            <ImageUploading
              /* multiple */
              value={images}
              onChange={onChange}
              maxNumber={maxNumber}
              dataURLKey="data_url"
            >
              {({
                imageList,
                onImageUpload,
                onImageRemoveAll,
                onImageUpdate,
                onImageRemove,
                isDragging,
                dragProps,
              }) => (
                // write your building UI
                <div className="upload__image-wrapper">
                  <button
                    className="btn btn-outline-primary"
                    style={isDragging ? { color: "blue" } : undefined}
                    onClick={onImageUpload}
                    {...dragProps}
                  >
                    Seleccionar archivo...
                  </button>
                  &nbsp;
                  <hr/>
                  {/* <button onClick={onImageRemoveAll}>Remove all images</button> */}
                  {imageList.map((image, index) => (
                    <div key={index} className="image-item">
                      <img src={image["data_url"]} alt="" width="100" />
                      {/* <div className="image-item__btn-wrapper">

                        <button onClick={() => onImageUpdate(index)}>
                          Update
                        </button>
                        <button className="btn btn-outline-danger" onClick={() => onImageRemove(index)}>
                          Eliminar
                        </button>

                      </div> */}
                    </div>
                  ))}
                </div>
              )}
            </ImageUploading>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancelar
          </Button>
          {isUpdate ? (
            <Button variant="primary" onClick={onUpdate}>
              Actualizar
            </Button>
          ) : (
            <Button variant="primary" onClick={onSubmit}>
              Guardar Cambios
            </Button>
          )}
        </Modal.Footer>
      </Modal>
      <div className="container">
        <div className="col">
          <div className="mt-5">
            <div style={{ display: "flex", justifyContent: "flex-start" }}>
              <img
                style={{ width: 100, height: 100 }}
                src={require("../../assets/icons-board/Trophy.png")}
                alt=""
              />
            </div>
            <div style={{ display: "flex", justifyContent: "flex-start" }}>
              <h3 className="title_text">Reconocemos</h3>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th
                style={{
                  color: "#272727",
                  fontSize: 18,
                  textShadowColor: "#272727",
                  textShadowOffset: { width: 0.5, height: 0.5 },
                  textShadowRadius: 1,
                }}
              >
                ID
              </th>
              <th
                style={{
                  color: "#272727",
                  fontSize: 18,
                  textShadowColor: "#272727",
                  textShadowOffset: { width: 0.5, height: 0.5 },
                  textShadowRadius: 1,
                }}
              >
                Reconocimiento
              </th>
              <th
                style={{
                  color: "#272727",
                  fontSize: 18,
                  textShadowColor: "#272727",
                  textShadowOffset: { width: 0.5, height: 0.5 },
                  textShadowRadius: 1,
                }}
              >
                Medalla
              </th>
              <th
                style={{
                  color: "#272727",
                  fontSize: 18,
                  textShadowColor: "#272727",
                  textShadowOffset: { width: 0.5, height: 0.5 },
                  textShadowRadius: 1,
                }}
              >
                Fecha de creación
              </th>
              <th
                style={{
                  color: "#272727",
                  fontSize: 18,
                  textShadowColor: "#272727",
                  textShadowOffset: { width: 0.5, height: 0.5 },
                  textShadowRadius: 1,
                }}
              >
                Opciones
              </th>
            </tr>
          </thead>
          <tbody>
            {cards.map((item, index) => (
              <>
                <tr>
                  <td
                    style={{
                      color: "#272727",
                      fontSize: 18,
                      textShadowColor: "#272727",
                      textShadowOffset: { width: 0.5, height: 0.5 },
                      textShadowRadius: 1,
                    }}
                  >
                    {index + 1}
                  </td>
                  <td>
                    <h5
                      style={{
                        color: "#272727",
                        fontSize: 18,
                        textShadowColor: "#272727",
                        textShadowOffset: { width: 0.5, height: 0.5 },
                        textShadowRadius: 1,
                      }}
                    >
                      {item.name}
                    </h5>
                  </td>
                  <td>
                    <img
                      style={{ width: 50, height: 50 }}
                      src={item.emoji}
                      alt=""
                    />
                  </td>
                  <td
                    style={{
                      color: "#272727",
                      fontSize: 18,
                      textShadowColor: "#272727",
                      textShadowOffset: { width: 0.5, height: 0.5 },
                      textShadowRadius: 1,
                    }}
                  >
                    {item.timestamp}
                  </td>
                  <td>
                    <div className="row">
                      <div className="col-2">
                        <button onClick={() => updateCard(item)} className="btn btn-info mr-5">
                          <i class="fas fa-edit"></i>
                        </button>
                      </div>
                      <div className="col-2">
                        <button onClick={() => handleDelete(item)} className="btn btn-danger">
                          <i class="fas fa-trash-alt"></i>
                        </button>
                      </div>
                    </div>
                  </td>
                </tr>
              </>
            ))}
          </tbody>
        </table>
      </div>
      <div className="container">
        <div className="col">
          <Button variant="primary" onClick={handleShow}>
            <i className="fa fa-plus mr-2"></i>
            Agregar
          </Button>
        </div>
      </div>

      {({ imageList, onImageUpload, onImageRemoveAll, errors }) =>
        errors && (
          <div>
            {errors.maxNumber && (
              <span>Number of selected images exceed maxNumber</span>
            )}
            {errors.acceptType && (
              <span>Your selected file type is not allow</span>
            )}
            {errors.maxFileSize && (
              <span>Selected file size exceed maxFileSize</span>
            )}
            {errors.resolution && (
              <span>Selected file is not match your desired resolution</span>
            )}
          </div>
        )
      }
      <div style={{display: 'flex', justifyContent: 'center', width: '100%'}}>
          <Pagination
            hideDisabled
            activePage={page}
            itemsCountPerPage={50}
            totalItemsCount={countTotal}
            onChange={(e) => handlePageChange(e)}
            pageRangeDisplayed={10}
            itemClass="page-item"
            linkClass="page-link"
          />
      </div>

    </>
  );
};
export default Reconocemos;

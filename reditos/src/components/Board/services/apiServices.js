import {body, url} from './auth';
import axios from 'axios';

//Sweet

export const createSweetCard = async obj => {

    const respToken = await axios.post(url.urlToken, body , {
            headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.post(`${url.urlBase}/sweet/`, obj ,{
        headers: { Authorization: `JWT ${token}` },
    });

    console.log('data => ', data);
}

export const updateSweetCard = async (id, obj) => {

    const respToken = await axios.post(url.urlToken, body , {
        headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.put(`${url.urlBase}/sweet/${id}/`, obj ,{
        headers: { Authorization: `JWT ${token}` },
    });

    console.log(data);


}

export const deleteCardSweet = async (id) => {

    const respToken = await axios.post(url.urlToken, body , {
        headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.delete(`${url.urlBase}/sweet/${id}/`, {
        headers: { Authorization: `JWT ${token}` },
    });

    console.log(data);
}

export const getSweetCard = async page => {

    const respToken = await axios.post(url.urlToken, body , {
            headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.get(`${url.urlBase}/sweet/?page=${page}`, {
        headers: { Authorization: `JWT ${token}` },
    });

    console.log('data sweet => ', data);

    console.log('data count page => ', data.data.count);

    return data.data;
}

//Bitter

export const createBitterCard = async obj => {

    const respToken = await axios.post(url.urlToken, body , {
            headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.post(`${url.urlBase}/bitter/`, obj ,{
        headers: { Authorization: `JWT ${token}` },
    });

    console.log('data => ', data);
}

export const updateBitterCard = async (id, obj) => {

    const respToken = await axios.post(url.urlToken, body , {
        headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.put(`${url.urlBase}/bitter/${id}/`, obj ,{
        headers: { Authorization: `JWT ${token}` },
    });

    console.log(data);


}

export const deleteBitterCard = async (id) => {

    const respToken = await axios.post(url.urlToken, body , {
        headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.delete(`${url.urlBase}/bitter/${id}/`, {
        headers: { Authorization: `JWT ${token}` },
    });

    console.log(data);
}

export const getBitterCard = async page => {

    const respToken = await axios.post(url.urlToken, body , {
            headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.get(`${url.urlBase}/bitter/?page=${page}`, {
        headers: { Authorization: `JWT ${token}` },
    });

    console.log('data => ', data);

    return data.data;
}



//Additions

export const createAdditionCard = async obj => {

    const respToken = await axios.post(url.urlToken, body , {
            headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.post(`${url.urlBase}/additions/`, obj ,{
        headers: { Authorization: `JWT ${token}` },
    });

    console.log('data => ', data);
}

export const updateAdditionCard = async (id, obj) => {

    const respToken = await axios.post(url.urlToken, body , {
        headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.put(`${url.urlBase}/additions/${id}/`, obj ,{
        headers: { Authorization: `JWT ${token}` },
    });

    console.log(data);


}

export const deleteAdditionCard = async (id) => {

    const respToken = await axios.post(url.urlToken, body , {
        headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.delete(`${url.urlBase}/additions/${id}/`, {
        headers: { Authorization: `JWT ${token}` },
    });

    console.log(data);
}

export const getAdditionCard = async page => {

    const respToken = await axios.post(url.urlToken, body , {
            headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.get(`${url.urlBase}/additions/?page=${page}`, {
        headers: { Authorization: `JWT ${token}` },
    });

    console.log('data => ', data);

    return data.data;
}

// WeRecognize

export const createWeRecognizeCard = async obj => {

    const respToken = await axios.post(url.urlToken, body , {
            headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.post(`${url.urlBase}/weRecognize/`, obj ,{
        headers: { Authorization: `JWT ${token}` },
    });

    console.log('data => ', data);
}

export const updateWeRecognizeCard = async (id, obj) => {

    const respToken = await axios.post(url.urlToken, body , {
        headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.put(`${url.urlBase}/weRecognize/${id}/`, obj ,{
        headers: { Authorization: `JWT ${token}` },
    });

    console.log(data);


}

export const deleteWeRecognizeCard = async (id) => {

    const respToken = await axios.post(url.urlToken, body , {
        headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.delete(`${url.urlBase}/weRecognize/${id}/`, {
        headers: { Authorization: `JWT ${token}` },
    });

    console.log(data);
}

export const getWeRecognizeCard = async page => {

    const respToken = await axios.post(url.urlToken, body , {
            headers: {'Content-Type': 'application/json'}
    });

    const token = await respToken.data.token;

    const data = await axios.get(`${url.urlBase}/weRecognize/?page=${page}`, {
        headers: { Authorization: `JWT ${token}` },
    });

    console.log('data => ', data);

    return data.data;
}














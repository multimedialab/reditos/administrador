import React, { useState, useEffect } from "react";
import "./board-style.scss";
import { Modal, Button, Form } from "react-bootstrap";
import {
  createBitterCard,
  getBitterCard,
  updateBitterCard,
  deleteBitterCard
} from "./services/apiServices";
import Pagination from "react-js-pagination";

export default function LoAmargo() {


  const [page, setPage] = useState(0);
  const [countTotal, setCountTotal] = useState(0);


  useEffect(() => {
    getBitterCards(1);
  }, []);

  const getBitterCards = async page => {
    const resp = await getBitterCard(page);
    console.log(resp);
    setCards(resp.results);
    setCountTotal(resp.count);
  };

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => {
    setShow(true);
    setCard({name: ''});
    setIsUpdate(false);
  };

  const [card, setCard] = useState({ name: "" });
  const [cards, setCards] = useState([]);

  const handleOnChange = (e) => {
    setCard({ name: e });
    console.log("name card =>", e);
  };

  const onSubmit = async () => {
    await createBitterCard(card);

    handleClose();

    getBitterCards(page);
  };

  const updateCard = (item) => {
    console.log(item);
    handleShow();
    setCard({ name: item.name });

    setIsUpdate(true);

    setIdCard(item.id);
  };

  const onUpdate = async () => {
    await updateBitterCard(idCard, card);
    handleClose();
    getBitterCards(page);
  };

  const [isUpdate, setIsUpdate] = useState(false);
  const [idCard, setIdCard] = useState(0);

  const handleDelete = async item => {
    await deleteBitterCard(item.id);
    getBitterCards(page);
  }

  const handlePageChange = e => {
    console.log(e);
    setPage(e);
    getBitterCards(e);
    window.scroll(0,0);
  }

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          {
            isUpdate ? 
              <Modal.Title>Editar Tarjeta</Modal.Title>
            :
              <Modal.Title>Nueva Tarjeta</Modal.Title>
          }
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="formGroupEmail">
              <Form.Label>Titulo de Tarjeta</Form.Label>
              <Form.Control
                value={card.name}
                name="name"
                onChange={(e) => handleOnChange(e.target.value)}
                type="name"
                placeholder="Nombre"
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancelar
          </Button>
          {
            isUpdate ?
              <Button variant="primary" onClick={onUpdate}>
              Actualizar
              </Button>
            :
              <Button variant="primary" onClick={onSubmit}>
                Guardar Cambios
              </Button>

          }
        </Modal.Footer>
      </Modal>

      <div className="container">
        <div className="col">
          <div className="mt-5">
            <div style={{ display: "flex", justifyContent: "flex-start" }}>
              <img
                style={{ width: 100, height: 100 }}
                src={require("../../assets/icons-board/Bitbucket.png")}
                alt=""
              />
            </div>
            <div style={{ display: "flex", justifyContent: "flex-start" }}>
              <h3 className="title_text">Lo Amargo</h3>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th
                style={{
                  color: "#272727",
                  fontSize: 18,
                  textShadowColor: "#272727",
                  textShadowOffset: { width: 0.5, height: 0.5 },
                  textShadowRadius: 1,
                }}
              >
                ID
              </th>
              <th
                style={{
                  color: "#272727",
                  fontSize: 18,
                  textShadowColor: "#272727",
                  textShadowOffset: { width: 0.5, height: 0.5 },
                  textShadowRadius: 1,
                }}
              >
                Nombre
              </th>

              <th
                style={{
                  color: "#272727",
                  fontSize: 18,
                  textShadowColor: "#272727",
                  textShadowOffset: { width: 0.5, height: 0.5 },
                  textShadowRadius: 1,
                }}
              >
                Opciones
              </th>
            </tr>
          </thead>
          <tbody>
            {cards.map((item, index) => (
              <>
                <tr>
                  <td
                    style={{
                      color: "#272727",
                      fontSize: 18,
                      textShadowColor: "#272727",
                      textShadowOffset: { width: 0.5, height: 0.5 },
                      textShadowRadius: 1,
                    }}
                  >
                    {index + 1}
                  </td>
                  <td
                    style={{
                      color: "#272727",
                      fontSize: 18,
                      textShadowColor: "#272727",
                      textShadowOffset: { width: 0.5, height: 0.5 },
                      textShadowRadius: 1,
                    }}
                  >
                    {item.name}
                  </td>

                  <td>
                    <div className="row">
                      <div className="col-2">
                        <button onClick={() => updateCard(item)} className="btn btn-info">
                          <i class="fas fa-edit"></i>
                        </button>
                      </div>
                      <div className="col-2">
                        <button onClick={() => handleDelete(item)} className="btn btn-danger">
                          <i class="fas fa-trash-alt"></i>
                        </button>
                      </div>
                    </div>
                  </td>
                </tr>
              </>
            ))}
          </tbody>
        </table>
      </div>
      <div className="container">
        <div className="col">
          <Button variant="primary" onClick={handleShow}>
            <i className="fa fa-plus mr-2"></i>
            Agregar
          </Button>
        </div>
      </div>
      <div style={{display: 'flex', justifyContent: 'center', width: '100%'}}>
          <Pagination
            hideDisabled
            activePage={page}
            itemsCountPerPage={50}
            totalItemsCount={countTotal}
            onChange={(e) => handlePageChange(e)}
            pageRangeDisplayed={10}
            itemClass="page-item"
            linkClass="page-link"
          />
      </div>
    </>
  );
}

import React, { useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { withRouter } from "react-router";

const useStyles = makeStyles({
  list: {
    width: 300,
    color: '#fff'
  },
  fullList: {
    width: "auto",
  },
});

const SwipeableTemporaryDrawer = (props) => {
  useEffect(() => {
    console.log("props => ", props);
  }, []);

  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const categories = [
    
    {
      text: "Valores Corporativos",
      
    },
    {
      text: "Objetivos",
    },
    {
      text: "Competencias Organizacionales",
    },
    {
      text: "Competencias Especificas",
    },
    {
      text: "Teletrabajo",
    },

  ];

  const checklist = [
    
    {
      text: "Administrar Fechas",
      
    },
    {
      text: "Administrar CheckList",
    },
    

  ];



  const gestion = [
    
    {
      text: "Empresas",
      
    },
    {
      text: "Lideres",
    },
    {
      text: "Colaboradores",
    },
    {
      text: "Cargos",
    },

  ];

  const credenciales = [
    
    {
      text: "Credenciales Lideres",
      
    },
    {
      text: "Credenciales Colaboradores",
    },
    

  ];

  const notificaciones = [
    
    {
      text: "Notificaciones",
      
    },
  ];

  const gestionTablero = [
    
    {
      text: "Gestionar Tablero",
      
    },
  ];

  const loDulce = [
    
    {
      text: "Lo Dulce",
      
    },
  ];

  const loAmargo = [
    
    {
      text: "Lo Amargo",
      
    },
  ];

  const adiciones = [
    
    {
      text: "Adiciones",
      
    },
  ];

  const reconocemos = [
    
    {
      text: "Reconocemos",
      
    },
  ];

  





  const list = (anchor) => (
    <div
      style={{ backgroundColor: "#004f9e" }}
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {categories.map((item, index) => {
          const { text, onclick } = item;
          /* const {history} = props; */
          return (
            <ListItem button key={text} onClick={onclick}>
              {/*             <ListItemIcon>{text == 'Administrar Categorias' ? console.log('seleccion => ', text) : null }</ListItemIcon>
               */}
              
              <ListItemText
                onClick={() => {
                  switch (index) {
                    case 0:
                      props.history.push("/dashboard/configurar-categorias");
                      break;
                    case 1:
                      props.history.push("/dashboard/objetivos");
                      break;
                    case 2:
                      props.history.push(
                        "/dashboard/competencias-organizacionales"
                      );
                      break;
                    case 3:
                      props.history.push("/dashboard/competencias-especificas");
                      break;
                    case 4:
                      props.history.push("/dashboard/teletrabajo");
                      break;
                  }
                }}
                primary={text}
                
              />
            </ListItem>
          );
        })}
      </List>
      <Divider />
      <List>
        {checklist.map((item, index) => {
          const { text, onclick } = item;
          /* const {history} = props; */
          return (
            <ListItem button key={text} onClick={onclick}>
              {/*             <ListItemIcon>{text == 'Administrar Categorias' ? console.log('seleccion => ', text) : null }</ListItemIcon>
               */}
              
              <ListItemText
                onClick={() => {
                  switch (index) {
                    case 0:
                      props.history.push("/dashboard/administrar-fechas");
                      break;
                    case 1:
                      props.history.push("/dashboard/crear-preguntas-checklist");
                      break;
                  }
                }}
                primary={text}
                
              />
            </ListItem>
          );
        })}
      </List>
      <Divider />
      <List>
        {gestion.map((item, index) => {
          const { text, onclick } = item;
          /* const {history} = props; */
          return (
            <ListItem button key={text} onClick={onclick}>
              {/*             <ListItemIcon>{text == 'Administrar Categorias' ? console.log('seleccion => ', text) : null }</ListItemIcon>
               */}
              
              <ListItemText
                onClick={() => {
                  switch (index) {
                    case 0:
                      props.history.push("/dashboard/empresas");
                      break;
                    case 1:
                      props.history.push("/dashboard/lideres");
                      break;
                    case 2:
                      props.history.push(
                        "/dashboard/colaboradores"
                      );
                      break;
                    case 3:
                      props.history.push("/dashboard/cargos");
                      break;
                    
                  }
                }}
                primary={text}
                
              />
            </ListItem>
          );
        })}
      </List>
      <Divider />
      <List>
        {credenciales.map((item, index) => {
          const { text, onclick } = item;
          /* const {history} = props; */
          return (
            <ListItem button key={text} onClick={onclick}>
              {/*             <ListItemIcon>{text == 'Administrar Categorias' ? console.log('seleccion => ', text) : null }</ListItemIcon>
               */}
              
              <ListItemText
                onClick={() => {
                  switch (index) {
                    case 0:
                      props.history.push("/dashboard/credenciales-lideres");
                      break;
                    case 1:
                      props.history.push("/dashboard/credenciales-colaboradores");
                      break;
                  }
                }}
                primary={text}
                
              />
            </ListItem>
          );
        })}
      </List>

      <Divider />
      <List>
        {notificaciones.map((item, index) => {
          const { text, onclick } = item;
          /* const {history} = props; */
          return (
            <ListItem button key={text} onClick={onclick}>
              {/*             <ListItemIcon>{text == 'Administrar Categorias' ? console.log('seleccion => ', text) : null }</ListItemIcon>
               */}
              
              <ListItemText
                onClick={() => {
                  switch (index) {
                    case 0:
                      props.history.push("/dashboard/notificaciones");
                      break;
                  }
                }}
                primary={text}
                
              />
            </ListItem>
          );
        })}
      </List>

      <Divider />
      <List>
        {gestionTablero.map((item, index) => {
          const { text, onclick } = item;
          /* const {history} = props; */
          return (
            <ListItem button key={text} onClick={onclick}>
              {/*             <ListItemIcon>{text == 'Administrar Categorias' ? console.log('seleccion => ', text) : null }</ListItemIcon>
               */}
              
              <ListItemText
                onClick={() => {
                  switch (index) {
                    case 0:
                      props.history.push("/dashboard/notificaciones");
                      break;
                  }
                }}
                primary={text}
                
              />
            </ListItem>
          );
        })}
      </List>
      <Divider />
      <List>
        {loDulce.map((item, index) => {
          const { text, onclick } = item;
          /* const {history} = props; */
          return (
            <ListItem button key={text} onClick={onclick}>
              {/*             <ListItemIcon>{text == 'Administrar Categorias' ? console.log('seleccion => ', text) : null }</ListItemIcon>
               */}
              
              <ListItemText
                onClick={() => {
                  switch (index) {
                    case 0:
                      props.history.push("/dashboard/Board/LoDulce");
                      break;
                  }
                }}
                primary={text}
                
              />
            </ListItem>
          );
        })}
      </List>
      <Divider />
      <List>
        {loAmargo.map((item, index) => {
          const { text, onclick } = item;
          /* const {history} = props; */
          return (
            <ListItem button key={text} onClick={onclick}>
              {/*             <ListItemIcon>{text == 'Administrar Categorias' ? console.log('seleccion => ', text) : null }</ListItemIcon>
               */}
              
              <ListItemText
                onClick={() => {
                  switch (index) {
                    case 0:
                      props.history.push("/dashboard/Board/LoAmargo");
                      break;
                  }
                }}
                primary={text}
                
              />
            </ListItem>
          );
        })}
      </List>
      <Divider />
      <List>
        {adiciones.map((item, index) => {
          const { text, onclick } = item;
          /* const {history} = props; */
          return (
            <ListItem button key={text} onClick={onclick}>
              {/*             <ListItemIcon>{text == 'Administrar Categorias' ? console.log('seleccion => ', text) : null }</ListItemIcon>
               */}
              
              <ListItemText
                onClick={() => {
                  switch (index) {
                    case 0:
                      props.history.push("/dashboard/Board/Adiciones");
                      break;
                  }
                }}
                primary={text}
                
              />
            </ListItem>
          );
        })}
      </List>
      <Divider />
      <List>
        {reconocemos.map((item, index) => {
          const { text, onclick } = item;
          /* const {history} = props; */
          return (
            <ListItem button key={text} onClick={onclick}>
              {/*             <ListItemIcon>{text == 'Administrar Categorias' ? console.log('seleccion => ', text) : null }</ListItemIcon>
               */}
              
              <ListItemText
                onClick={() => {
                  switch (index) {
                    case 0:
                      props.history.push("/dashboard/Board/Reconocemos");
                      break;
                  }
                }}
                primary={text}
                
              />
            </ListItem>
          );
        })}
      </List>








      <List>
        {["GRUPO REDITOS", "", ""].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <img src="" alt="" /> : <img src="" alt="" />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <div>
      {["left"].map((anchor) => (
        <React.Fragment key={anchor}>
          <button
            className="btn btn-primary ml-3"
            onClick={toggleDrawer(anchor, true)}
          >
            {/* <i class="fas fa-bars"></i> */}
            <img src={require("../../assets/1x/logo.png")} alt="" />
          </button>
          {/* <Button onClick={toggleDrawer(anchor, true)}>{anchor}</Button> */}
          <SwipeableDrawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
            onOpen={toggleDrawer(anchor, true)}
          >
            {list(anchor)}
          </SwipeableDrawer>
        </React.Fragment>
      ))}
    </div>
  );
};

export default withRouter(SwipeableTemporaryDrawer);

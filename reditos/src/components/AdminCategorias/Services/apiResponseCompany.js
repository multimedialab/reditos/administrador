import axios from "axios";
import { url, body } from "./auth";

export const apiToken = async () => {
  try {
      
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    const resp = await axios.get(`${url.urlBase}/company/`, {
        headers: {'Authorization': `JWT ${token}`}
    });

    const data = resp.data;

    return token;

  } catch (error) {
    alert("error", error.message);
  }
};

export const getCompanies = async () => {
  try {

    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    const resp = await axios.get(`${url.urlBase}/company/`, {
      headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
    });
    console.log('resp => ', resp);
    console.log("resp companies => ", resp.data.results);
    return resp.data.results;

  } catch (error) {
    console.log(error);
    throw Error(error);
  }
}


export const deleteCompany = async id => {
  //const token = await apiToken();
  try {
      
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    await axios.delete(`${url.urlBase}/company/${id}/`, {
      headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
    });

    
  } catch (error) {
    alert("error", error.message);
  }

}

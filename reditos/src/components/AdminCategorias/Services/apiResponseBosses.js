import axios from "axios";
import { url, body } from "./auth";

export const apiToken = async () => {
  try {
    //Go to token
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    //Go to data
    const resp = await axios.get(`${url.urlBase}/bosses/`, {
        headers: {'Authorization': `JWT ${token}`}
    });

    const data = resp.data;

    return token;

  } catch (error) {
    alert("error", error.message);
  }
};
import axios from "axios";
import { url, body } from "./auth";

export const getLeadersByCompany = async id_company => {
  try {
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    const resp = await axios.get(`${url.urlBase}/leaders/?id_company=${id_company}`, {
        headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
    });

    console.log('resp => ', resp);
    console.log("resp leaders => ", resp.data.results);
    return resp.data.results;


  } catch (error) {
    console.log(error);
    throw Error(error);
  }
};

export const getCollaboratorsByLeader = async id_leader => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
  
      const token = await responseToken.data.token;
  
      const resp = await axios.get(`${url.urlBase}/collaborators/?id_leader=${id_leader}`, {
          headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
      });
  
      console.log('resp => ', resp);
      console.log("resp collaborators => ", resp.data.results);
      return resp.data.results;
  
  
    } catch (error) {
      console.log(error);
      throw Error(error);
    }
  };









import { url, body } from "./auth";
import axios from "axios";

//Get Data
export const getDate = async () => {
  const respToken = await axios.post(url.urlToken, body, {
    headers: { "Content-Type": "application/json" },
  });

  const token = await respToken.data.token;

  const data = await axios.get(`${url.urlBase}/settings/`, {
    headers: { Authorization: `JWT ${token}` },
  });

  console.log("results => ", data);

  return data.data.results;
};

//Create Data
export const createDate = async (values) => {
  const respToken = await axios.post(url.urlToken, body, {
    headers: { "Content-Type": "application/json" },
  });

  const token = await respToken.data.token;

  const data = await axios.post(`${url.urlBase}/settings/`, values, {
    headers: { 'Authorization': `JWT ${token}` },
  });

  console.log("values => ", values);

  return console.log("resp service =>", data);
};


//Update Data
export const updateData = async (idDate, values) => {
  const respToken = await axios.post(url.urlToken, body, {
    headers: { "Content-Type": "application/json" },
  });

  const token = await respToken.data.token;

  const data = await axios.put(`${url.urlBase}/settings/${idDate}/`, values, {
    headers: { 'Authorization': `JWT ${token}` },
  });

  console.log("values => ", values);

  return console.log("resp service =>", data);
};

//Delete Data
export const deleteData = async (idDate) => {
    const respToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
    
      const token = await respToken.data.token;
    
      const data = await axios.delete(`${url.urlBase}/settings/${idDate}/`, {
        headers: { 'Authorization': `JWT ${token}` },
      });
    
      //console.log("values => ", values);
    
      return console.log("resp service =>", data);

};

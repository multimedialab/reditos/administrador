import axios from 'axios';
import { url, body } from "./auth";

export const deletePosition = async (id) => {

    try {
      
        const responseToken = await axios.post(url.urlToken, body, {
          headers: { "Content-Type": "application/json" },
        });
    
        const token = await responseToken.data.token;
    
        await axios.delete(`${url.urlBase}/positions/${id}/`, {
          headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
        });
    
        
      } catch (error) {
        alert("error", error.message);
      }

}
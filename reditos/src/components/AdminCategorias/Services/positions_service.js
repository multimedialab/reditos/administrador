import axios from "axios";
import { url, body } from "../Services/auth";

export const getCompanies = async () => {
  try {
    const token = await getToken();
    const respData = axios.get(`${url.urlBase}/company/`, {
      headers: { Authorization: `JWT ${token}` },
    });
    const data = (await respData).data;

    return data.results;

  } catch (error) {
    console.log("company error: ", error.message);
  }
};

export const getPositions = async () => {
  try {
    const token = await getToken();
    const respData = axios.get(`${url.urlBase}/getPositions/`, {
      headers: { Authorization: `JWT ${token}` },
    });
    const data = (await respData).data;
    
    return data.cargo;
    
  } catch (error) {
    console.log("positions error: ", error.message);
  }
};

const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
};

export const getCategories = async () => {
    try {
        const token = await getToken();
        const respData = axios.get(`${url.urlBase}/category/`, {
          headers: { Authorization: `JWT ${token}` },
        });
        const data = (await respData).data;
        
        return data.results;
        
      } catch (error) {
        console.log("categories error: ", error.message);
      }
}
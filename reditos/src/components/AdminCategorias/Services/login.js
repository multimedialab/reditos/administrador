import {url ,body} from '../Services/auth';
import axios from 'axios';

export const signIn = async (email, password) => {

    try {
        const getToken = await axios.post(url.urlToken, body, {
            headers: {'Content-Type': 'application/json'}
        });

        const token = await getToken.data.token;

        const resp = await axios.get(`${url.urlBase}/accessLogin/?email=${email}&password=${password}`, {
            headers: {'Authorization': `JWT ${token}`}
        });

        const data = resp.data.results;

        
        return data;
    } catch (error) {
        console.log(error.message);
    }
}


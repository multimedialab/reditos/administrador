import axios from "axios";
import { url, body } from "./auth";

export const deleteCompany = async (id) => {
    //const token = await apiToken();
    try {
        
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
  
      const token = await responseToken.data.token;
  
      await axios.delete(`${url.urlBase}/collaborators/${id}/`, {
        headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
      });
  
      
    } catch (error) {
      alert("error", error.message);
    }
}
  
  
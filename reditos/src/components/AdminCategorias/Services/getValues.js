import axios from "axios";
import { url, body } from "./auth";

export const getValues = async id_value => {
  try {
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    const request = await axios.get(
      `${url.urlBase}/items/?id_value=${id_value}`,
      {
        headers: { 'Authorization': `JWT ${token}` },
      }
    );

    return request.data.results;
  } catch (error) {
    console.log(`Error getValues => ${error.code} : ${error.message}`);
  }
};

export const updateValues = async (id_value, obj) => {
    try {

        const responseToken = await axios.post(url.urlToken, body, {
            headers: {'Content-Type': 'application/json'}
        });

        const token = await responseToken.data.token;

        const request = await axios.put(`${url.urlBase}/items/${id_value}/`, obj, {
            headers: {'Authorization': `JWT ${token}`}
        });

        console.log(request);
        
    } catch (error) {
        console.log(`Error updateValues => ${error.code} - ${error.message}`);
    }
}

export const deleteValues = async (id_value) => {
    try {

        const responseToken = await axios.post(url.urlToken, body, {
            headers: {'Content-Type': 'application/json'}
        });

        const token = await responseToken.data.token;

        const request = await axios.delete(`${url.urlBase}/items/${id_value}/`, {
            headers: {'Authorization': `JWT ${token}`}
        });

        console.log(request);
        
    } catch (error) {
        console.log(`Error updateValues => ${error.code} - ${error.message}`);
    }
}





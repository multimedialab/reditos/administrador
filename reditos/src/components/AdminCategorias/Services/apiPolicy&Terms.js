import axios from 'axios';
import { url, body } from "./auth";

// Get Policy Terms

export const getPolicyTerms = async () => {
    try {
      const respToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
  
      const token = await respToken.data.token;
  
      const data = await axios.get(`${url.urlBase}/settings/`, {
          headers: { 'Authorization': `JWT ${token}` },
        });

        console.log('data =>', data);
  
      return data.data.results;
    } catch (error) {
      console.log(
        `Error getPolicyTerms => ${error.code} - ${error.message}`
      );
    }
};

export const UpdatePolicyTerms = async (id,obj) => {
    try {
      const respToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
  
      const token = await respToken.data.token;
  
      const data = await axios.put(`${url.urlBase}/settings/${id}/`, obj, {
          headers: { 'Authorization': `JWT ${token}` },
      });
  
      console.log(data);
    } catch (error) {
      console.log(
        `Error UpdatePolicyTerms => ${error.code} - ${error.message}`
      );
    }
};




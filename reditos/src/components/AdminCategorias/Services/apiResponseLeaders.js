import axios from "axios";
import { url, body } from "./auth";

export const apiToken = async () => {
  try {
      
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    const resp = await axios.get(`${url.urlBase}/leaders/`, {
        headers: {'Authorization': `JWT ${token}`}
    });

    const data = resp.data;

    return token;

  } catch (error) {
    alert("error", error.message);
  }
};

export const deleteCompany = async (id) => {
  //const token = await apiToken();
  try {
      
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    await axios.delete(`${url.urlBase}/accessLogin/${id}/`, {
      headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
    });

    
  } catch (error) {
    alert("error", error.message);
  }
}

export const deleteLeader = async (id) => {
  //const token = await apiToken();
  try {
      
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    await axios.delete(`${url.urlBase}/leaders/${id}/`, {
      headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
    });
    
  } catch (error) {
    alert("error", error.message);
  }
}




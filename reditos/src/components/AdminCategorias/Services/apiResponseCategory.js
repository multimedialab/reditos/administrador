import axios from "axios";
import { url, body } from "./auth";

export const apiToken = async () => {
  try {
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    const resp = await axios.get(`${url.urlBase}/category/`, {
      headers: { Authorization: `JWT ${token}` },
    });

    const data = resp.data;

    return token;
  } catch (error) {
    alert("error", error.message);
  }
};

export const getCategories = async () => {
  const token = await apiToken();
  const respData = axios.get(`${url.urlBase}/category/`, {
    headers: { Authorization: `JWT ${token}` },
  });
  const data = (await respData).data;
  const resp = data.results;

  return resp;
};

export const getValuesByCategory = async (idCategory) => {

  const token = await apiToken();
  const respData = axios.get(`${url.urlBase}/values/?id_category=${idCategory}`, {
    headers: { Authorization: `JWT ${token}` },
  });
  const data = (await respData).data;
  const resp = data.results;

  return resp;
  
}

export const deleteCategory = async (id) => {
  //const token = await apiToken();
  try {
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    await axios.delete(`${url.urlBase}/category/${id}/`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${token}`,
      },
    });
  } catch (error) {
    alert("error", error.message);
  }
};

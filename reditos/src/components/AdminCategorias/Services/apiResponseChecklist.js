import axios from "axios";
import { url, body } from "./auth";

export const apiToken = async () => {
  try {
      
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    const resp = await axios.get(`${url.urlBase}/question/`, {
        headers: {'Authorization': `JWT ${token}`}
    });

    const data = resp.data;

    return token;

  } catch (error) {
    alert("error", error.message);
  }
};


export const deleteQuestionChecklist = async id => {
  //const token = await apiToken();
  try {
      
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    await axios.delete(`${url.urlBase}/question/${id}/`, {
      headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
    });

    
  } catch (error) {
    alert("error", error.message);
  }

}

export const deleteChecklist = async id => {
  //const token = await apiToken();
  try {
      
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    await axios.delete(`${url.urlBase}/checklist/${id}/`, {
      headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
    });

    
  } catch (error) {
    alert("error", error.message);
  }

}

//Update Data
export const updateChecklist = async (id, values) => {
  const respToken = await axios.post(url.urlToken, body, {
    headers: { "Content-Type": "application/json" },
  });

  const token = await respToken.data.token;

  const data = await axios.put(`${url.urlBase}/checklist/${id}/`, values, {
    headers: { 'Authorization': `JWT ${token}` },
  });

  console.log("values => ", values);

  return console.log("resp service =>", data);
};





import { url, body } from "./auth";
import axios from "axios";

//Get CheckList
export const getCheckListByCollaborator = async (idCollaborator) => {
  try {
    const respToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
    
      const token = await respToken.data.token;
    
      const data = await axios.get(`${url.urlBase}/answersChecklist/?collaborator=${idCollaborator}`, {
        headers: { Authorization: `JWT ${token}` },
      });

    console.log("request => ", data);

    return data.data.results;
  } catch (error) {
    console.log(
      `Error getCheckListByCollaborator => ${error.code} - ${error.message}`
    );
  }
};

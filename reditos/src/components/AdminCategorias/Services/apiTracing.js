import { url, body } from "./auth";
import axios from "axios";

// Get Tracing

export const getTracingByCollaborator = async (idCollaborator) => {
  try {
    const respToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await respToken.data.token;

    const data = await axios.get(
      `${url.urlBase}/tracing/?collaborator=${idCollaborator}`,
      {
        headers: { Authorization: `JWT ${token}` },
      }
    );

    return data.data.results;
  } catch (error) {
    console.log(
      `Error getActionPlanByCollaborator => ${error.code} - ${error.message}`
    );
  }
};

import React, { useState, useEffect } from "react";
import axios from "axios";
import { apiToken, deleteValue } from "../Services/apiResponseValues";
import { url, body } from "../Services/auth";
import Swal from "sweetalert2";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const ViewCRUD = () => {
  const initialState = {
    nameValue: "",
    description: "",
  };

  const [values, setValues] = useState(initialState);
  const [info, setInfo] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [desCat, setDesCat] = useState("");
  const [isConsulted, setIsConsulted] = useState(false);

  const [currentId, setCurrentId] = useState("");

  const [isUpdated, setIsUpdated] = useState(false);
  const [idQuestion, setidQuestion] = useState("");

  const consult = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/values/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => setInfo(json.results))
      .catch((error) => console.error(error));
  };

  const updateQuestion = async (id, name, weight, description, id_cat) => {
    const values = {
      nameValue: name,
      weight: weight,
      description: description,
      id_category: id_cat,
    };

    console.log("values: ", values);

    setidQuestion(id);

    console.log("id: ", id);
    setIsUpdated(true);
    setValues(values);
  };

  const handleDelete = async (id) => {
    console.log(id);
    console.log("DELETE ID: ", id);

    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#ccc",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        deleteValue(id);
        Swal.fire("Eliminado!");
        handleClickTodos();
      }
    });
  };

  //OnChange Inputs

  const handleOnChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };

  //OnSubmit Inputs

  const handleSubmit = (e) => {
    console.log(values);

    e.preventDefault();

    setValues({ ...initialState });
  };

  const handleClickEliminar = () => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Eliminado!");
      }
    });
  };

  useEffect(() => {
    handleClickTodos();
    //consult();
    console.log(info);
  }, []);

  const handleClickTodos = () => {
    consult();
    setIsConsulted(true);
  };

  const handleClickLimpiar = () => {
    setIsConsulted(false);
  };

  const handleClickActualizar = async () => {
    const token = await apiToken();
    const response = await axios.put(
      `${url.urlBase}/values/${idQuestion}/`,
      values,
      { headers: { Authorization: `JWT ${token}` } }
    );
    console.log(response);
    handleClickTodos();
    setValues({
        nameValue: '',
        description: ''
    })
    Swal.fire({
      title: "Actualizado con exito",
      icon: "success",
      showConfirmButton: false,
      timer: 1500,
    });

    

  };

  const handleClickCancel = () => {
    Swal.fire({
      title: "Cancelando...",
      icon: "warning",
    });
  };

  return (
    <>
      <div className="col-lg-12 col-md-12 col-sm-9 col-xs-9">
        <div className="logoImg "></div>
        <div className="">
          <h2 className="mt-5">
            <strong
              style={{ fontSize: 24, position: "relative", marginTop: 20 }}
            >
              ADMINISTRACION DE VALORES
            </strong>{" "}
          </h2>
          <br />
          <button className="btn btn-outline-primary">
            <Link to="/dashboard/configurar-categorias">Volver</Link>
          </button>
          <br />
          <form id="form" action="" onSubmit={handleSubmit}>
            <div className="admin">
              <div className="col">
                <label>Nombre del valor corporativo</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="nameValue"
                  value={values.nameValue}
                />
              </div>
            </div>
            <div className="admin">
              <div className="col">
                <label>Descripción</label>
              </div>
              <div className="col">
                <textarea
                  type="textarea"
                  onChange={handleOnChange}
                  className="form-control"
                  name="description"
                  value={values.description}
                />
              </div>
            </div>

            <div className="botones col-lg-12 col-md-12 col-sm-12">
              <div className="col">
                <Button
                  className="btn btn-info"
                  onClick={handleClickActualizar}
                  type="button"
                >
                  Actualizar
                  <i class="fas fa-sync-alt ml-2"></i>
                </Button>
              </div>
            </div>
          </form>
          {isConsulted ? (
            <div
              className="card-body ml-5 mr-3 mt-5"
              style={{
                height: "50vh",
                overflow: "scroll",
                overflowX: "hidden",
                top: 80,
                position: "relative",
              }}
            >
              <table
                className="table mt-3"
                style={{ border: "2px solid #004F9E" }}
              >
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Subcategoria</th>
                    <th>Descripción</th>
                    <th>Preguntas</th>
                    <th>Opciones</th>
                  </tr>
                </thead>

                <tbody>
                  {info.map((item, i) => (
                    <tr key={i}>
                      {item.name_category == "VALORES CORPORATIVOS" ? (
                        <>
                          <td>{i + 1} </td>
                          <td>{item.nameValue}</td>
                          <td>{item.description}</td>
                          <td>
                            <button>ver preguntas</button>
                          </td>
                          <td>
                            <button
                              onClick={() =>
                                updateQuestion(
                                  item.id,
                                  item.nameValue,
                                  item.weight,
                                  item.description,
                                  item.id_category
                                )
                              }
                              className="btn btn-info"
                            >
                              <i class="fas fa-edit"></i>
                            </button>{" "}
                            <button
                              onClick={() => handleDelete(item.id)}
                              className="btn btn-danger"
                            >
                              <i class="fas fa-trash-alt"></i>
                            </button>
                          </td>
                        </>
                      ) : null}
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </div>
    </>
  );
};
export default ViewCRUD;

import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
//import lupaLogo from "../../assets/lupa.png";
import Swal from "sweetalert2";
import axios from "axios";
import { url, body } from "../Services/auth";
import { Link } from "react-router-dom";
import './styleAdmin.css';

const Teletrabajo = () => {
  const initialState = {
    /*
    nameValue: "",
    weight: "",
    id_category: "",
    */
  };

  const [inputList, setInputList] = useState([
    { nameValue: "", description: "", weight: "", id_category: "" },
  ]);

  const [category, setCategory] = useState([]);
  const [values, setValues] = useState(initialState);
  const [info, setInfo] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [desCat, setDesCat] = useState("");
  const [isConsulted, setIsConsulted] = useState(false);

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };

  const handleOnChange = (e) => {
    console.log(e.target.value);
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };

  const getCategories = async () => {
    const token = await getToken();
    const respData = axios.get(`${url.urlBase}/category/`, {
      headers: { Authorization: `JWT ${token}` },
    });
    const data = (await respData).data;

    setCategory(data.results);
  };

  useEffect(() => {
    getCategories();
  }, []);

  const handleClickGuardar = () => {
    Swal.fire({
      icon: "success",
      title: "Registro Guardado Exitosamente",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleClickEliminar = () => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Eliminado!");
      }
    });
  };

  const handleClickCrear = async () => {
    const token = await getToken();

    console.log(values);
    console.log(inputList);

    for (let i = 0; i < inputList.length; i++) {
      if (Object.keys(inputList).length !== 0) {
        fetch(`${url.urlBase}/values/`, {
          method: "POST",
          body: JSON.stringify(inputList[i]),
          headers: {
            "Content-Type": "application/json",
            Authorization: `JWT ${token}`,
          },
        })
          .then((response) => response.json())
          .then((json) => {
            getCategories();
            setIsConsulted(true);
            Swal.fire({
              icon: "success",
              title: "Creado con exito",
              showConfirmButton: false,
              timer: 1500,
            });

            console.log("Initial State: ", values);
            console.log("Inputs State: ", inputList);
            console.log();
          })
          .catch((error) => console.error(error));
      } else {
        Swal.fire({
          icon: "error",
          title: "Los campos no pueden estar vacios",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    }
  };

  const handleSubmit = (e) => {
    //console.log(values);

    e.preventDefault();
    document.getElementById("form").reset();

    setValues({ ...initialState });
  };

  const flag = true;

  const handleAddClick = () => {
    setInputList([
      ...inputList,
      { nameValue: "", description: "", weight: "", id_category: "" },
    ]);
  };

  const handleRemoveClick = (index) => {
    const list = [...inputList];
    list.splice(index, 1);
    setInputList(list);
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
  };

  return (
    <>
      <div className="body">
        <div className="adminCategoria">
          <h2 id="title">
            Teletrabajo
          </h2>
          <br />

          <form id="form" action="" onSubmit={handleSubmit}>
            <div className="admin">
              
              <div className="col">
                <Form>
                  {/* 
                  <select
                    onChange={}
                    className="form-control"
                    name="id_category"
                    id=""
                  >
                    <option value={inputList.id_category}>Seleccionar</option>
                    {category.map((item, i) => (
                      <option value={item.url} key={i}>
                        {item.categoryName}
                      </option>
                    ))}
                  </select>
                  */}
                  {/* 
                  <select
                    onChange={handleOnChange}
                    className="form-control"
                    name="id_category"
                    id=""
                  >
                    {interf.map((item, i) => (
                      <option value={item.url} key={i}>
                        {item.nameInterfaceField}
                      </option>
                    ))}
                  </select>
                  */}
                </Form>
              </div>
              {/* 
              <img src={lupaLogo} style={{ width: 30, height: 30 }} alt="" />
              */}
            </div>

            <hr />

            <div className="container">
              <div className="col-12 text-center">
                <div className="col-4">
                  <strong>{/*VALORES*/}</strong>
                </div>
                <div className="text-center"></div>
                {/* 
                <div style={{ marginTop: 20 }}>{JSON.stringify(inputList)}</div>
                */}
                {/* 
                {initialState.subcategories.map((subcategory, i) => (
                  <div key={i}>
                    <input type="text" value={subcategory} />
                  </div>
                ))}
                */}

                <br />
                <table
                  className="table"
                  style={{ border: "2px solid #004F9E" }}
                >
                  <thead>
                    <tr>
                      <th># Valor</th>
                      <th>Nombre de Subcategoria</th>
                      <th>Descripción</th>

                      <th>Peso subcategoria</th>

                      <th>Categoria a la que pertenece</th>
                      <th>Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    {inputList.map((item, i) => {
                      return (
                        <>
                          <tr>
                            <td>{i + 1}</td>
                            <td>
                              <input
                                className="form-control"
                                type="text"
                                name="nameValue"
                                value={inputList.nameValue}
                                onChange={(e) => handleInputChange(e, i)}
                              />
                            </td>
                            <td>
                              <input
                                className="form-control"
                                type="text"
                                name="description"
                                value={inputList.description}
                                onChange={(e) => handleInputChange(e, i)}
                              />
                            </td>
                            <td>
                              <input
                                className="form-control"
                                type="text"
                                name="weight"
                                value={inputList.weight}
                                onChange={(e) => handleInputChange(e, i)}
                              />
                            </td>

                            <td>
                              <select
                                onChange={(e) => handleInputChange(e, i)}
                                className="form-control"
                                name="id_category"
                                id=""
                                value={inputList.id_category}
                              >
                                <option value={inputList.id_category}>
                                  Seleccionar
                                </option>
                                {category.map((item, i) =>
                                  item.categoryName ==
                                  "TELETRABAJO" ? (
                                    <option value={item.url} key={i}>
                                      {item.categoryName}
                                    </option>
                                  ) : null
                                )}
                              </select>
                            </td>

                            <td>
                              <div className="row">
                                <div className="col-1 mr-2">
                                  {inputList.length - 1 === i && (
                                    <button
                                      className="btn btn-primary mr-3"
                                      onClick={handleAddClick}
                                    >
                                      <i class="fas fa-plus"></i>
                                    </button>
                                  )}
                                </div>
                                <div className="col-1">
                                  {inputList.length !== 1 && (
                                    <button
                                      onClick={() => handleRemoveClick(i)}
                                      className="btn btn-danger"
                                    >
                                      <i class="fas fa-minus"></i>
                                    </button>
                                  )}
                                </div>
                              </div>
                            </td>
                          </tr>
                        </>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
            <div className="text-center">
              <button onClick={handleClickCrear} className="btn btn-primary">
                <i class="fas fa-check"></i> Guardar
              </button>
            </div>
          </form>
          <div className="container">
            <button className="btn btn-primary">
              <Link to="/dashboard/competencias-organizacionales-preguntas">
                Crear Preguntas
              </Link>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
export default Teletrabajo;

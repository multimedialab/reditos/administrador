import React, { useState, useEffect } from "react";
import ReactExport from "react-data-export";
import {
  getLeadersByCompany,
  getCollaboratorsByLeader,
} from "../Services/apiRelationships";
import {
  getCategories,
  getValuesByCategory,
} from "../Services/apiResponseCategory";
import { getCompanies } from "../Services/apiResponseCompany";
import { getTracingByCollaborator } from "../Services/apiTracing";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ConsultaEvaluacion = () => {
  useEffect(() => {
    handleGetCompanies();
    getCategories();
  }, []);

  const [companies, setCompanies] = useState([]);
  const [leaders, setLeaders] = useState([]);
  const [collaborators, setCollaborators] = useState([]);

  const [categoriesTracing, setCategoriesTracing] = useState([]);
  const [tracing, setTracing] = useState([]);
  const [tracingByCategory, setTracingByCategory] = useState([]);
  const [valuesTracing, setValuesTracing] = useState([]);

  const [tracingByValues, setTracingByValues] = useState([]);

  const [data, setData] = useState([]);
  const [nameCol, setNameCol] = useState("");
  const [nameCategory, setNameCategory] = useState("");
  const [nameValue, setNameValue] = useState("");

  const [nameLeader, setNameLeader] = useState("");


  const [dataGeneralExcel, setDataGeneralExcel] = useState([]);
  const [dataGeneralCompanyExcel, setDataGeneralCompanyExcel] = useState([]);
  const [companyName, setCompanyName] = useState('');


  const handleGetCompanies = async () => {
    const resp = await getCompanies();
    setCompanies(resp);
  };

  const handleGetLeaderByCompanies = async (id_company) => {
    const resp = await getLeadersByCompany(id_company);
    setLeaders(resp);
    setCompanyName(resp[0].company_name);

    let dataExcel = [];

    resp.forEach((leader) => {
      leader.collaboratorList.forEach((col) => {
        col.tracingList.forEach((tracingList) => {
          tracingList.tracingItemsList.forEach((tracingItemList) => {
            console.log("tracingItemList => ", tracingItemList);
            dataExcel.push({
              "Empresa": leader.company_name,
              "Nombre Lider": leader.leaderName,
              "Nombre Colaborador": col.completeName,
              Categoria: tracingItemList.categoryName,
              Valor: tracingItemList.valueName,
              Indicador: tracingItemList.itemName,
              Fortaleza: tracingItemList.strengths,
              "Oportunidad de Mejora": tracingItemList.improveChange,
              Concepto: tracingItemList.concept,
              Peso: tracingItemList.weigth,
            });
          });
        });
      });
    });

    setDataGeneralCompanyExcel(dataExcel);




  };

  const handleGetCollaboratorsByLeader = async (item) => {
    let jsonData = JSON.parse(item);
    const resp = await getCollaboratorsByLeader(jsonData.id);

    setCollaborators(resp);
    setNameLeader(jsonData.leaderName);

    let dataExcel = [];
    
    resp.forEach((col) => {
      console.log("data => ", col);

      col.tracingList.forEach((tracingList) => {
        tracingList.tracingItemsList.forEach((tracingItemList) => {
          console.log("tracingItemList => ", tracingItemList);
          dataExcel.push({
            "Nombre Colaborador": col.completeName,
            "Nombre Lider": col.leader_name,
            Categoria: tracingItemList.categoryName,
            Valor: tracingItemList.valueName,
            Indicador: tracingItemList.itemName,
            Fortaleza: tracingItemList.strengths,
            "Oportunidad de Mejora": tracingItemList.improveChange,
            Concepto: tracingItemList.concept,
            Peso: tracingItemList.weigth,
          });
        });
      });
    });

    setDataGeneralExcel(dataExcel);

    console.log("dataExcel => ", dataExcel);
  };

  const handleGetTracingByCollaborator = async (item) => {
    let jsonData = JSON.parse(item);
    setNameCol(jsonData.completeName);
    const resp = await getTracingByCollaborator(jsonData.id);
    if (resp) {
      handleGetCategories();
      setTracing(resp[0].tracingItemsList);
    } else {
      setTracing([]);
    }
  };

  const handleGetCategories = async () => {
    const resp = await getCategories();
    setCategoriesTracing(resp);
  };

  /* const handleGetValues = async (idCategory) => {
    const resp = await getValuesByCategory(idCategory);
    setValuesTracing(resp);


  } */

  const handleUpdateTracingCategories = async (item) => {
    console.log(item);
    let jsonData = JSON.parse(item);
    setNameCategory(jsonData.categoryName);
    var dataCategoriesCompEsp = [];
    dataCategoriesCompEsp = tracing.filter(
      (x) => x.categoryName === jsonData.categoryName
    );
    console.log(dataCategoriesCompEsp);
    setTracingByCategory(dataCategoriesCompEsp);

    const resp = await getValuesByCategory(jsonData.id);
    setValuesTracing(resp);
  };

  const handleUpdateTracingValues = (nameValue) => {
    var dataValues = [];
    console.log(tracingByCategory);
    dataValues = tracingByCategory.filter((x) => x.valueName === nameValue);
    console.log("dataValues => ", dataValues);
    setTracingByValues(dataValues);
    setNameValue(nameValue);

    const dataExcel = [];

    dataValues.forEach((element, i) => {
      let objExcel = {
        indicador: dataValues[i].itemName,
        fortaleza: dataValues[i].strengths,
        oportMejora: dataValues[i].improveChange,
        concept: dataValues[i].concept,
        weigth: dataValues[i].weigth,
      };
      dataExcel.push(objExcel);
      setData(dataExcel);
    });
  };

  return (
    <>
      <div className="container p-5 mt-5">
        <h2 id="title">Consulta de Evaluación</h2>
        <div className="row mt-5">
          <div className="col-2">
            <h5>Empresa</h5>
          </div>
          <div className="col-5">
            <select
              onChange={(e) => handleGetLeaderByCompanies(e.target.value)}
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>
              {companies.map((item) => (
                <option value={item.id}> {item.companyName} </option>
              ))}
            </select>
          </div>

          {dataGeneralCompanyExcel.length !== 0 && (
            <ExcelFile
              filename={`${companyName} - Reporte Seguimientos ( EMPRESA )`}
              element={
                <button
                  onClick={() => console.log("data excel => ", data)}
                  className="btn btn-success"
                >
                  Descargar Reporte General - Empresa
                </button>
              }
            >
              <ExcelSheet data={dataGeneralCompanyExcel} name={""}>
                {dataGeneralCompanyExcel.length !== 0 &&
                  Object.keys(dataGeneralCompanyExcel[0]).map((key, index) => (
                    <ExcelColumn
                      key={index.toString()}
                      label={key}
                      value={key}
                      style={{ font: { sz: "29", bold: true } }}
                    />
                  ))}
              </ExcelSheet>
            </ExcelFile>
          )}


        </div>

        <div className="row mt-5">
          <div className="col-2">
            <h5>Lideres</h5>
          </div>

          <div className="col-5">
            <select
              onChange={(e) => handleGetCollaboratorsByLeader(e.target.value)}
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>

              {leaders.map((item) => (
                <option value={JSON.stringify(item)}>
                  {" "}
                  {item.leaderName}{" "}
                </option>
              ))}
            </select>
          </div>

          {dataGeneralExcel.length !== 0 && (
            <ExcelFile
              filename={`${nameLeader} - Reporte Seguimientos ( COLABORADORES )`}
              element={
                <button
                  onClick={() => console.log("data excel => ", data)}
                  className="btn btn-success"
                >
                  Descargar Reporte General - Lider
                </button>
              }
            >
              <ExcelSheet data={dataGeneralExcel} name={""}>
                {dataGeneralExcel.length !== 0 &&
                  Object.keys(dataGeneralExcel[0]).map((key, index) => (
                    <ExcelColumn
                      key={index.toString()}
                      label={key}
                      value={key}
                      style={{ font: { sz: "29", bold: true } }}
                    />
                  ))}
              </ExcelSheet>
            </ExcelFile>
          )}
        </div>

        <div className="row mt-5">
          <div className="col-2">
            <h5>Colaboradores</h5>
          </div>
          <div className="col-5">
            <select
              onChange={(e) => handleGetTracingByCollaborator(e.target.value)}
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>
              {collaborators.map((item) => (
                <option value={JSON.stringify(item)}>
                  {" "}
                  {item.completeName}{" "}
                </option>
              ))}
            </select>
          </div>
        </div>

        <br />
        <h5>Selecciona una categoria:</h5>
        <select
          onChange={(e) => handleUpdateTracingCategories(e.target.value)}
          className="form-control"
          name=""
          id=""
        >
          <option value="">Selecciona una categoria de desempeño</option>
          {categoriesTracing.map((item) => (
            <>
              {item.categoryName !== "OBJETIVOS" ? (
                <option value={JSON.stringify(item)}>
                  {item.categoryName}
                </option>
              ) : null}
            </>
          ))}
        </select>
        <br />
        <h5>Selecciona un valor:</h5>
        <select
          onChange={(e) => handleUpdateTracingValues(e.target.value)}
          className="form-control"
          name=""
          id=""
        >
          <option value="">Selecciona un valor de desempeño</option>
          {valuesTracing.map((item) => (
            <option value={item.nameValue}> {item.nameValue} </option>
          ))}
        </select>

        <hr />
        {tracingByValues.length !== 0 ? (
          <>
            <div className="text-center">
              <h4 id="title">EVALUACIÓN DE DESEMPEÑO</h4>
            </div>

            <div className="row mt-5">
              <table
                className="table"
                style={{
                  backgroundColor: "white",
                  border: "2px solid #004F9E",
                }}
              >
                <thead>
                  <tr>
                    <th> # </th>
                    <th style={{ color: "#004F9E", textAlign: "center" }}>
                      Indicador
                    </th>

                    <th> Fortaleza </th>
                    <th> Oportunidades de mejora </th>
                    <th> Concepto </th>
                    <th> Peso </th>
                  </tr>
                </thead>
                <tbody>
                  {tracingByValues.map((item, index) => (
                    <>
                      <tr>
                        <td> {index + 1} </td>

                        <td> {item.itemName} </td>
                        <td> {item.strengths} </td>
                        <td> {item.improveChange} </td>
                        <td> {item.concept} </td>

                        <td> {item.weigth} </td>
                      </tr>
                    </>
                  ))}
                </tbody>
              </table>
              {/* <ExcelFile
                element={
                  <button
                    onClick={() => console.log("data excel => ", data)}
                    className="btn btn-success"
                  >
                    Descargar Reporte
                  </button>
                }
                filename={`${nameCategory} - ${nameValue} - ${nameCol} `}
              >
                <ExcelSheet data={data} name={""}>
                  
                  <ExcelColumn
                    label="Indicador"
                    value="indicador"
                    style={{ font: { sz: "29", bold: true } }}
                  />
                  <ExcelColumn
                    label="Fortaleza"
                    value="fortaleza"
                    style={{ font: { sz: "29", bold: true } }}
                  />
                  <ExcelColumn
                    label="Oportunidades de mejora"
                    value="oportMejora"
                    style={{ font: { sz: "29", bold: true } }}
                  />
                  <ExcelColumn
                    label="Concepto"
                    value="concept"
                    style={{ font: { sz: "29", bold: true } }}
                  />
                  <ExcelColumn
                    label="Peso"
                    value="weigth"
                    style={{ font: { sz: "29", bold: true } }}
                  />
                </ExcelSheet>
              </ExcelFile> */}
            </div>
          </>
        ) : (
          <div className="text-center">
            {" "}
            Para visualizar un checklist realiza el filtro{" "}
          </div>
        )}
      </div>
    </>
  );
};
export default ConsultaEvaluacion;

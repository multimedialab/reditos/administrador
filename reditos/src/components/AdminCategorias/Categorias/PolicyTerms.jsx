import React, { useState, useEffect, useCallback } from "react";
import { getPolicyTerms, UpdatePolicyTerms } from "../Services/apiPolicy&Terms";
import "./styleAdmin.css";
import Swal from 'sweetalert2';

export default function PolicyTerms() {
  const initialState = {
    policyTextCollaborator: "",
  };

  const [state, setState] = useState(initialState);
  const [id, setId] = useState("");

  const getData = useCallback(async () => {
    await getPolicyTerms().then((resp) => {
      setId(resp[0].id);

      setState({
        policyTextCollaborator: resp[0].policyTextCollaborator,
      });

      console.log("resp => ", resp);
    });
  }, []);

  useEffect(() => {
    getData();
  }, []);

  const handleChange = (e) => {
    setState({
      ...state,
      [e.target.name]: e.target.value,
    });

    console.log(state);
  };

  const saveMessage = async () => {

    await UpdatePolicyTerms(id, state);
    Swal.fire({
        
        icon: 'success',
        title: 'Guardado con Exito!',
        showConfirmButton: false,
        timer: 1500
      })


  };

  return (
    <div className="adminCategoria">
      <h2 id="title">Aviso de Tratamiento de Datos</h2>

      <div
        style={{
          display: "flex",
          width: "100",
          justifyContent: "center",
          flexWrap: "wrap",
        }}
      >
        
        <div>
          <textarea
            onChange={(e) => handleChange(e)}
            value={state.policyTextCollaborator}
            style={{ maxHeight: 250 }}
            name="policyTextCollaborator"
            id=""
            cols="100"
            rows="10"
          ></textarea>
        </div>
        <div
          style={{ display: "flex", justifyContent: "center", width: "100%" }}
        >
          <button onClick={() => saveMessage()} className="btn btn-primary">
            Guardar Cambios
          </button>
        </div>
      </div>
    </div>
  );
}

import React, { useState, useEffect, useRef } from "react";
import axios from "axios";

import Swal from "sweetalert2";
import { apiToken } from "../Services/apiResponseLeaders";
import { deletePosition } from "../Services/apiResponsePositions";

import { Form, Button } from "react-bootstrap";

import { url, body } from "../Services/auth";

import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

import './styleAdmin.css';

const Cargos = () => {
  const initialState = {
    name_office: "",
    category_office: 0,
    category_position: 0,
  };

  const [values, setValues] = useState(initialState);

  const [info, setInfo] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [desCat, setDesCat] = useState("");
  const [isConsulted, setIsConsulted] = useState(false);
  const [leader, setLeaders] = useState([]);
  const [company, setCompany] = useState([]);

  const [isUpdated, setIsUpdated] = useState(false);

  const [leaderNam, setLeaderNam] = useState("");

  const [company_name, setCompanyName] = useState("");

  const [identification, setIdentification] = useState("");
  const [idLeader, setIdLeader] = useState("");

  //state combo box
  const [value, setValue] = useState(company[0]);
  const [inputValue, setInputValue] = useState("");

  const [display, setDisplay] = useState(false);
  const [options, setOptions] = useState([]);
  const [search, setSearch] = useState("");

  const wrapperRef = useRef(null);

  const setCompanyDex = (comp) => {
    setSearch(comp);
    setDisplay(false);
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  const handleClickOutside = (event) => {
    const { current: wrap } = wrapperRef;
    if (wrap && !wrap.contains(event.target)) {
      setDisplay(false);
    }
  };

  const api = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };

  //GetPositions

  const getPositions = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/positions/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        setLeaders(json.results);
        console.log(json.results);
      })
      .catch((error) => console.error(error));
  };

  //OnChange Inputs

  const handleOnChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
    console.log(e.target.value);
    console.log("values", values);
  };

  //OnSubmit Inputs

  const handleSubmit = (e) => {
    console.log(values);

    e.preventDefault();

    document.getElementById("form").reset();

    setValues({ ...initialState });
  };

  const handleClickGuardar = () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleClickEliminar = () => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Eliminado!");
      }
    });
  };

  const handleClickConsultar = () => {
    //Swal.fire('Consultando...')
  };

  useEffect(() => {
    handleClickTodos();
    getPositions();
  }, []);

  const handleClickTodos = () => {
    getPositions();
    setIsConsulted(true);
    /*
    if (Object.keys(info).length !== 0){
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'No hay registros',
        showConfirmButton: false,
        timer: 3000
      });
    }*/
  };

  const handleClickLimpiar = () => {
    setIsConsulted(false);
  };

  const handleClickCrear = async () => {
    const token = await api();

    if (Object.keys(values).length !== 0) {
      fetch(`${url.urlBase}/positions/`, {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${token}`,
        },
      })
        .then((response) => response.json())
        .then((json) => {
          getPositions();
          setIsConsulted(true);
          Swal.fire({
            icon: "success",
            title: "Creado con exito",
            showConfirmButton: false,
            timer: 1500,
          });
          setValues({ ...initialState });
        })
        .catch((error) => console.error(error));
    } else {
      Swal.fire({
        icon: "error",
        title: "Los campos no pueden estar vacios",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };

  const handleClickActualizar = async () => {
    const token = await apiToken();
    const response = await axios.put(
      `${url.urlBase}/positions/${idLeader}/`,
      values,
      { headers: { Authorization: `JWT ${token}` } }
    );
    console.log(response);
    handleClickTodos();

    Swal.fire({
      title: "Actualizado con exito",
      icon: "info",
    });
  };

  const handleClickCancel = () => {
    Swal.fire({
      title: "Cancelando...",
      icon: "warning",
    });
  };

  const handleDelete = async (id) => {
    console.log(id);
    console.log("DELETE ID: ", id);

    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#ccc",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        deletePosition(id);
        Swal.fire("Eliminado!");
      }
    });
  };

  const updateInfoCat = async (id, name, category, category_pos) => {
    const values = {
      name_office: name,
      category_office: category,
      category_position: category_pos,
    };

    setIsUpdated(true);
    setIdLeader(id);
    setValues(values);
    //setCompanyName(company_name);
    console.log("Values: ", values);
  };

  return (
    <>
      <div className="">
        <div className="logoImg "></div>
        <div className="adminCategoria">
          <h2 id="title">
            Crear Cargos
          </h2>
          <br />

          {/* <div className="container p-5">
            <div>{`name: '${values.name_office}', category office: '${values.category_office}', category position: '${values.category_position}'`}</div>
          </div> */}

          <form id="form" action="" onSubmit={handleSubmit}>
            <div className="admin">
              <div className="col">
                <label>Descripcion de Oficio</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="name_office"
                  value={values.name_office}
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Categoria</label>
              </div>
              <div className="col">
                <select
                  onChange={handleOnChange}
                  className="form-control"
                  name="category_office"
                  value={values.category_office}
                >
                  <option value={0}>ADMINISTRATIVO ADMON</option>
                  <option value={1}>ADMINISTRATIVO COMERCIAL</option>
                  <option value={2}>ASESOR COMERCIAL</option>
                  <option value={3}>GANASERVICIOS</option>
                </select>
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Categoria de Cargo</label>
              </div>

              <div className="col">
                <select
                  onChange={handleOnChange}
                  className="form-control"
                  name="category_position"
                  value={values.category_position}
                >
                  <option value={0}>OPERATIVO</option>
                  <option value={1}>TÁCTICO</option>
                  <option value={2}>ESTRATÉGICO</option>
                </select>
              </div>
            </div>

            <div className="botones col-lg-12 col-md-12 col-sm-12">
              <div className="col">
                <Button
                  className="btn btn-info"
                  
                  onClick={handleClickTodos}
                  type="button"
                >
                  <i className="fas fa-eye mr-2"></i>
                  VER LISTADO
                </Button>
              </div>

              {/* <div className="col">
                <Button
                  className="btnLimpiar"
                  id="btnAdmin"
                  onClick={handleClickLimpiar}
                  type="button"
                >
                  Limpiar
                </Button>
              </div> */}

              <div className="col">
                <Button
                  className="btn btn-primary"
                  
                  onClick={handleClickCrear}
                  type="submit"
                >
                  <i className="fas fa-check mr-2"></i>
                  GUARDAR
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btn btn-success"
                  
                  onClick={handleClickActualizar}
                  type="button"
                >
                  <i className="fas fa-pencil-alt mr-2"></i>
                  ACTUALIZAR 
                </Button>
              </div>

              {/* <div className="col">
                <Button
                  className="btnGuardar"
                  id="btnAdmin"
                  onClick={handleClickGuardar}
                  type="submit"
                >
                  Guardar
                </Button>
              </div> */}
            </div>

            {/* <div ref={wrapperRef} className="flex-container flex-column pos-rel mt-3">
              <div className="row">
                <div className="col-4">
                  <input name="id_company" onChange={event => setSearch(event.target.value)} value={search} type="text" onClick={() => setDisplay(!display)}  className="form-control" placeholder="type to search" />
                  {display && (
                    <div  className="autoContainer">
                      {options
                      .filter( ({companyName}) => companyName.indexOf(search.toUpperCase()) > -1 )
                      .map((v, i) => {
                        return <div tabIndex="0" onClick={() => setCompanyDex(v.companyName)}  className="card" key={i}>
                          <span style={{cursor: 'pointer'}}>{v.companyName}</span>
                        </div>
                      })}
                    </div>
                  )}
                </div>
              </div>
            </div> */}
          </form>
          {isConsulted ? (
            <div
              className="card-body ml-5 mr-3 mt-5"
              style={{
                height: "40vh",
                overflow: "scroll",
                overflowX: "hidden",
                top: 80,
                position: "relative",
              }}
            >
              <table
                className="table mt-5"
                style={{ border: "2px solid #004F9E" }}
              >
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Cargo (Descripcion de oficio)</th>
                    <th>Categoria</th>
                    <th>Categoria de cargo</th>
                    <th>Opciones</th>
                  </tr>
                </thead>

                <tbody>
                  {leader.map((item, i) => (
                    <tr key={i}>
                      <td>{i + 1} </td>
                      <td> {item.name_office} </td>
                      <td> {item.office_name} </td>
                      <td> {item.position_name} </td>
                      <td>
                        <button
                          onClick={() =>
                            updateInfoCat(
                              item.id,
                              item.name_office,
                              item.category_office,
                              item.category_position,
                              item.url
                            )
                          }
                          className="btn btn-info"
                        >
                          <i class="fas fa-edit"></i>
                        </button>{" "}
                        <button
                          onClick={() => handleDelete(item.id)}
                          className="btn btn-danger"
                        >
                          <i class="fas fa-trash-alt"></i>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </div>
    </>
  );
};
export default Cargos;

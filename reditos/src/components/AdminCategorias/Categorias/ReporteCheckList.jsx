import React, { useEffect, useState } from "react";
import ReactExport from "react-data-export";
import Swal from "sweetalert2";
import "./styleAdmin.css";
import {
  getLeadersByCompany,
  getCollaboratorsByLeader,
} from "../Services/apiRelationships";
import { getCompanies } from "../Services/apiResponseCompany";
import { getCheckListByCollaborator } from "../Services/apiCheckList";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ReporteCheckList = (props) => {
  const [companies, setCompanies] = useState([]);
  const [leaders, setLeaders] = useState([]);
  const [collaborators, setCollaborators] = useState([]);
  const [checklist, setChecklist] = useState([]);
  const [nameLeader, setNameLeader] = useState("");
  const [nameCollaborator, setNameCollaborator] = useState("");

  const [dataGeneralExcel, setDataGeneralExcel] = useState([]);
  const [dataGeneralCompanyExcel, setDataGeneralCompanyExcel] = useState([]);
  const [companyName, setCompanyName] = useState('');


  const [data, setData] = useState([]);

  const handleGetCompanies = async () => {
    const resp = await getCompanies();
    setCompanies(resp);
  };

  const handleGetLeaderByCompanies = async (id_company) => {
    const resp = await getLeadersByCompany(id_company);

    console.log('resp company => ', resp[0].company_name);
    setCompanyName(resp[0].company_name);
    setLeaders(resp);

    let dataExcel = [];
    resp.forEach((leader) => {
      leader.collaboratorList.forEach((col) => {
        col.answersList.forEach((ans) => {
          dataExcel.push({
            "Empresa": leader.company_name,
            "Nombre Colaborador": col.completeName,
            "Nombre Lider": col.leader_name,
            Pregunta: ans.name_question,
            Respuesta: ans.isChecked,
            Periodo: ans.period,
            Año: ans.year,
          });
        });
      });
    });

    setDataGeneralCompanyExcel(dataExcel);


  };

  const handleGetCollaboratorsByLeader = async (item) => {
    let jsonData = JSON.parse(item);
    const resp = await getCollaboratorsByLeader(jsonData.id);

    setCollaborators(resp);
    setNameLeader(jsonData.leaderName);

    let dataExcel = [];

    resp.forEach((col) => {
      col.answersList.forEach((ans) => {
        dataExcel.push({
          "Nombre Colaborador": col.completeName,
          "Nombre Lider": col.leader_name,
          Pregunta: ans.name_question,
          Respuesta: ans.isChecked,
          Periodo: ans.period,
          Año: ans.year,
        });
      });
    });

    setDataGeneralExcel(dataExcel);

    console.log("dataExcel => ", dataExcel);
  };

  const handleGetCheckListByCollaborator = async (item) => {
    let jsonData = JSON.parse(item);
    const resp = await getCheckListByCollaborator(jsonData.id);
    setNameCollaborator(jsonData.completeName);

    if (resp) {
      setChecklist(resp);

      const dataExcel = [];

      resp.forEach((element, i) => {
        let objExcel = {
          "Nombre Colaborador": jsonData.completeName,
          "Nombre Lider": jsonData.leader_name,
          Pregunta: resp[i].name_question,
          Respuesta: resp[i].isChecked,
          Periodo: resp[i].period,
          Año: resp[i].year,
        };
        dataExcel.push(objExcel);
      });
      setData(dataExcel);

      console.log("data => ", dataExcel);
    } else {
      setChecklist([]);
    }
  };

  useEffect(() => {
    handleGetCompanies();
  }, []);

  return (
    <>
      <div className="container p-5 mt-5">
        <h2 id="title">Reporte de CheckList</h2>
        <div className="row mt-5">
          <div className="col-2">
            <h5>Empresa</h5>
          </div>
          <div className="col-5">
            <select
              onChange={(e) => handleGetLeaderByCompanies(e.target.value)}
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>
              {companies.map((item) => (
                <option value={item.id}> {item.companyName} </option>
              ))}
            </select>
          </div>

          {dataGeneralCompanyExcel.length !== 0 && (
            <ExcelFile
              filename={`${companyName} - Reporte CheckList ( EMPRESA )`}
              element={
                <button
                  onClick={() => console.log("data excel => ", data)}
                  className="btn btn-success"
                >
                  Descargar Reporte General - Empresa
                </button>
              }
            >
              <ExcelSheet data={dataGeneralCompanyExcel} name={""}>
                {dataGeneralCompanyExcel.length !== 0 &&
                  Object.keys(dataGeneralCompanyExcel[0]).map((key, index) => (
                    <ExcelColumn
                      key={index.toString()}
                      label={key}
                      value={key}
                      style={{ font: { sz: "29", bold: true } }}
                    />
                  ))}
              </ExcelSheet>
            </ExcelFile>
          )}


        </div>

        <div className="row mt-5">
          <div className="col-2">
            <h5>Lideres</h5>
          </div>
          <div className="col-5">
            <select
              onChange={(e) => handleGetCollaboratorsByLeader(e.target.value)}
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>

              {leaders.map((item) => (
                <option value={JSON.stringify(item)}>
                  {" "}
                  {item.leaderName}{" "}
                </option>
              ))}
            </select>
          </div>

          {dataGeneralExcel.length !== 0 && (
            <ExcelFile
              element={
                <button className="btn btn-success">
                  Descargar Reporte General
                </button>
              }
              filename={`${nameLeader} - Reporte CheckList ( COLABORADORES )`}
            >
              <ExcelSheet data={dataGeneralExcel} name="Checklist">
                {dataGeneralExcel.length !== 0 &&
                  Object.keys(dataGeneralExcel[0]).map((key, index) => (
                    <ExcelColumn
                      key={index.toString()}
                      label={key}
                      value={key}
                      style={{ font: { sz: "29", bold: true } }}
                    />
                  ))}
              </ExcelSheet>
            </ExcelFile>
          )}
        </div>

        <div className="row mt-5">
          <div className="col-2">
            <h5>Colaboradores</h5>
          </div>
          <div className="col-5">
            <select
              onChange={(e) => handleGetCheckListByCollaborator(e.target.value)}
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>
              {collaborators.map((item) => (
                <option value={JSON.stringify(item)}>
                  {" "}
                  {item.completeName}{" "}
                </option>
              ))}
            </select>
          </div>
        </div>

        <hr />
        {checklist.length !== 0 ? (
          <>
            <div className="text-center">
              <h4 id="title">CHECKLIST</h4>
            </div>

            <div className="row mt-5">
              <table
                className="table"
                style={{
                  backgroundColor: "white",
                  border: "2px solid #004F9E",
                }}
              >
                <thead>
                  <tr>
                    <th> # </th>

                    <th style={{ color: "#004F9E", textAlign: "center" }}>
                      Pregunta
                    </th>

                    <th>Respuesta</th>
                  </tr>
                </thead>
                <tbody>
                  {checklist.map((item, index) => (
                    <>
                      <tr>
                        <td> {index + 1} </td>
                        <td style={{ textAlign: "center" }}>
                          {" "}
                          {item.name_question}{" "}
                        </td>
                        <td>
                          {" "}
                          {item.isChecked ? (
                            <span className="badge badge-success p-3">
                              Acuerdo
                            </span>
                          ) : (
                            <span className="badge badge-danger p-2">
                              Desacuerdo
                            </span>
                          )}
                        </td>
                      </tr>
                    </>
                  ))}
                </tbody>
              </table>
              <ExcelFile
                element={
                  <button className="btn btn-success">Descargar Reporte</button>
                }
                filename={`Reporte CheckList - ${nameCollaborator}`}
              >
                <ExcelSheet data={data} name="Checklist">
                  {data.length !== 0 &&
                    Object.keys(data[0]).map((key, index) => (
                      <ExcelColumn
                        key={index.toString()}
                        label={key}
                        value={key}
                        style={{ font: { sz: "29", bold: true } }}
                      />
                    ))}
                </ExcelSheet>
              </ExcelFile>
            </div>
          </>
        ) : (
          <div className="text-center">
            {" "}
            Para visualizar un checklist realiza el filtro{" "}
          </div>
        )}
      </div>
    </>
  );
};
export default ReporteCheckList;

import React, { useState ,useEffect } from "react";
import { Form, Button } from "react-bootstrap";

import Swal from "sweetalert2";

import { toast } from "react-toastify";

import {url, body} from '../Services/auth';
import {apiToken} from '../Services/apiResponseLeaders';

import {createDate, getDate, updateData, deleteData} from '../Services/apiDate';

import './styleAdmin.css';

const AdministrarFechas = () => {
  
  useEffect(() => {

    getInfo();
  }, []);


  const initialState = {
    initDateTracingOne: '',
    endDateTracingOne: '',
    initDateTracingTwo: '',
    endDateTracingTwo: ''
  }
  
  const [valuesDate, setValuesDate] = useState(initialState);
  const [state, setstate] = useState([]);
  const [idDate, setIdDate] = useState("");



  const handleClickGuardar = async () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });

    await createDate(valuesDate);

    console.log(valuesDate);

    getInfo();
    
  };

  const handleClickActualizar = async () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });

    await updateData(idDate,valuesDate);



    console.log(valuesDate);
  };



  const getInfo = async () => {

    const resp = await getDate();

    setstate(resp);

    console.log('resp => ', resp);


  } 

  const updateInfoCat = async (id, initDateTracingOne, endDateTracingOne, initDateTracingTwo, endDateTracingTwo) => {
    const values = {
      initDateTracingOne: initDateTracingOne,
      endDateTracingOne: endDateTracingOne,
      initDateTracingTwo: initDateTracingTwo,
      endDateTracingTwo: endDateTracingTwo,
    };
    //setIsUpdated(true);
    setIdDate(id);
    setValuesDate(values);
    //setCompanyName(company_name);
    console.log("Values: ", values);
  };

  const handleDelete = async (id) => {

    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        deleteData(id);

        //Swal.fire("Eliminado!");
        getInfo();
      }
    });





  }



  const handleChange = e => {

    setValuesDate({
      ...valuesDate,
      [e.target.name]: e.target.value,
    });
    console.log(valuesDate);

  }

  

  return (
    <>
      <div className="container p-5 mt-5">
        <h2 id="title">
          
            Configuracion de Fechas Seguimientos
          
        </h2>

        
        <br />

        <div className="row mt-5">
          <div className="col-2">1° Seguimiento</div>
          <div className="col-2">
            <div className="text-center">
              <Form>
                <Form.Label style={{ float: "right" }}>
                  Disponible Desde
                </Form.Label>
              </Form>
            </div>
          </div>

          <div className="col-3">
            <Form>
              <input value={valuesDate.initDateTracingOne} onChange={handleChange} className="form-control" type="date" name="initDateTracingOne" id="" />
            </Form>
          </div>

          <div className="col-2">
            <div className="text-center">
              <Form>
                <Form.Label style={{ float: "right" }}>Hasta</Form.Label>
              </Form>
            </div>
          </div>

          <div className="col-3">
            <Form>
              <input value={valuesDate.endDateTracingOne} onChange={handleChange} className="form-control" type="date" name="endDateTracingOne" id="" />
            </Form>
          </div>
        </div>

        <br />

        <div className="row">
          <div className="col-2">2° Seguimiento</div>
          <div className="col-2">
            <div className="text-center">
              <Form>
                <Form.Label style={{ float: "right" }}>
                  Disponible Desde
                </Form.Label>
              </Form>
            </div>
          </div>

          <div className="col-3">
            <Form>
              <input value={valuesDate.initDateTracingTwo} onChange={handleChange} className="form-control" type="date" name="initDateTracingTwo" id="" />
            </Form>
          </div>

          <div className="col-2">
            <div className="text-center">
              <Form>
                <Form.Label style={{ float: "right" }}>Hasta</Form.Label>
              </Form>
            </div>
          </div>

          <div className="col-3">
            <Form>
              <input value={valuesDate.endDateTracingTwo} onChange={handleChange} className="form-control" type="date" name="endDateTracingTwo" id="" />
            </Form>
          </div>
        </div>

        <div className="row">
        <div className="text-center mt-5">
          <div className="col">
            <Button onClick={handleClickGuardar} className="btn btn-primary">
              <i className="fa fa-check mr-2"></i>
              GUARDAR
            </Button>
          </div>
        </div>
        <div className="text-center mt-5">
          <div className="col">
            <Button onClick={handleClickActualizar} className="btn btn-success">
            <i class="fas fa-pencil-alt mr-2"></i>
              ACTUALIZAR
            </Button>
          </div>
        </div>

        </div>


        <div
              className="card-body ml-5 mr-3 mt-5"
              style={{
                height: "40vh",
                overflow: "scroll",
                overflowX: "hidden",
                top: 80,
                position: "relative",
              }}
            >
              <table
                className="table mt-5"
                style={{ border: "2px solid #004F9E" }}
              >
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Fecha Inicio - 1° Seguimiento</th>
                    <th>Fecha Fin - 1° Seguimiento</th>
                    <th>Fecha Inicio - 2° Seguimiento</th>
                    <th>Fecha Fin - 2° Seguimiento</th>
                    <th>Opciones</th>

                  </tr>
                </thead>

                <tbody>
                  {state.map((item, i) => (
                    <>
                      {item.leader !== null ? (
                        <>
                          <tr key={i}>
                            <td>{i + 1} </td>
                            <td> {item.initDateTracingOne} </td>
                            <td> {item.endDateTracingOne} </td>
                            <td> {item.initDateTracingTwo} </td>
                            <td> {item.endDateTracingTwo} </td>
                            
                            
                            <td>
                              <button
                                onClick={() =>
                                  updateInfoCat(
                                    item.id,
                                    item.initDateTracingOne,
                                    item.endDateTracingOne,
                                    item.initDateTracingTwo,
                                    item.endDateTracingTwo,
                                    item.url,
                                  )
                                }
                                className="btn btn-info"
                              >
                                <i class="fas fa-edit"></i>
                              </button>{" "}
                              <button
                                onClick={() => handleDelete(item.id)}
                                className="btn btn-danger"
                              >
                                <i class="fas fa-trash-alt"></i>
                              </button>
                            </td>
                          </tr>
                        </>
                      ) : null

                      }

                    </>
                  ))}
                </tbody>
              </table>
            </div>


      </div>
    </>
  );
};
export default AdministrarFechas;

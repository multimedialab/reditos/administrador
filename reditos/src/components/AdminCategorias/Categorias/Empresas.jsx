import React, { useState, useEffect } from "react";
import axios from "axios";
import { apiToken, deleteCompany } from "../Services/apiResponseCompany";
import { url, body } from "../Services/auth";
import Swal from "sweetalert2";
import { Form, Button } from "react-bootstrap";
import "./styleAdmin.css";

const Empresas = () => {
  const initialState = {
    companyName: "",
  };

  const [values, setValues] = useState(initialState);
  const [info, setInfo] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [desCat, setDesCat] = useState("");
  const [isConsulted, setIsConsulted] = useState(false);

  const [currentId, setCurrentId] = useState("");

  const [isUpdated, setIsUpdated] = useState(false);
  const [idCompany, setidCompany] = useState("");

  const consult = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/company/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => setInfo(json.results))
      .catch((error) => console.error(error));
  };

  const getCompanyById = async (id) => {
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    const response = await axios.get(`${url.urlBase}/company/${id}/`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${token}`,
      },
    });

    const data = response.data;

    setValues(data);
  };

  const updateCompany = async (id, name) => {
    const values = {
      companyName: name,
    };

    setidCompany(id);

    console.log("id: ", id);
    setIsUpdated(true);
    setValues(values);
  };

  const handleDelete = async (id) => {
    console.log(id);
    console.log("DELETE ID: ", id);

    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#ccc",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        deleteCompany(id);
        Swal.fire("Eliminado!");
        handleClickTodos();
      }
    });
  };

  //OnChange Inputs

  const handleOnChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };

  //OnSubmit Inputs

  const handleSubmit = (e) => {
    console.log(values);

    e.preventDefault();

    setValues({ ...initialState });
  };

  const handleClickGuardar = () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleClickEliminar = () => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Eliminado!");
      }
    });
  };

  const handleClickConsultar = () => {
    //Swal.fire('Consultando...')
  };

  useEffect(() => {
    handleClickTodos();
    console.log(info);

    getCompanyById(currentId);
  }, []);

  const handleClickTodos = () => {
    consult();
    setIsConsulted(true);
    /*
    if (Object.keys(info).length !== 0){
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'No hay registros',
        showConfirmButton: false,
        timer: 3000
      });
    }*/
  };

  const handleClickLimpiar = () => {
    setIsConsulted(false);
  };

  const handleClickCrear = async () => {
    const token = await apiToken();

    if (Object.keys(values).length !== 0) {
      fetch(`${url.urlBase}/company/`, {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${token}`,
        },
      })
        .then((response) => response.json())
        .then((json) => {
          consult();
          setIsConsulted(true);
          Swal.fire({
            icon: "success",
            title: "Creado con exito",
            showConfirmButton: false,
            timer: 1500,
          });
          setValues({ ...initialState });
        })
        .catch((error) => console.error(error));
    } else {
      Swal.fire({
        icon: "error",
        title: "Los campos no pueden estar vacios",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };

  const handleClickActualizar = async () => {
    const token = await apiToken();
    const response = await axios.put(
      `${url.urlBase}/company/${idCompany}/`,
      values,
      { headers: { Authorization: `JWT ${token}` } }
    );
    console.log(response);
    handleClickTodos();
    Swal.fire({
      title: "Actualizado con exito",
      icon: "info",
    });
  };

  const handleClickCancel = () => {
    Swal.fire({
      title: "Cancelando...",
      icon: "warning",
    });
  };

  return (
    <>
      <div className="body col-lg-12 col-md-12 col-sm-9 col-xs-9">
        <div className="logoImg "></div>
        <div className="adminCategoria">
          <h2 id="title">Empresas</h2>
          <br />
          <form action="" onSubmit={handleSubmit}>
            <div className="admin">
              <div className="col">
                <label>ID Empresa</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  disabled
                  type="number"
                  name="id"
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Nombre de la Empresa</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="companyName"
                  value={values.companyName}
                />
              </div>
            </div>

            <div className="botones col-lg-12 col-md-12 col-sm-12">
              {/* <div className="col">
                <Button className="btnConsulta" id="btnAdmin" type="button">
                  Consultar
                </Button>
              </div> */}

              <div className="col">
                <Button
                  className="btn btn-info"
                  onClick={handleClickTodos}
                  type="button"
                >
                  <i className="fas fa-eye mr-2"></i>
                  VER LISTADO
                </Button>
              </div>

              {/* <div className="col">
                <Button
                  className="btnLimpiar"
                  id="btnAdmin"
                  onClick={handleClickLimpiar}
                  type="button"
                >
                  Limpiar
                </Button>
              </div> */}

              <div className="col">
                <Button
                  className="btn btn-primary"
                  onClick={handleClickCrear}
                  type="submit"
                >
                  <i className="fas fa-check mr-2"></i>
                  GUARDAR
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btn btn-success"
                  onClick={handleClickActualizar}
                  type="button"
                >
                  <i class="fas fa-pencil-alt mr-2"></i>
                  ACTUALIZAR
                </Button>
              </div>

              {/* <div className="col">
                <Button
                  className="btnEliminar"
                  id="btnAdmin"
                  onClick={handleClickEliminar}
                  type="button"
                >
                  Eliminar
                </Button>
              </div> */}
              {/* <div className="col">
                <Button
                  className="btnGuardar"
                  id="btnAdmin"
                  onClick={handleClickGuardar}
                  type="submit"
                >
                  Guardar
                </Button>
              </div> */}

              {/* <div className="col">
                <Button
                  className="btnCancelar"
                  id="btnAdmin"
                  onClick={handleClickCancel}
                  type="button"
                >
                  Cancelar
                </Button>
              </div> */}
            </div>
          </form>
          {isConsulted ? (
            <div
              className="card-body ml-5 mr-3 mt-5"
              style={{
                height: "50vh",
                overflow: "scroll",
                overflowX: "hidden",
                top: 80,
                position: "relative",
              }}
            >
              <table
                className="table mt-3"
                style={{ border: "2px solid #ccc" }}
              >
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre de Empresa</th>
                    <th>Opciones</th>
                  </tr>
                </thead>

                <tbody>
                  {info.map((item, i) => (
                    <tr key={i}>
                      <td>{i + 1} </td>
                      <td> {item.companyName} </td>
                      <td>
                        <button
                          onClick={() =>
                            updateCompany(item.id, item.companyName)
                          }
                          className="btn btn-info"
                        >
                          <i class="fas fa-edit"></i>
                        </button>{" "}
                        <button
                          onClick={() => handleDelete(item.id)}
                          className="btn btn-danger"
                        >
                          <i class="fas fa-trash-alt"></i>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </div>

      
    </>
  );
};
export default Empresas;

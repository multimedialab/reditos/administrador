import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  apiToken,
  deleteQuestionChecklist,
} from "../Services/apiResponseChecklist";
import { url, body } from "../Services/auth";
import Swal from "sweetalert2";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./styleAdmin.css";

const ViewQuestions = () => {
  const initialState = {
    name: "",
    checklist: "",
  };

  const [values, setValues] = useState(initialState);
  const [info, setInfo] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [desCat, setDesCat] = useState("");
  const [isConsulted, setIsConsulted] = useState(false);

  const [currentId, setCurrentId] = useState("");

  const [isUpdated, setIsUpdated] = useState(false);
  const [idQuestion, setidQuestion] = useState("");

  const consult = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/question/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => setInfo(json.results))
      .catch((error) => console.error(error));
  };

  const updateQuestion = async (id, name, checklist) => {
    const values = {
      name: name,
      checklist: checklist,
    };

    console.log("values: ", values);

    setidQuestion(id);

    console.log("id: ", id);
    setIsUpdated(true);
    setValues(values);
  };

  const handleDelete = async (id) => {
    console.log(id);
    console.log("DELETE ID: ", id);

    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#ccc",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        deleteQuestionChecklist(id);
        Swal.fire("Eliminado!");
        handleClickTodos();
      }
    });
  };

  //OnChange Inputs

  const handleOnChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };

  //OnSubmit Inputs

  const handleSubmit = (e) => {
    console.log(values);

    e.preventDefault();

    setValues({ ...initialState });
  };

  const handleClickEliminar = () => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Eliminado!");
      }
    });
  };

  useEffect(() => {
    handleClickTodos();
    //consult();
    console.log(info);
  }, []);

  const handleClickTodos = () => {
    consult();
    setIsConsulted(true);
  };

  const handleClickLimpiar = () => {
    setIsConsulted(false);
  };

  const handleClickActualizar = async () => {
    console.log(values);

    if (values.name != "") {
      const token = await apiToken();
      const response = await axios.put(
        `${url.urlBase}/question/${idQuestion}/`,
        values,
        { headers: { Authorization: `JWT ${token}` } }
      );
      console.log(response);
      handleClickTodos();
      Swal.fire({
        title: "Actualizado con exito",
        icon: "info",
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "Los campos no pueden estar vacios",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };

  const handleClickCancel = () => {
    Swal.fire({
      title: "Cancelando...",
      icon: "warning",
    });
  };

  return (
    <>
      <div className="col-lg-12 col-md-12 col-sm-9 col-xs-9">
      <div className="p-3">
          <button className="btn btn-primary">
            <Link
              style={{ textDecoration: "none", color: "#fff" }}
              to="/dashboard/crear-preguntas-checklist"
            >
              <i class="fas fa-arrow-circle-left mr-2"></i>
              Volver
            </Link>
          </button>
        </div>
        <div className="logoImg "></div>
        <div className="">
          <h2 id="title" className="mt-5">
            Administrar Preguntas del CheckList
          </h2>
          <br />
          
          <br />
          <form action="" onSubmit={handleSubmit}>
            {/* <div className="admin">
              <div className="col">
                <label>ID Empresa</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  disabled
                  type="number"
                  name="id"
                />
              </div>
            </div> */}

            <div className="admin">
              <div className="col">
                <label>Nombre de Pregunta</label>
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="name"
                  value={values.name}
                />
              </div>
            </div>

            <div className="botones col-lg-12 col-md-12 col-sm-12">
              <div className="col">
                <Button
                  className="btn btn-info"
                  onClick={handleClickTodos}
                  type="button"
                >
                  <i className="fas fa-eye mr-2"></i>
                  VER LISTADO
                </Button>
              </div>

              {/* <div className="col">
                <Button
                  className="btnLimpiar"
                  id="btnAdmin"
                  onClick={handleClickLimpiar}
                  type="button"
                >
                  Limpiar
                </Button>
              </div> */}

              <div className="col">
                <Button
                  className="btn btn-success"
                  onClick={handleClickActualizar}
                  type="button"
                >
                  <i class="fas fa-pencil-alt mr-2"></i>
                  ACTUALIZAR
                </Button>
              </div>
            </div>
          </form>
          {isConsulted ? (
            <div
              className="card-body ml-5 mr-3 mt-5"
              style={{
                height: "50vh",
                overflow: "scroll",
                overflowX: "hidden",
                top: 80,
                position: "relative",
              }}
            >
              <table
                className="table mt-3"
                style={{ border: "2px solid #004F9E" }}
              >
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Pregunta CheckList</th>
                    <th>Opciones</th>
                  </tr>
                </thead>

                <tbody>
                  {info.map((item, i) => (
                    <tr key={i}>
                      <td>{i + 1} </td>
                      <td> {item.name} </td>
                      <td>
                        <button
                          onClick={() =>
                            updateQuestion(item.id, item.name, item.checklist)
                          }
                          className="btn btn-info"
                        >
                          <i class="fas fa-edit"></i>
                        </button>{" "}
                        <button
                          onClick={() => handleDelete(item.id)}
                          className="btn btn-danger"
                        >
                          <i class="fas fa-trash-alt"></i>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </div>
    </>
  );
};
export default ViewQuestions;

import React, { useEffect, useState, useCallback } from "react";
import { Modal, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import Swal from "sweetalert2";
import ReactExport from "react-data-export";

import LoDulce from "../../../assets/imagesBoards/Coffeee.png";
import LoAmargo from "../../../assets/imagesBoards/CoffeeAmargo.png";
import Adiciones from "../../../assets/imagesBoards/Adiciones.png";
import Reconocemos from "../../../assets/imagesBoards/Reconocemos.png";

import {
  getBoardCollaborators,
  getBoardColUnlimited,
  searchColaborator,
} from "../../../redux/actions/boardActions";
import {
  getLeadersByCompany,
  getCollaboratorsByLeader,
} from "../Services/apiRelationships";
import { getCompanies } from "../Services/apiResponseCompany";

import "./styleCustom.css";
import "./styleBoard.scss";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function ReporteTablero() {
  const [idLeader, setIdLeader] = useState("");
  const [show, setShow] = useState(false);
  const [title, setTitle] = useState("");
  const [content, setContent] = useState([]);
  const [period, setPeriod] = useState(1);
  const [yearFilter, setYearFilter] = useState(0);
  const [nameColab, setNameColab] = useState("");
  const [sweetList, setSweetList] = useState([]);
  const [bitterList, setBitterList] = useState([]);
  const [weRecognizeList, setWeRecognizeList] = useState([]);
  const [additionList, setAdditionList] = useState([]);
  const [dataColaborators, setDataColaborators] = useState([]);
  const [boarRequired, setBoarRequired] = useState("required");
  const [years, setYears] = useState([]);
  const [bitterLeader, setBitterLeader] = useState("");
  const [sweetLeader, setSweetLeader] = useState("");
  const [showLeaderAdittions, setShowLeaderAdittions] = useState(false);
  const [generalReport, setGeneralReport] = useState([]);

  const [resumenIndividual, setResumenIndividual] = useState([]);
  const handleClose = () => setShow(false);
  const dispatch = useDispatch();

  const handleShow = (name) => {
    switch (name) {
      case "sweet":
        setTitle("LO DULCE");
        setContent(sweetList);
        break;

      case "bitter":
        setTitle("LO AMARGO");
        setContent(bitterList);
        break;

      case "recognize":
        setTitle("RECONOCEMOS");
        setContent(weRecognizeList);
        break;

      case "additions":
        setContent(additionList);
        setTitle("ADICIONES");
        break;
      default:
        break;
    }
    setShow(true);
  };

  const getDataCollaborators = useCallback(
    async (period, year, idLeader) => {
      console.log("id leader from hook => ", idLeader);
      //let idLead = JSON.parse(atob(sessionStorage.getItem("uld")));
      //const idLeader = idLead.split("/")[5];
      await dispatch(
        getBoardCollaborators(
          year,
          period,
          sessionStorage.getItem("idLeader"),
          "True"
        )
      ).then((resp) => {
        if (resp.length !== 0) {
          setDataColaborators(resp);
          console.log("resp ", resp);
          //let datLead = JSON.parse(atob(sessionStorage.getItem("rdt")));
          let excelData = [];
          resp.forEach((element, i) => {
            setNameColab(element.nameCollaborator);
            let sweet = JSON.parse(element.data).sweetData;
            sweet[i] = {
              nameColab: element.nameCollaborator,
              nameLeader: nameLeader,
              ...sweet[0],
            };
            setSweetList(sweet);
            setBitterList(JSON.parse(element.data).bitterData);
            setAdditionList(JSON.parse(element.data).additionsData);
            setWeRecognizeList(JSON.parse(element.data).recognize);
          });

          resp.forEach((col, index) => {
            let board = JSON.parse(col.data);
            if (col.leaderAdditions !== null) {
              let leaderBoard = JSON.parse(col.leaderAdditions);
              let fullboard = { ...board, ...leaderBoard };
              Object.keys(fullboard).forEach((key, i) => {
                fullboard[key].forEach((card, j) => {
                  excelData.push({
                    "Nombre colaborador": col.nameCollaborator,
                    "Nombre lider": nameLeader,
                    "Tipo tarjeta":
                      key === "sweetDataLeader"
                        ? "Lo dulce lider"
                        : key === "bitterDataLeader"
                        ? "Lo amargo lider"
                        : key === "sweetData"
                        ? "Lo dulce"
                        : key === "bitterData"
                        ? "Lo amargo"
                        : key === "additionsData"
                        ? "Adiciones"
                        : key === "recognize"
                        ? "Reconocemos"
                        : "",
                    Tarjeta: card.name,
                    Conversacion:
                      card.selectedPlus !== undefined
                        ? card.selectedPlus
                          ? "De acuerdo"
                          : "En desacuerdo"
                        : "",
                    "Plan de accion":
                      card.actionPlan !== undefined ? card.actionPlan : "",
                    Fecha: card.date,
                  });
                });
              });
            } else {
              Object.keys(board).forEach((key, i) => {
                board[key].forEach((card, j) => {
                  excelData.push({
                    "Nombre colaborador": col.nameCollaborator,
                    "Nombre lider": nameLeader,
                    "Tipo tarjeta":
                      key === "sweetData"
                        ? "Lo dulce"
                        : key === "bitterData"
                        ? "Lo amargo"
                        : key === "additionsData"
                        ? "Adiciones"
                        : key === "recognize"
                        ? "Reconocemos"
                        : "",
                    Tarjeta: card.name,
                    Conversacion:
                      card.selectedPlus !== undefined
                        ? card.selectedPlus
                          ? "De acuerdo"
                          : "En desacuerdo"
                        : "",
                    "Plan de accion":
                      card.actionPlan !== undefined ? card.actionPlan : "",
                    Fecha: card.date,
                  });
                });
              });
            }
          });

          setGeneralReport(excelData);
          setResumenIndividual([]);
          setNameColab("");
        } else {
          Swal.fire(
            "oops!",
            "Lo sentimos no se encontraron resultados con los filtros seleccionados"
          );
          setDataColaborators([]);
          setGeneralReport([]);
          setResumenIndividual([]);
          setNameColab("");
        }
      });
    },
    [dispatch]
  );

  const getDataSearched = async (nameSearch) => {
    //let idLead = JSON.parse(atob(sessionStorage.getItem("uld")));
    //const idLeader = idLead.split("/")[5];
    await dispatch(
      searchColaborator(yearFilter, period, idLeader, "True", nameSearch)
    ).then((resp) => {
      console.log("resp searchered => ", resp);
      if (resp.length !== 0) {
        setDataColaborators(resp);
        setResumenIndividual([]);
        setNameColab("");
      } else {
        Swal.fire(
          "oops!",
          "Lo sentimos no se encontraron resultados con los filtros seleccionados"
        );
        setDataColaborators([]);
        setResumenIndividual([]);
        setNameColab("");
      }
    });
  };

  const calculateYears = () => {
    let currentYear = new Date().getFullYear();
    setYears([currentYear]);
    var prevYear = new Date();
    for (let i = 0; i < 5; i++) {
      prevYear.setFullYear(prevYear.getFullYear() - 1);
      let year = prevYear.getFullYear();
      setYears((old) => [...old, year]);
    }
  };

  const getDataColabUnlimited = useCallback(
    async (year) => {
      //let idLead = JSON.parse(atob(sessionStorage.getItem("uld")));
      //const idLeader = idLead.split("/")[5];
      await dispatch(
        getBoardColUnlimited(year, sessionStorage.getItem("idLeader"), "True")
      ).then((resp) => {
        setDataColaborators(resp);
        //let datLead = JSON.parse(atob(sessionStorage.getItem("rdt")));
        let excelData = [];
        resp.forEach((element, i) => {
          setNameColab(element.nameCollaborator);
          let sweet = JSON.parse(element.data).sweetData;
          sweet[i] = {
            nameColab: element.nameCollaborator,
            nameLeader: nameLeader,
            ...sweet[0],
          };
          setSweetList(sweet);
          setBitterList(JSON.parse(element.data).bitterData);
          setAdditionList(JSON.parse(element.data).additionsData);
          setWeRecognizeList(JSON.parse(element.data).recognize);
        });

        resp.forEach((col, index) => {
          let board = JSON.parse(col.data);
          Object.keys(board).forEach((key, i) => {
            board[key].forEach((card, j) => {
              excelData.push({
                "Nombre colaborador": col.nameCollaborator,
                "Nombre lider": nameLeader,
                "Tipo tarjeta":
                  key === "sweetData"
                    ? "Lo dulce"
                    : key === "bitterData"
                    ? "Lo amargo"
                    : key === "additionsData"
                    ? "Adiciones"
                    : key === "recognize"
                    ? "Reconocemos"
                    : "",
                Tarjeta: card.name,
                Conversacion:
                  card.selectedPlus !== undefined
                    ? card.selectedPlus
                      ? "De acuerdo"
                      : "En desacuerdo"
                    : "",
                "Plan de accion": card.actionPlan,
                Fecha: card.date,
              });
            });
          });
        });
        setGeneralReport(excelData);
      });
    },
    [dispatch]
  );

  useEffect(() => {
    handleGetCompanies();

    let dateSystem = new Date();
    let month = dateSystem.getMonth() + 1;
    let period = 0;
    if (month >= 1 && month <= 6) {
      period = 1;
      setPeriod(1);
    } else {
      period = 2;
      setPeriod(2);
    }
    const year = new Date().getFullYear();
    setYearFilter(year);

    calculateYears();
  }, [getDataCollaborators, getDataColabUnlimited]);

  const setInfoBoard = (item) => {

    let excelData = [];

    let board = JSON.parse(item.data);
            if (item.leaderAdditions !== null) {
              let leaderBoard = JSON.parse(item.leaderAdditions);
              let fullboard = { ...board, ...leaderBoard };
              Object.keys(fullboard).forEach((key, i) => {
                fullboard[key].forEach((card, j) => {
                  excelData.push({
                    "Nombre colaborador": item.nameCollaborator,
                    "Nombre lider": nameLeader,
                    "Tipo tarjeta":
                      key === "sweetDataLeader"
                        ? "Lo dulce lider"
                        : key === "bitterDataLeader"
                        ? "Lo amargo lider"
                        : key === "sweetData"
                        ? "Lo dulce"
                        : key === "bitterData"
                        ? "Lo amargo"
                        : key === "additionsData"
                        ? "Adiciones"
                        : key === "recognize"
                        ? "Reconocemos"
                        : "",
                    Tarjeta: card.name,
                    Conversacion:
                      card.selectedPlus !== undefined
                        ? card.selectedPlus
                          ? "De acuerdo"
                          : "En desacuerdo"
                        : "",
                    "Plan de accion":
                      card.actionPlan !== undefined ? card.actionPlan : "",
                    Fecha: card.date,
                  });
                });
              });
            } else {
              Object.keys(board).forEach((key, i) => {
                board[key].forEach((card, j) => {
                  excelData.push({
                    "Nombre colaborador": item.nameCollaborator,
                    "Nombre lider": nameLeader,
                    "Tipo tarjeta":
                      key === "sweetData"
                        ? "Lo dulce"
                        : key === "bitterData"
                        ? "Lo amargo"
                        : key === "additionsData"
                        ? "Adiciones"
                        : key === "recognize"
                        ? "Reconocemos"
                        : "",
                    Tarjeta: card.name,
                    Conversacion:
                      card.selectedPlus !== undefined
                        ? card.selectedPlus
                          ? "De acuerdo"
                          : "En desacuerdo"
                        : "",
                    "Plan de accion":
                      card.actionPlan !== undefined ? card.actionPlan : "",
                    Fecha: card.date,
                  });
                });
              });
            }


            setResumenIndividual(excelData);

          console.log("item selected => ", item);
          setNameColab(item.nameCollaborator);
          let sweet = JSON.parse(item.data).sweetData;
          sweet[0] = { nameColab: item.nameCollaborator, ...sweet[0] };
          setSweetList(sweet);
          setBitterList(JSON.parse(item.data).bitterData);
          setAdditionList(JSON.parse(item.data).additionsData);
          setWeRecognizeList(JSON.parse(item.data).recognize);
          if (item.leaderAdditions !== null) {
            let sweet = JSON.parse(item.leaderAdditions).sweetDataLeader;
            sweet[0] = { nameColab: item.nameCollaborator, ...sweet[0] };
            setSweetLeader(sweet);
            setBitterLeader(JSON.parse(item.leaderAdditions).bitterDataLeader);
            setShowLeaderAdittions(true);
          } else {
            setShowLeaderAdittions(false);
          }
  };

  const searchColaborators = () => {
    if (boarRequired === "unlimited") {
      getDataColabUnlimited(yearFilter);
    } else {
      getDataCollaborators(period, yearFilter, idLeader);
    }
  };

  const renderTimestamp = (timestamp) => {
    var date = new Date(timestamp).toLocaleDateString("es-ES");
    console.log("timestampss", date);
    return <strong>{date}</strong>;
  };

  const getRandomColor = () => {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  const [companies, setCompanies] = useState([]);
  const [data, setData] = useState([]);

  const [companyName, setCompanyName] = useState("");
  const [leaders, setLeaders] = useState([]);
  const [dataGeneralCompanyExcel, setDataGeneralCompanyExcel] = useState([]);

  const handleGetCompanies = async () => {
    const resp = await getCompanies();
    setCompanies(resp);
  };

  const handleGetLeaderByCompanies = async (id_company) => {
    const resp = await getLeadersByCompany(id_company);

    console.log("resp company => ", resp[0].company_name);
    setCompanyName(resp[0].company_name);
    setLeaders(resp);

    let dataExcel = [];
    resp.forEach((leader) => {
      leader.collaboratorList.forEach((col) => {
        col.answersList.forEach((ans) => {
          dataExcel.push({
            Empresa: leader.company_name,
            "Nombre Colaborador": col.completeName,
            "Nombre Lider": col.leader_name,
            Pregunta: ans.name_question,
            Respuesta: ans.isChecked,
            Periodo: ans.period,
            Año: ans.year,
          });
        });
      });
    });

    setDataGeneralCompanyExcel(dataExcel);
  };

  const [collaborators, setCollaborators] = useState([]);
  const [nameLeader, setNameLeader] = useState("");
  const [dataGeneralExcel, setDataGeneralExcel] = useState([]);

  /* const handleGetCollaboratorsByLeader = async (item) => {
    let jsonData = JSON.parse(item);
    const resp = await getCollaboratorsByLeader(jsonData.id);

    setCollaborators(resp);
    

    let dataExcel = [];

    resp.forEach((col) => {
      col.answersList.forEach((ans) => {
        dataExcel.push({
          "Nombre Colaborador": col.completeName,
          "Nombre Lider": col.leader_name,
          Pregunta: ans.name_question,
          Respuesta: ans.isChecked,
          Periodo: ans.period,
          Año: ans.year,
        });
      });
    });

    setDataGeneralExcel(dataExcel);

    console.log("dataExcel => ", dataExcel);
  }; */

  const handleChangeLeader = (e) => {
    let jsonData = JSON.parse(e);
    console.log(jsonData);

    setIdLeader(jsonData.id);
    sessionStorage.setItem("idLeader", jsonData.id);
    setNameLeader(jsonData.leaderName);

    getDataCollaborators(period, yearFilter, idLeader);
  };

  return (
    <>
      <div className="container p-5 mt-5">
        <div style={{ textAlign: "center" }}>
          <h2
            style={{
              textAlign: "center",
              fontSize: 58,
              color: "#004F9E",
              fontWeight: "bold",
            }}
            className="resume"
          >
            Historial de Tableros
          </h2>
          <hr />
        </div>

        <div className="row mt-5">
          <div className="col-2">
            <h5>Empresa</h5>
          </div>
          <div className="col-5">
            <select
              onChange={(e) => handleGetLeaderByCompanies(e.target.value)}
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>
              {companies.map((item) => (
                <option value={item.id}> {item.companyName} </option>
              ))}
            </select>
          </div>

          {/* {dataGeneralCompanyExcel.length !== 0 && (
            <ExcelFile
              filename={`${companyName} - Reporte CheckList ( EMPRESA )`}
              element={
                <button
                  onClick={() => console.log("data excel => ", data)}
                  className="btn btn-success"
                >
                  Descargar Reporte General - Empresa
                </button>
              }
            >
              <ExcelSheet data={dataGeneralCompanyExcel} name={""}>
                {dataGeneralCompanyExcel.length !== 0 &&
                  Object.keys(dataGeneralCompanyExcel[0]).map((key, index) => (
                    <ExcelColumn
                      key={index.toString()}
                      label={key}
                      value={key}
                      style={{ font: { sz: "29", bold: true } }}
                    />
                  ))}
              </ExcelSheet>
            </ExcelFile>
          )} */}
        </div>

        <div className="row mt-5">
          <div className="col-2">
            <h5>Lideres</h5>
          </div>
          <div className="col-5">
            <select
              onChange={(e) => handleChangeLeader(e.target.value)}
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>

              {leaders.map((item) => (
                <option value={JSON.stringify(item)}>
                  {" "}
                  {item.leaderName}{" "}
                </option>
              ))}
            </select>
          </div>

          {/* {dataGeneralExcel.length !== 0 && (
            <ExcelFile
              element={
                <button className="btn btn-success">
                  Descargar Reporte General
                </button>
              }
              filename={`${nameLeader} - Reporte CheckList ( COLABORADORES )`}
            >
              <ExcelSheet data={dataGeneralExcel} name="Checklist">
                {dataGeneralExcel.length !== 0 &&
                  Object.keys(dataGeneralExcel[0]).map((key, index) => (
                    <ExcelColumn
                      key={index.toString()}
                      label={key}
                      value={key}
                      style={{ font: { sz: "29", bold: true } }}
                    />
                  ))}
              </ExcelSheet>
            </ExcelFile>
          )} */}
        </div>

        {idLeader !== "" ? (
          <div
            style={{
              marginTop: 50,
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-around",
              width: "100%",
            }}
          >
            <div style={{ width: "24%" }}>
              <h5>Buscar por Colaborador</h5>
              <input
                type="text"
                className="form-control"
                onChange={(e) => getDataSearched(e.target.value)}
              />
              <br />
              <div>
                {dataColaborators.map((item) => (
                  <div
                    key={item.id.toString()}
                    className="cardItems"
                    onClick={() => setInfoBoard(item)}
                  >
                    <strong>{item.nameCollaborator}</strong>
                    {item.month !== undefined && (
                      <strong>{renderTimestamp(item.timestamp)}</strong>
                    )}
                  </div>
                ))}
              </div>
            </div>
            <div style={{ minWidth: "51%" }}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-around",
                }}
              >
                <select
                  className="form-control selectStyle"
                  style={{ width: "27%", height: 35 }}
                  name=""
                  id=""
                  onChange={(e) => setBoarRequired(e.target.value)}
                  defaultValue={period}
                >
                  <option value="required">Gestion Desempeño</option>
                  <option value="unlimited">Conversación Cotidiana </option>
                </select>
                <select
                  className="form-control selectStyle"
                  style={{ width: "27%", height: 35 }}
                  onChange={(e) => setYearFilter(e.target.value)}
                  defaultValue={yearFilter}
                >
                  {years.map((year, index) => (
                    <option key={index.toString()} value={year}>
                      {year}
                    </option>
                  ))}
                </select>
                <select
                  className="form-control selectStyle"
                  style={{ width: "27%", height: 35 }}
                  name=""
                  id=""
                  onChange={(e) => setPeriod(e.target.value)}
                  defaultValue={period}
                >
                  <option value="1">1</option>
                  <option value="2">2</option>
                </select>
                <button
                  onClick={() => searchColaborators()}
                  className="btn btn-primary"
                >
                  <small>Buscar</small>
                  <svg
                    className="bi bi-search"
                    width="20"
                    height="20"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      d="M6.646 3.646a.5.5 0 01.708 0l6 6a.5.5 0 010 .708l-6 6a.5.5 0 01-.708-.708L12.293 10 6.646 4.354a.5.5 0 010-.708z"
                    />
                  </svg>
                </button>
              </div>
              <hr />

              {nameColab !== "" && (
                <>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      width: "100%",
                    }}
                  >
                    <div>
                      <i class="fas fa-user-circle fa-2x"></i>
                    </div>
                  </div>
                  <div>
                    <h5
                      style={{
                        textAlign: "center",
                        color: "#272727",
                        fontWeight: "bold",
                        marginTop: 20,
                      }}
                    >
                      {nameColab}
                    </h5>
                  </div>
                </>
              )}
              <div
                style={{
                  display: "center",
                  justifyContent: "center",
                  width: "100%",
                }}
              ></div>
              <hr />
              {generalReport.length !== 0 && (
                <ExcelFile
                  element={
                    <button className="btn btn-success">
                      Descargar Reporte General
                    </button>
                  }
                  filename="Reporte"
                >
                  <ExcelSheet data={generalReport} name="Reporte General">
                    {generalReport.length !== 0 &&
                      Object.keys(generalReport[0]).map((key, index) => (
                        <ExcelColumn
                          key={index.toString()}
                          label={key}
                          value={key}
                        />
                      ))}
                  </ExcelSheet>
                </ExcelFile>
              )}

              {resumenIndividual.length > 0 && (
                <>

                <ExcelFile
                  element={
                    <button className="btn btn-success ml-3">
                      Descargar Reporte Individual
                    </button>
                  }
                  filename="Reporte"
                >
                  <ExcelSheet data={resumenIndividual} name="Reporte General">
                    {resumenIndividual.length !== 0 &&
                      Object.keys(resumenIndividual[0]).map((key, index) => (
                        <ExcelColumn
                          key={index.toString()}
                          label={key}
                          value={key}
                        />
                      ))}
                  </ExcelSheet>
                </ExcelFile>
                  

                  
                  
                  <div style={{ display: "flex", justifyContent: "center" }}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        width: "99%",
                      }}
                    >
                      <div>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            marginTop: 30,
                          }}
                        >
                          <div
                            onClick={() => handleShow("sweet")}
                            className="conversation-box-sweet"
                            style={{
                              textDecoration: "none",
                              borderColor: "transparent",
                            }}
                          >
                            <h4 className="title mt-2 text-white"> LO DULCE</h4>
                            <img src={LoDulce} alt="img dulce" />
                          </div>
                        </div>

                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                          }} /* class="d-flex justify-content-around mt-5" */
                        >
                          <div
                            onClick={() => handleShow("bitter")}
                            //to="/conversation/sweet"
                            className="conversation-box-bitter"
                            style={{
                              textDecoration: "none",
                              borderColor: "transparent",
                            }}
                          >
                            <h4 className="title mt-2 text-white">
                              {" "}
                              EXPLOREMOS LO AMARGO
                            </h4>
                            <img
                              className="mt-4"
                              src={LoAmargo}
                              alt="img amargo"
                            />
                          </div>
                        </div>
                      </div>

                      <div
                        style={{
                          display: "flex",
                          marginTop: 30,
                        }} /* class="d-flex justify-content-around mt-5" */
                      >
                        <div
                          onClick={() => handleShow("recognize")}
                          //to="/conversation/sweet"
                          className="conversation-box-weRecognize"
                          style={{
                            textDecoration: "none",
                            borderColor: "transparent",
                          }}
                        >
                          <h4 className="title verticaltext mt-2 text-white">
                            {" "}
                            RECONOCEMOS
                          </h4>
                          <img src={Reconocemos} alt="img reconocemos" />
                        </div>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          marginTop: 30,
                        }}
                      >
                        <div
                          onClick={() => handleShow("additions")}
                          //to="/conversation/sweet"
                          className="conversation-box-additions"
                          style={{
                            textDecoration: "none",
                            borderColor: "transparent",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            flexDirection: "column",
                          }}
                        >
                          <h4 className="title mt-2 text-white"> ADICIONES</h4>
                          <img src={Adiciones} alt="img adiciones" />
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        ) : null}

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>{title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {content.map((item) => (
              <div key={item.id.toString()}>
                <p
                  style={{
                    border: `1px solid ${getRandomColor()}`,
                    textAlign: "center",
                    borderRadius: 17,
                    backgroundColor: getRandomColor(),
                    color: "white",
                    padding: "5px 0",
                  }}
                >
                  {item.name}
                </p>
                {title === "ADICIONES" ? (
                  <div>
                    <p>Plan de accion:</p>
                    <p>{item.actionPlan}</p>
                    <p>{item.date}</p>
                  </div>
                ) : (
                  <p></p>
                )}
                {title === "RECONOCEMOS" ? (
                  <>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        width: "100%",
                      }}
                    >
                      <img src={item.emoji} alt="" />
                    </div>
                  </>
                ) : null}
              </div>
            ))}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={handleClose}>
              OK
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    </>
  );
}

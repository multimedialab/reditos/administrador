import React, { useEffect, useState } from "react";

import Swal from "sweetalert2";
import ReactExport from "react-data-export";

import "./styleAdmin.css";

import { getCompanies } from "../Services/apiResponseCompany";
import {
  getLeadersByCompany,
  getCollaboratorsByLeader,
} from "../Services/apiRelationships";
import { getActionPlanByCollaborator } from "../Services/apiActionPlan";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ReportePlanAccion = (props) => {
  useEffect(() => {
    handleGetCompanies();
  }, []);

  const [companies, setCompanies] = useState([]);
  const [leaders, setLeaders] = useState([]);
  const [collaborators, setCollaborators] = useState([]);
  const [actionPlan, setActionPlan] = useState([]);
  const [nameLeader, setNameLeader] = useState("");
  const [nameCol, setNameCol] = useState("");
  const [dataGeneralExcel, setDataGeneralExcel] = useState([]);
  const [dataGeneralCompanyExcel, setDataGeneralCompanyExcel] = useState([]);
  const [companyName, setCompanyName] = useState('');


  /*   const [nameCollaborator, setNameCollaborator] = useState("");
   */

  const [data, setData] = useState([]);

  const handleGetCompanies = async () => {
    const resp = await getCompanies();
    setCompanies(resp);
  };

  const handleGetLeaderByCompanies = async (id_company) => {
    const resp = await getLeadersByCompany(id_company);
    setLeaders(resp);

    setCompanyName(resp[0].company_name);
    console.log("leaders for company => ", resp);

    let dataExcel = [];
    resp.forEach((leader) => {
      leader.collaboratorList.forEach((col) => {
        col.planIndividualList.forEach((plan) => {
          plan.evaluationList.forEach((eva) => {
            dataExcel.push({
              "Empresa": leader.company_name,
              "Nombre Colaborador": col.completeName,
              "Nombre Lider": col.leader_name,
              "Objetivo Estratégico": plan.obj_estrategico,
              "Objetivo Táctico": plan.obj_tactico,
              "¿Que espera lograr?": plan.que_espera,
              "Como lo va lograr?": plan.como_lo_logra,
              "Plan de accion": eva.action_plan,
              Observaciones: eva.observations,
            });
          });
        });
      });
    });

    setDataGeneralCompanyExcel(dataExcel);

  };

  const handleGetCollaboratorsByLeader = async (item) => {
    let jsonData = JSON.parse(item);
    const resp = await getCollaboratorsByLeader(jsonData.id);

    setNameLeader(jsonData.leaderName);

    setCollaborators(resp);
    console.log("collaborators for leader => ", resp);

    let dataExcel = [];

    resp.forEach((col) => {
      col.planIndividualList.forEach((plan) => {
        plan.evaluationList.forEach((eva) => {
          dataExcel.push({
            "Nombre Colaborador": col.completeName,
            "Nombre Lider": col.leader_name,
            "Objetivo Estratégico": plan.obj_estrategico,
            "Objetivo Táctico": plan.obj_tactico,
            "¿Que espera lograr?": plan.que_espera,
            "Como lo va lograr?": plan.como_lo_logra,
            "Plan de accion": eva.action_plan,
            Observaciones: eva.observations,
          });
        });
      });
    });

    setDataGeneralExcel(dataExcel);

    console.log("dataExcel => ", dataExcel);
  };

  const handleGetActionPlanByCollaborator = async (item) => {
    let jsonData = JSON.parse(item);
    //setNames({collaborator: jsonData.completeName});
    console.log("COMPLETE NAME OF COLLABORATOR => ", jsonData.completeName);
    setNameCol(`${jsonData.completeName}`);
    console.log("item => ", jsonData.completeName);
    const resp = await getActionPlanByCollaborator(jsonData.id);

    //console.log('state names =>', names);

    if (resp) {
      setActionPlan(resp);
      //data excel
      const dataExcel = [];

      resp.forEach((element, i) => {
        let objExcel = {
          objEstrategico: resp[i].obj_estrategico,
          objTactico: resp[i].obj_tactico,
          queEspera: resp[i].que_espera,
          comoLoLogra: resp[i].como_lo_logra,
          weight: resp[i].weight,
          leader: nameLeader,
          collaborator: nameCol,
        };

        dataExcel.push(objExcel);
        setData(dataExcel);
        console.log("dataExcel => ", dataExcel);

        /* console.log('state name complete => ', nameCollaborator); */
      });
    } else {
      setActionPlan([]);
    }
  };

  return (
    <>
      <div className="container p-5 mt-5">
        <h2 id="title">Reporte de Plan Individual</h2>
        <div className="row mt-5">
          <div className="col-2">
            <h5>Empresa</h5>
          </div>
          <div className="col-5">
            <select
              onChange={(e) => handleGetLeaderByCompanies(e.target.value)}
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>
              {companies.map((item) => (
                <option value={item.id}> {item.companyName} </option>
              ))}
            </select>
          </div>

          {dataGeneralCompanyExcel.length !== 0 && (
            <ExcelFile
              filename={`${companyName} - Reporte Plan Individual ( EMPRESA )`}
              element={
                <button
                  onClick={() => console.log("data excel => ", data)}
                  className="btn btn-success"
                >
                  Descargar Reporte General - Empresa
                </button>
              }
            >
              <ExcelSheet data={dataGeneralCompanyExcel} name={""}>
                {dataGeneralCompanyExcel.length !== 0 &&
                  Object.keys(dataGeneralCompanyExcel[0]).map((key, index) => (
                    <ExcelColumn
                      key={index.toString()}
                      label={key}
                      value={key}
                      style={{ font: { sz: "29", bold: true } }}
                    />
                  ))}
              </ExcelSheet>
            </ExcelFile>
          )}



        </div>

        <div className="row mt-5">
          <div className="col-2">
            <h5>Lideres</h5>
          </div>
          <div className="col-5">
            <select
              onChange={(e) => handleGetCollaboratorsByLeader(e.target.value)}
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>

              {leaders.map((item) => (
                <option value={JSON.stringify(item)}>
                  {" "}
                  {item.leaderName}{" "}
                </option>
              ))}
            </select>
          </div>

          {dataGeneralExcel.length !== 0 && (
            <ExcelFile
              element={
                <button className="btn btn-success">
                  Descargar Reporte General - Lider
                </button>
              }
              filename={`${nameLeader} - Reporte Plan de Acción Individual ( COLABORADORES )`}
            >
              <ExcelSheet data={dataGeneralExcel} name="">
                {dataGeneralExcel.length !== 0 &&
                  Object.keys(dataGeneralExcel[0]).map((key, index) => (
                    <ExcelColumn
                      key={index.toString()}
                      label={key}
                      value={key}
                      style={{ font: { sz: "29", bold: true } }}
                    />
                  ))}
              </ExcelSheet>
            </ExcelFile>
          )}
        </div>

        <div className="row mt-5">
          <div className="col-2">
            <h5>Colaboradores</h5>
          </div>
          <div className="col-5">
            <select
              onChange={(e) =>
                handleGetActionPlanByCollaborator(e.target.value)
              }
              className="form-control"
              name=""
              id=""
            >
              <option value="">Selecciona una opción</option>

              {collaborators.map((item) => (
                <option value={JSON.stringify(item)}>
                  {" "}
                  {item.completeName}{" "}
                </option>
              ))}
            </select>
          </div>
        </div>

        <hr />

        {actionPlan.length !== 0 ? (
          <>
            <div className="text-center">
              <h4 id="title">PLAN DE ACCIÓN</h4>
            </div>

            <div className="row mt-5">
              <table
                className="table"
                style={{
                  backgroundColor: "white",
                  border: "2px solid #004F9E",
                }}
              >
                <thead>
                  <tr>
                    <th style={{ color: "#004F9E", textAlign: "center" }}>#</th>
                    <th style={{ color: "#004F9E", textAlign: "center" }}>
                      {" "}
                      Objetivo Estratégico{" "}
                    </th>
                    <th style={{ color: "#004F9E", textAlign: "center" }}>
                      {" "}
                      Objetivo Táctico{" "}
                    </th>
                    <th style={{ color: "#004F9E", textAlign: "center" }}>
                      {" "}
                      ¿Que espera lograr?{" "}
                    </th>
                    <th style={{ color: "#004F9E", textAlign: "center" }}>
                      {" "}
                      ¿Como lo va a lograr?{" "}
                    </th>
                    <th style={{ color: "#004F9E", textAlign: "center" }}>
                      {" "}
                      Peso{" "}
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {actionPlan.map((item, index) => (
                    <>
                      <tr>
                        <td> {index + 1} </td>
                        <td> {item.obj_estrategico} </td>
                        <td> {item.obj_tactico} </td>
                        <td> {item.que_espera} </td>
                        <td> {item.como_lo_logra} </td>
                        <td> {item.weight} </td>
                      </tr>
                    </>
                  ))}
                </tbody>
              </table>

              {/* <ExcelFile
                element={
                  <button
                    onClick={() => console.log("data excel => ", data)}
                    className="btn btn-success"
                  >
                    Descargar Reporte
                  </button>
                }
                filename={`Reporte Plan de Acción - ${nameCol} `}
              >
                <ExcelSheet data={data} name="Checklist">
                  
                  <ExcelColumn
                    label="Objetivo estratégico"
                    value="objEstrategico"
                    style={{ font: { sz: "29", bold: true } }}
                  />
                  <ExcelColumn
                    label="Objetivo táctico"
                    value="objTactico"
                    style={{ font: { sz: "29", bold: true } }}
                  />
                  <ExcelColumn
                    label="¿Que espera lograr?"
                    value="queEspera"
                    style={{ font: { sz: "29", bold: true } }}
                  />
                  <ExcelColumn
                    label="¿Como lo va a lograr?"
                    value="comoLoLogra"
                    style={{ font: { sz: "29", bold: true } }}
                  />
                  <ExcelColumn
                    label="Peso"
                    value="weight"
                    style={{ font: { sz: "29", bold: true } }}
                  />
                </ExcelSheet>
              </ExcelFile> */}

            </div>
          </>
        ) : (
          <div className="text-center">
            {" "}
            Para visualizar un plan de acción realiza el filtro{" "}
          </div>
        )}
      </div>
    </>
  );
};
export default ReportePlanAccion;

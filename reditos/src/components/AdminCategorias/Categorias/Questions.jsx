import React, { useState, useEffect } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import { url, body } from "../Services/auth";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./styleAdmin.css";

const Questions = () => {
  const [inputList, setInputList] = useState([
    { isChecked: false, name: "", checklist: "" },
  ]);

  const initialState = {
    /*
    nameValue: "",
    weight: "",
    id_category: "",
    */
  };

  const [subcategory, setSubCategory] = useState([]);

  const [values, setValues] = useState(initialState);
  const [isConsulted, setIsConsulted] = useState(false);

  const [questions, setquestions] = useState([]);

  const handleClickCrear = async () => {
    const token = await getToken();

    console.log(values);
    console.log(inputList);

    for (let i = 0; i < inputList.length; i++) {
      if (Object.keys(inputList).length !== 0) {
        fetch(`${url.urlBase}/question/`, {
          method: "POST",
          body: JSON.stringify(inputList[i]),
          headers: {
            "Content-Type": "application/json",
            Authorization: `JWT ${token}`,
          },
        })
          .then((response) => response.json())
          .then((json) => {
            getChecklist();
            setIsConsulted(true);
            Swal.fire({
              icon: "success",
              title: "Creado con exito",
              showConfirmButton: false,
              timer: 1500,
            });

            console.log("Initial State: ", values);
            console.log("Inputs State: ", inputList);
            console.log();
          })
          .catch((error) => console.error(error));
      } else {
        Swal.fire({
          icon: "error",
          title: "Los campos no pueden estar vacios",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    }
  };

  const handleSubmit = (e) => {
    //console.log(values);

    e.preventDefault();
    document.getElementById("form").reset();

    setValues({ ...initialState });
  };

  const handleAddClick = () => {
    setInputList([...inputList, { isChecked: false, name: "", checklist: "" }]);
  };

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };

  const handleRemoveClick = (index) => {
    const list = [...inputList];
    list.splice(index, 1);
    setInputList(list);
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
  };

  const getChecklist = async () => {
    const token = await getToken();
    const respData = axios.get(`${url.urlBase}/checklist/`, {
      headers: { Authorization: `JWT ${token}` },
    });
    const data = (await respData).data;

    setSubCategory(data.results);
  };

  useEffect(() => {
    getChecklist();
  }, []);

  return (
    <>
      <div className="body">
        <div className="adminCategoria">
          <h2 id="title" className="title d-flex justify-content-center">
            Preguntas CheckList
          </h2>
          <br />
          <div className="row">
            <div className="col">
              <div className="p-3">
                <button className="btn btn-primary" style={{ float: "left" }}>
                  <Link to="/dashboard/administrar-checklist">
                    <strong className="text-white">
                      <i className="fas fa-plus mr-2"></i>
                      Nuevo Checklist
                    </strong>
                  </Link>
                </button>
                <button className="btn btn-primary" style={{ float: "right" }}>
                  <Link to="/dashboard/ver-preguntas-checklist">
                    <strong className="text-white">
                      <i className="fas fa-eye mr-2"></i>
                      Ver Preguntas del CheckList
                    </strong>
                  </Link>
                </button>
              </div>
            </div>
          </div>

          <form id="form" action="" onSubmit={handleSubmit}>
            <br />

            <div className="container">
              <div className="col-12 text-center">
                <br />
                <table
                  className="table"
                  style={{ border: "2px solid #004F9E" }}
                >
                  <thead>
                    <tr>
                      <th># Valor</th>

                      <th>Titulo de la pregunta</th>

                      <th>Checklist</th>
                      <th>Agregar / Eliminar</th>
                    </tr>
                  </thead>
                  <tbody>
                    {inputList.map((item, i) => {
                      return (
                        <>
                          <tr>
                            <td>{i + 1}</td>

                            <td>
                              <input
                                className="form-control"
                                type="text"
                                name="name"
                                placeholder="Ej: ¿Cumples con las metas?"
                                value={inputList.name}
                                onChange={(e) => handleInputChange(e, i)}
                              />
                            </td>

                            <td>
                              <select
                                onChange={(e) => handleInputChange(e, i)}
                                className="form-control"
                                name="checklist"
                                id=""
                                value={inputList.checklist}
                              >
                                <option value={inputList.checklist}>
                                  Seleccionar
                                </option>
                                {subcategory.map((item, i) =>
                                  item.name == "GENERAL" ? (
                                    <option value={item.url} key={i}>
                                      {item.name}
                                    </option>
                                  ) : null
                                )}
                              </select>
                            </td>

                            <td>
                              <div className="row">
                                <div className="col-1 mr-2">
                                  {inputList.length - 1 === i && (
                                    <button
                                      className="btn btn-primary mr-3"
                                      onClick={handleAddClick}
                                    >
                                      <i className="fas fa-plus"></i>
                                    </button>
                                  )}
                                </div>
                                <div className="col-1">
                                  {inputList.length !== 1 && (
                                    <button
                                      onClick={() => handleRemoveClick(i)}
                                      className="btn btn-danger"
                                    >
                                      <i class="fas fa-minus"></i>
                                    </button>
                                  )}
                                </div>
                              </div>
                            </td>
                          </tr>
                        </>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
            <div className="text-center">
              <button onClick={handleClickCrear} className="btn btn-primary">
                <i class="fas fa-check"></i> Guardar
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};
export default Questions;

import React from "react";

import Swal from 'sweetalert2';

const DesbloquearSeguimiento = () => {

    const handleClickActivar = () => {
        Swal.fire({
            title: '¿Estas seguro de activar el seguimiento seleccionado?',
            
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Activar!'
          }).then((result) => {
            if (result.value) {
              Swal.fire(
                  
                'Activado!',
                
              )
            }
          })
    }


    const handleClickGuardar = () => {
        Swal.fire({
            
            icon: 'success',
            title: 'Guardado con exito',
            showConfirmButton: false,
            timer: 1500
          })

    }


  return (
    <>
      <div className="container p-5 mt-5">
        <h2>
          <strong style={{ fontSize: 24 }}>DESBLOQUEAR SEGUIMIENTO</strong>
        </h2>

        <div className="row mt-5">
          <div className="col">Nro. Identificacion del colaborador</div>
          <div className="col">
            <select className="form-control" name="" id="">
              <option value="">1234</option>
              <option value="">1234</option>
              <option value="">1234</option>
              <option value="">1234</option>
            </select>
          </div>
        </div>
        <div className="row mt-5">
          <div className="col">Empresa</div>
          <div className="col">
            <select className="form-control" name="" id="">
              <option value="">Empresa 1</option>
              <option value="">Empresa 2</option>
              <option value="">Empresa 3</option>
              <option value="">Empresa 4</option>
            </select>
          </div>
        </div>

        <hr />

        <div className="row">
          <div className="col">
            <strong>Nro. Identificacion</strong>
          </div>
          <div className="col">
            <input type="text" className="form-control" />
          </div>
          <div className="col">
            <strong>Nombre del Colaborador</strong>
          </div>
          <div className="col">
            <input type="text" className="form-control" />
          </div>
        </div>

        <div className="text-center mt-5">
          <strong style={{fontSize: 18}}>SEGUIMIENTO</strong>
        </div>

        <div className="row mt-5">
          <table
            className="table"
            style={{ backgroundColor: "white", border: "2px solid #004F9E" }}
          >
            <thead>
              <tr>
                <th>SEGUIMIENTO</th>
                <th style={{ color: "#004F9E", textAlign: "center" }}>
                  ESTADO
                </th>
                <th style={{ color: "#004F9E", textAlign: "center" }}>
                  FECHA DE BLOQUEO
                </th>
                <th>MOTIVO DE BLOQUEO</th>
                <th>ACCIÓN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="text-center">Seguimiento 1</td>
                <td style={{ textAlign: "center" }}>Activo</td>
                <td style={{ textAlign: "center" }}>
                  <p>15/01/2020</p>
                </td>
                <td>
                  <input type="text" className="form-control" />
                </td>
                <td>
                  <button
                  className="btnConsulta"
                  id="btnAdmin"
                  style={{ borderRadius: 5 }}
                  onClick={handleClickActivar}>Activar</button>
                </td>
              </tr>
              <tr>
                <td className="text-center">Seguimiento 1</td>
                <td style={{ textAlign: "center" }}>Activo</td>
                <td style={{ textAlign: "center" }}>
                  <p>15/01/2020</p>
                </td>
                <td>
                  <input type="text" className="form-control" />
                </td>
                <td>
                  <button
                  className="btnConsulta"
                  id="btnAdmin"
                  style={{ borderRadius: 5 }}
                  onClick={handleClickActivar}>Activar</button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <div className="text-center mt-5">
            <div className="row">
                <div className="col">
                    <h5 >Asignar nueva fecha limite para el seguimiento</h5>
                </div>
                <div className="col">
                    <input className="form-control" type="date" name="" id=""/>
                </div>
            </div>
            <div className="row">
                <div className="col mt-5">
                    <button 
                    className="btnConsulta"
                    id="btnAdmin"
                    style={{ borderRadius: 5 }}
                    onClick={handleClickGuardar}>Guardar</button>
                    <button
                    className="btnConsulta"
                    id="btnAdmin"
                    style={{ borderRadius: 5 }}
                    className="ml-4">Cancelar</button>
                </div>
                
            </div>
        </div>
      </div>
    </>
  );
};
export default DesbloquearSeguimiento;

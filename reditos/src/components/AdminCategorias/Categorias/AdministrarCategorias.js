import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import AdministrarCategorias from "./AdministrarCategorias";
import Swal from "sweetalert2";
import axios from "axios";
import { url, body } from "../Services/auth";
import { apiToken, deleteCategory } from "../Services/apiResponseCategory";

import { toast } from "react-toastify";
import "./styleAdmin.css";

const AdminCategorias = (props) => {
  const initialState = {
    categoryName: "",
    description: "",
  };

  const [values, setValues] = useState(initialState);
  const [info, setInfo] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [desCat, setDesCat] = useState("");
  const [isConsulted, setIsConsulted] = useState(false);

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };

  useEffect(() => {
    handleClickTodos();
  }, []);

  const handleOnChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    console.log(values);
    e.preventDefault();
    setValues({ ...initialState });
  };

  const consult = async () => {
    const token = await getToken();
    fetch(`${url.urlBase}/category/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => setInfo(json.results))
      .catch((error) => console.error(error));
  };

  const handleClickGuardar = () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleClickEliminar = () => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Eliminado!");
        consult();
      }
    });
  };

  const handleClickConsultar = () => {
    //Swal.fire('Consultando...')
  };

  const handleClickTodos = () => {
    consult();
    setIsConsulted(true);
    /*
    if (Object.keys(info).length !== 0){
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'No hay registros',
        showConfirmButton: false,
        timer: 3000
      });
    }*/
  };

  const handleClickOcultar = () => {
    setIsConsulted(false);
  };

  const handleClickLimpiar = () => {
    setValues({ ...initialState });
    document.getElementById("form").reset();
  };

  const handleClickCrear = async () => {
    const token = await getToken();
    if (values.categoryName !== "" && values.description !== "") {
      fetch(`${url.urlBase}/category/`, {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${token}`,
        },
      })
        .then((response) => response.json())
        .then((json) => {
          setIsConsulted(true);
          consult();
          Swal.fire({
            icon: "success",
            title: "Creado con exito",
            showConfirmButton: false,
            timer: 1500,
          });

          setValues({ ...initialState });
        })
        .catch((error) => console.error(error));
    } else {
      Swal.fire({
        icon: "error",
        title: "Los campos no pueden estar vacios",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };

  const handleClickActualizar = async () => {
    const token = await apiToken();
    const response = await axios.put(
      `${url.urlBase}/category/${idCategory}/`,
      values,
      { headers: { Authorization: `JWT ${token}` } }
    );
    console.log(response);
    setValues({ ...initialState });
    handleClickTodos();
    Swal.fire({
      title: "Actualizado con exito",
      icon: "info",
    });
  };

  const handleClickCancel = () => {
    Swal.fire({
      title: "Cancelando...",
      icon: "warning",
    });
  };

  const [isUpdated, setIsUpdated] = useState(false);
  const [idCategory, setidCategory] = useState("");

  const handleDelete = async (id) => {
    console.log(id);
    console.log("DELETE ID: ", id);

    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#ccc",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        deleteCategory(id);
        Swal.fire("Eliminado!");
        handleClickTodos();
        consult();
      }
    });
  };

  const updateInfoCat = async (id, name, description) => {
    const values = {
      categoryName: name,
      description: description,
    };
    setIsUpdated(true);
    setidCategory(id);
    setValues(values);
    //setCompanyName(company_name);
    console.log("Values: ", values);
  };

  return (
    <div className="body col-lg-12 col-md-12 col-sm-9 col-xs-9">
      <div className="logoImg "></div>
      <div className="adminCategoria">
        <h2 id="title">Administrar Categorias de Evaluación de Desempeño</h2>
        <br />
        <form id="form" action="" onSubmit={handleSubmit}>
          

          <div className="admin">
            <div className="col">
              <label>Nombre de Categoria de Evaluacion de Desempeño</label>
            </div>
            <div className="col">
              <input
                onChange={handleOnChange}
                className="form-control"
                value={values.categoryName}
                name="categoryName"
              />
            </div>
          </div>

          <div className="admin">
            <div className="col">
              <label>Descripción</label>
            </div>
            <div className="col">
              <input
                onChange={handleOnChange}
                className="form-control"
                name="description"
                value={values.description}
              />
            </div>
          </div>

          <div className="botones col-lg-12 col-md-12 col-sm-12">
            <div className="col">
              <Button
                className="btn btn-primary"
                onClick={handleClickCrear}
                type="submit"
              >
                <i className="fas fa-check mr-2"></i>
                GUARDAR
              </Button>
            </div>

            <div className="col">
              <Button
                className="btn btn-success"
                onClick={handleClickActualizar}
                type="button"
              >
                <i className="fas fa-pencil-alt mr-2"></i>
                ACTUALIZAR
              </Button>
            </div>
            <div className="col">
              <Button
                className="btn btn-secondary"
                onClick={handleClickLimpiar}
                type="button"
              >
                LIMPIAR CAMPOS
              </Button>
            </div>
          </div>
        </form>
        {isConsulted ? (
          <div
            className="card-body ml-5 mr-3 mt-5"
            style={{
              height: "40vh",
              overflow: "scroll",
              overflowX: "hidden",
              position: "relative",
            }}
          >
            <table
              className="table mt-5"
              style={{ border: "2px solid #004F9E" }}
            >
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre de Categoria de Evaluación de Desempeño</th>
                  <th>Descripción</th>
                  <th>Opciones</th>
                </tr>
              </thead>

              <tbody>
                {info.map((item, i) => (
                  <tr key={i}>
                    <td>{i + 1} </td>
                    <td> {item.categoryName} </td>
                    <td> {item.description} </td>
                    <td>
                      <button
                        onClick={() =>
                          updateInfoCat(
                            item.id,
                            item.categoryName,
                            item.description
                          )
                        }
                        className="btn btn-info"
                      >
                        <i class="fas fa-edit"></i>
                      </button>{" "}
                      <button
                        onClick={() => handleDelete(item.id)}
                        className="btn btn-danger"
                      >
                        <i class="fas fa-trash-alt"></i>
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </div>
  );
};

export default AdminCategorias;

import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import axios from "axios";
import Swal from "sweetalert2";
import { url, body } from "../Services/auth";
import { Form, Button, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import { getDefaultNormalizer } from "@testing-library/react";
import { getValues, updateValues, deleteValues } from "../Services/getValues";

const ConfigurarSubcategorias = () => {
  const { categoryname } = useSelector((state) => state.menuReducer);
  const { idCategory } = useSelector((state) => state.menuReducer);

  const [inputList, setInputList] = useState([
    { name_item: "", description: "", weight: "", id_value: "" },
  ]);

  const initialState = {
    /*
    nameValue: "",
    weight: "",
    id_category: "",
    */
  };

  const [subcategory, setSubCategory] = useState([]);

  const [values, setValues] = useState(initialState);
  const [isConsulted, setIsConsulted] = useState(false);

  const handleClickCrear = async () => {
    const token = await getToken();

    console.log(values);
    console.log(inputList);

    for (let i = 0; i < inputList.length; i++) {
      if (Object.keys(inputList).length !== 0) {
        fetch(`${url.urlBase}/items/`, {
          method: "POST",
          body: JSON.stringify(inputList[i]),
          headers: {
            "Content-Type": "application/json",
            Authorization: `JWT ${token}`,
          },
        })
          .then((response) => response.json())
          .then((json) => {
            getSubCategories();
            setIsConsulted(true);
            Swal.fire({
              icon: "success",
              title: "Creado con exito",
              showConfirmButton: false,
              timer: 1500,
            });

            console.log("Initial State: ", values);
            console.log("Inputs State: ", inputList);
            console.log();
          })
          .catch((error) => console.error(error));
      } else {
        Swal.fire({
          icon: "error",
          title: "Los campos no pueden estar vacios",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    }
  };

  const handleSubmit = (e) => {
    //console.log(values);

    e.preventDefault();
    document.getElementById("form").reset();

    setValues({ ...initialState });
  };

  const handleAddClick = () => {
    setInputList([
      ...inputList,
      { name_item: "", description: "", weight: "", id_value: "" },
    ]);
  };

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };

  const handleRemoveClick = (index) => {
    const list = [...inputList];
    list.splice(index, 1);
    setInputList(list);
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
  };

  const getSubCategories = async () => {
    const token = await getToken();
    const respData = axios.get(
      `${url.urlBase}/values/?id_category=${idCategory}`,
      {
        headers: { Authorization: `JWT ${token}` },
      }
    );
    const data = (await respData).data;

    setSubCategory(data.results);
  };

  useEffect(() => {
    getSubCategories();
  }, []);

  const handleChangeSelect = (e) => {
    getDataValues(e);
  };

  const [valuesData, setValuesData] = useState([]);

  const getDataValues = async (id_value) => {
    const resp = await getValues(id_value);
    if (resp) {
      setValuesData(resp);
    } else {
      setValuesData([]);
    }
  };

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => {
    setShow(true);
    /* setCard({name: ''}); */
    /* setIsUpdate(false); */
  };

  
  const initialStateItem = {
    name_item: '',
  }

  const [nameitem, setNameItem] = useState(initialStateItem);
  const [idItem, setIdItem] = useState('');
  const [idValue, setIdValue] = useState('');
  
  const updateItem = item => {
    setNameItem({name_item: item.name_item});
    setIdItem(item.id);
    setIdValue(item.id_value);
    handleShow();
  }

  const handleOnChange = e => setNameItem({name_item: e});

  const onUpdate = async () => {
    await updateValues(idItem, {name_item: nameitem.name_item, id_value: idValue});
    console.log('state => ', nameitem.id_value);
    handleClose();
  };


  const deleteItem = async idValue => {

    Swal.fire({
      title: 'Estas seguro de eliminar?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#ccc',
      confirmButtonText: 'Eliminar'
    }).then(async (result) => {
      if (result.isConfirmed) {
        
        await deleteValues(idValue);

        Swal.fire(
          'Item Eliminado!',
          '',
          'success'
        )
      }
    })


  }


  return (
    <>
      <div className="body">
        <div className="adminCategoria">
          <div className="d-flex justify-content-center">
            <h3 id="title">
              CONFIGURACION DE LAS SUBCATEGORIAS DE {categoryname}
            </h3>
          </div>
          <br />
          <div className="p-3">
            <button className="btn btn-primary">
              <Link
                style={{ textDecoration: "none", color: "#fff" }}
                to="/dashboard/configurar-categorias"
              >
                <i class="fas fa-arrow-circle-left mr-2"></i>
                Volver
              </Link>
            </button>
          </div>

          <form id="form" action="" onSubmit={handleSubmit}>
            <div className="admin">
              <div className="col">
                <Form></Form>
              </div>
            </div>

            <hr />
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                width: "100%",
              }}
            >
              <div className="text-center">
                <button onClick={handleClickCrear} className="btn btn-primary">
                  <i class="fas fa-check"></i> Guardar
                </button>
              </div>
            </div>

            <div className="container">
              <div className="row">
                <div
                  className="col-7"
                  style={{ overflowY: "scroll", height: "80vh" }}
                >
                  <table className="table" style={{ border: "2px solid #ccc" }}>
                    <thead>
                      <tr>
                        <th># Valor</th>
                        <th>Titulo de la pregunta</th>
                        <th>Peso de la pregunta</th>
                        <th>Subcategoria</th>
                        <th>Agregar / Eliminar</th>
                      </tr>
                    </thead>
                    <tbody>
                      {inputList.map((item, i) => {
                        return (
                          <>
                            <tr>
                              <td>{i + 1}</td>
                              <td>
                                <input
                                  className="form-control"
                                  type="text"
                                  name="name_item"
                                  placeholder="Ej: ¿Cumples con las metas?"
                                  value={inputList.nameValue}
                                  onChange={(e) => handleInputChange(e, i)}
                                />
                              </td>

                              <td>
                                <input
                                  className="form-control"
                                  type="number"
                                  min="0"
                                  name="weight"
                                  value={inputList.weight}
                                  onChange={(e) => handleInputChange(e, i)}
                                />
                              </td>

                              <td>
                                <select
                                  onChange={(e) => handleInputChange(e, i)}
                                  className="form-control"
                                  name="id_value"
                                  id=""
                                  value={inputList.id_value}
                                >
                                  <option value={inputList.id_category}>
                                    Seleccionar
                                  </option>
                                  {subcategory.map((item, i) => (
                                    <option value={item.url} key={i}>
                                      {item.nameValue}
                                    </option>
                                  ))}
                                </select>
                              </td>

                              <td>
                                <div className="row">
                                  <div className="col-1 mr-2">
                                    {inputList.length - 1 === i && (
                                      <button
                                        className="btn btn-primary mr-3"
                                        onClick={handleAddClick}
                                      >
                                        <i class="fas fa-plus"></i>
                                      </button>
                                    )}
                                  </div>
                                  <div className="col-1">
                                    {inputList.length !== 1 && (
                                      <button
                                        onClick={() => handleRemoveClick(i)}
                                        className="btn btn-danger"
                                      >
                                        <i class="fas fa-minus"></i>
                                      </button>
                                    )}
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
                <div
                  className="col-5"
                  style={{ overflowY: "scroll", height: "80vh" }}
                >
                  <h5>Consultar indicadores por subcategoria</h5>
                  <select
                    onChange={(e) => handleChangeSelect(e.target.value)}
                    name=""
                    className="form-control"
                    id=""
                  >
                    <option value="">Seleccionar una subcategoria</option>
                    {subcategory.map((item) => (
                      <option value={item.id}> {item.nameValue} </option>
                    ))}
                  </select>
                  <hr />
                  <table className="table" style={{ border: "2px solid #ccc" }}>
                    <thead>
                      <tr>
                        <th> # </th>
                        <th>Nombre del Item</th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      {valuesData.map((item, index) => (
                        <tr>
                          <td> {index + 1} </td>
                          <td>{item.name_item}</td>
                          <td>
                            <div className="row">
                              <div className="col">
                                <button onClick={() => updateItem(item)} className="btn btn-info">
                                  <i class="fas fa-edit"></i>
                                </button>
                              </div>
                              <div className="col">
                                <button onClick={() => deleteItem(item.id)} className="btn btn-danger">
                                  <i class="fas fa-trash-alt"></i>
                                </button>
                              </div>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                <br />
              </div>
            </div>
          </form>
        </div>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Card</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="formGroupEmail">
              <Form.Label>Nombre Item</Form.Label>
              <Form.Control
                value={nameitem.name_item}
                name="name_item"
                onChange={(e) => handleOnChange(e.target.value)}
                type="text"
                placeholder="Nombre"
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancelar
          </Button>

          <Button variant="primary" onClick={onUpdate}>
            Actualizar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default ConfigurarSubcategorias;

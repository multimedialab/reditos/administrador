import React, { useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { apiToken } from "../Services/apiResponseCompany";
import axios from "axios";
import { url, body } from "../Services/auth";
import {
  getCompanies,
  getPositions,
  getCategories,
} from "../Services/positions_service";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

import "./styleAdministrarCategorias.css";

const AsignarCategorias = () => {
  const initialState = {};

  const [values, setValues] = useState(initialState);

  const [company, setCompany] = useState([]);
  const [positions, setPositions] = useState([]);
  const [category, setCategory] = useState([]);

  const [value, setValue] = React.useState(company[0]);
  const [inputValue, setInputValue] = React.useState('');

  const handleClickConsultar = () => {
    Swal.fire("Consultando...");
  };

  const handleClickCrear = () => {
    Swal.fire({
      icon: "success",
      title: "Creado con exito",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleClickLimpiar = () => {
    Swal.fire("Limpiando...");
  };

  const setData = async () => {
    const respCompanies = await getCompanies();
    setCompany(respCompanies);

    const respPositions = await getPositions();
    setPositions(respPositions);

    const respCategory = await getCategories();
    setCategory(respCategory);
  };

  useEffect(() => {
    setData();
  }, []);

  

  return (
    <>
      <div className="container p-5 mt-5">
        <h2>
          <strong style={{ fontSize: 24 }}>
            ASIGNAR CATEGORIAS DE EVALUACION Y PESOS GENERALES
          </strong>
        </h2>

        <div className="container p-5">
        <div>{`value: ${value !== null ? `'${value}'` : 'null'}`}</div>
        <div>{`inputValue: '${inputValue}'`}</div>
        </div>

        <div className="row mt-5">
          <div className="col-3">
            <div className="text-center">
              <Form>
                <Form.Label>Empresa</Form.Label>
              </Form>
            </div>
          </div>

          <div className="col-3">
            <Form>
            <Autocomplete
                id="combo-box-demo"
                value={value}
                onChange={(event, newValue) => {
                  setValue(newValue);
                }}
                inputValue={inputValue}
                onInputChange={(event, newInputValue) => {
                  setInputValue(newInputValue);
                }}
                options={company}
                getOptionLabel={(option) => option.companyName}
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Selecciona una opción "
                    variant="outlined"
                  />
                )}
              />
              
              {/* <select className="form-control">
                <option value="">Seleccione una opción</option>
                {company.map((item, i) => (
                  <option value={item.url} key={i}>
                    {item.companyName}
                  </option>
                ))}
              </select> */}
            </Form>
          </div>
        </div>



        <div className="row mt-5">
          <div className="col-3">
            <div className="text-center">
              <Form>
                <Form.Label style={{ float: "right" }}>
                  Categoria de cargos
                </Form.Label>
              </Form>
            </div>
          </div>

          <div className="col-3">
            <Form>

              <Autocomplete
                id="combo-box-demo"
                options={positions}
                getOptionLabel={(option) => option.nomCargo}
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Selecciona una opción "
                    variant="outlined"
                  />
                )}
              />

              {/* <select className="form-control" name="ideCargo">
                {positions.map((item, i) => (
                  <option value={item.url} key={i}>
                    {item.nomCargo}
                  </option>
                ))}
              </select> */}
            </Form>
          </div>
        </div>

        <div className="botones">
          <div className="col">
            <Button
              onClick={handleClickConsultar}
              className="btnConsulta"
              id="btnAdmin"
            >
              Consultar
            </Button>
          </div>
        </div>

        <div className="row mt-5" style={{ backgroundColor: "#004F9E" }}>
          <div className="col-12" style={{ backgroundColor: "#004F9E" }}>
            <p className="text-white text-center" style={{ marginTop: 10 }}>
              CARGOS
            </p>
          </div>
        </div>

        <div className="container mt-3">
          <div className="row">
            <div className="col-3">
              <ul style={{ listStyle: "none" }}>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
              </ul>
            </div>
            <div className="col-3">
              <ul style={{ listStyle: "none" }}>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
              </ul>
            </div>
            <div className="col-3">
              <ul style={{ listStyle: "none" }}>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
              </ul>
            </div>
            <div className="col-3">
              <ul style={{ listStyle: "none" }}>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
                <li>
                  <input
                    type="checkbox"
                    name=""
                    id=""
                    style={{ cursor: "pointer" }}
                  />{" "}
                  Analistas
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div className="row" style={{ backgroundColor: "#004F9E" }}>
          <div className="col-12">
            <p className="text-center text-white" style={{ marginTop: 10 }}>
              CATEGORIAS
            </p>
          </div>
        </div>

        <div className="container mt-2 ml-5">
          <table
            style={{ border: "2px solid #004F9E" }}
            className="table table-light"
          >
            <thead>
              <tr>
                <th scope="col">Categoria</th>
                <th scope="col">Pesos</th>
              </tr>
            </thead>
            <tbody>
              {category.map((item, i) => (
                <>
                  <tr key={i}>
                    <td>{item.categoryName}</td>
                    <td></td>
                  </tr>
                </>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};
export default AsignarCategorias;

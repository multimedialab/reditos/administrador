import React, { useState, useEffect } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import { apiToken } from "../Services/apiResponseCompany";
import {deleteCompany} from '../Services/apiResponseCollaborators';
//import {apiToken} from '../Services/apiResponseLeaders';
import { Form, Button } from "react-bootstrap";
import { url, body } from "../Services/auth";

import './styleAdmin.css';

const Colaboradores = () => {
  const initialState = {
    identification: "",
    completeName: "",
    idCompany: "",
    idLeader: "",
    idOffice: "",
  };

  const [values, setValues] = useState(initialState);
  const [info, setInfo] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [desCat, setDesCat] = useState("");
  const [isConsulted, setIsConsulted] = useState(false);

  const [company, setCompany] = useState([]);
  const [leader, setLeader] = useState([]);
  const [position, setPosition] = useState([]);

  const [IsUpdated, setIsUpdated] = useState(false);
  const [idCollaborator, setIdCollaborator] = useState("");

  const [company_name, setCompanyName] = useState("");


  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };

  //Get Collaborators

  const getCollaborators = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/collaborators/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => setInfo(json.results))
      .catch((error) => console.error(error));
  };


  //Get Companies

  const getCompanies = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/company/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => setCompany(json.results))
      .catch((error) => console.error(error));
  };

  //Get Leaders

  const getLeaders = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/leaders/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => setLeader(json.results))
      .catch((error) => console.error(error));
  };

  //Get Positions

  const getPositions = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/positions/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log('DATA POSITIONS: ', json.results);
        setPosition(json.results)})
      .catch((error) => console.error(error));
  };

  //OnChange Inputs

  const handleOnChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };

  //OnSubmit Inputs
  
  const handleSubmit = (e) => {
    console.log(values);
    e.preventDefault();
    document.getElementById('form').reset();
    setValues({ ...initialState });
  };

  const handleClickGuardar = () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleClickEliminar = () => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Eliminado!");
      }
    });
  };

  const handleClickConsultar = () => {
    //Swal.fire('Consultando...')
  };

  useEffect(() => {
    handleClickTodos();
    getCollaborators();
    getCompanies();
    getLeaders();
    getPositions();
  }, []);

  const handleClickTodos = () => {
    getCollaborators();
    setIsConsulted(true);
    /*
    if (Object.keys(info).length !== 0){
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'No hay registros',
        showConfirmButton: false,
        timer: 3000
      });
    }*/
  };

  const handleClickLimpiar = () => {
    setIsConsulted(false);
  };

  const handleClickCrear = async () => {
    const token = await apiToken();

    if (Object.keys(values).length !== 0) {
      fetch(`${url.urlBase}/collaborators/`, {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${token}`,
        },
      })
        .then((response) => response.json())
        .then((json) => {
          getCollaborators();
          setIsConsulted(true);
          Swal.fire({
            icon: "success",
            title: "Creado con exito",
            showConfirmButton: false,
            timer: 1500,
          });
          setValues({ ...initialState });
        })
        .catch((error) => console.error(error));
    } else {
      Swal.fire({
        icon: "error",
        title: "Los campos no pueden estar vacios",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };

  const handleClickActualizar = async () => {

    const token = await apiToken();
    const response = await axios.put(
      `${url.urlBase}/collaborators/${idCollaborator}/`,
      values,
      { headers: { Authorization: `JWT ${token}` } }
    );
    console.log(response);
    handleClickTodos();
    setValues({...initialState})
    document.getElementById('form').reset();
    Swal.fire({
      title: "Actualizado con exito",
      icon: "info",
    });
  };

  const handleClickCancel = () => {
    Swal.fire({
      title: "Cancelando...",
      icon: "warning",
    });
  };


  const handleDelete = async (id) => {
    console.log(id);
    console.log("DELETE ID: ", id);

    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#ccc",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        deleteCompany(id);
        Swal.fire("Eliminado!");
        handleClickTodos();
      }
    });
  };

  const updateInfoCat = async (id, identify, name, id_company, id_leader, company_name) => {
    const values = {
      identification: identify,
      completeName: name,
      id_company: id_company,
      id_leader: id_leader,
    };
    setIsUpdated(true);
    setIdCollaborator(id);
    setValues(values);
    setCompanyName(company_name);
    console.log("Values: ", values);
  };

  return (
    <>
      <div className="body col-lg-12 col-md-12 col-sm-9 col-xs-9">
        <div className="logoImg "></div>
        <div className="adminCategoria">
          <h2 id="title">
            Colaboradores
          </h2>
          <br />
          <form id="form" action="" onSubmit={handleSubmit}>
            <div className="admin">
              <div className="col">
                <label>ID Colaborador</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  disabled
                  type="number"
                  name="id"
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Numero de Identificacion</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="identification"
                  value={values.identification}
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Nombre del Colaborador</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="completeName"
                  value={values.completeName}
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Empresa a la que pertenece: </label>
              </div>
              <div className="col">
                <select
                  id="select"
                  onChange={handleOnChange}
                  className="form-control"
                  name="id_company"
                  
                >
                  <option value="">Seleccione una opción</option>
                  {company.map((item, i) => (
                    <option value={item.url} key={i}>
                      {item.companyName}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Lider a cargo: </label>
              </div>
              <div className="col">
                <select
                  id="select"
                  onChange={handleOnChange}
                  className="form-control"
                  name="id_leader"
                  
                >
                  <option value="">Seleccione una opción</option>
                  {leader.map((item, i) => (
                    <option value={item.url} key={i}>
                      {item.leaderName}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Cargo: </label>
              </div>
              <div className="col">
                <select
                  id="select"
                  onChange={handleOnChange}
                  className="form-control"
                  name="id_office"
                  
                >
                  <option value="">Seleccione una opción</option>
                  {position.map((item, i) => (
                    <option value={item.url} key={i}>
                      {item.name_office}
                    </option>
                  ))}
                </select>
              </div>
            </div>






            <div className="botones col-lg-12 col-md-12 col-sm-12">
              {/* <div className="col">
                <Button className="btnConsulta" id="btnAdmin" type="button">
                  Consultar
                </Button>
              </div> */}

              <div className="col">
                <Button
                  className="btn btn-info"
                  
                  onClick={handleClickTodos}
                  type="button"
                >
                  <i className="fas fa-eye mr-2"></i>
                  VER LISTADO
                </Button>
              </div>

              {/* <div className="col">
                <Button
                  className="btnLimpiar"
                  id="btnAdmin"
                  onClick={handleClickLimpiar}
                  type="button"
                >
                  Limpiar
                </Button>
              </div> */}

              <div className="col">
                <Button
                  className="btn btn-primary"
                  
                  onClick={handleClickCrear}
                  type="submit"
                >
                  <i className="fas fa-check mr-2"></i>
                  GUARDAR
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btn btn-success"
                  
                  onClick={handleClickActualizar}
                  type="button"
                >
                  <i className="fas fa-pencil-alt mr-2"></i>
                  ACTUALIZAR
                </Button>
              </div>

              {/* <div className="col">
                <Button
                  className="btnEliminar"
                  id="btnAdmin"
                  onClick={handleClickEliminar}
                  type="button"
                >
                  Eliminar
                </Button>
              </div> */}
              {/* <div className="col">
                <Button
                  className="btnGuardar"
                  id="btnAdmin"
                  onClick={handleClickGuardar}
                  type="submit"
                >
                  Guardar
                </Button>
              </div> */}

              {/* <div className="col">
                <Button
                  className="btnCancelar"
                  id="btnAdmin"
                  onClick={handleClickCancel}
                  type="button"
                >
                  Cancelar
                </Button>
              </div> */}

            </div>
          </form>
          {isConsulted ? (
            <div className="card-body ml-5 mr-3 mt-5" style={{height: '40vh', overflow: 'scroll', overflowX: 'hidden', top: 20, position: 'relative'}}>
              <table
                className="table mt-5"
                style={{ border: "2px solid #004F9E" }}
              >
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Numero de Identificacion</th>
                    <th>Nombre del Colaborador</th>
                    <th>Empresa</th>
                    <th>Cargo</th>
                    <th>Lider</th>
                    <th>Opciones</th>
                  </tr>
                </thead>

                <tbody>
                  {info.map((item, i) => (
                    <tr key={i}>
                      <td>{i + 1} </td>
                      <td> {item.identification} </td>
                      <td> {item.completeName} </td>
                      <td> {item.name_company} </td>
                      <td> {item.name_office} </td>
                      <td> {item.leader_name} </td>
                      <td>
                        <button
                          onClick={() => updateInfoCat(
                            item.id, 
                            item.identification,
                            item.completeName,
                            item.id_company,
                            item.id_leader,
                            item.url,
                            item.company_name,
                            )}
                          className="btn btn-info"
                        >
                          <i class="fas fa-edit"></i>
                        </button>{" "}
                        <button
                          onClick={() => handleDelete(item.id)}
                          className="btn btn-danger"
                        >
                          <i class="fas fa-trash-alt"></i>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </div>
    </>
  );
};
export default Colaboradores;

import React, { useState, useEffect, useRef } from "react";
import axios from "axios";

import Swal from "sweetalert2";
import { apiToken, deleteCompany } from "../Services/apiResponseLeaders";

import { Form, Button } from "react-bootstrap";

import { url, body } from "../Services/auth";

import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";



const CredencialesAdministrador = () => {

  useEffect(() => {
    handleClickTodos();
    getLeaders();
    getCompanies();
    getLeadersCredentials();
  }, []);


  const initialState = {
    identification: "",
    leaderName: "",
    idCompany: "",
  };

  const accessValues = {
    email: '',
    password: '',
    //Default Leader
    rol: 1,
    idLeader: '',


  }

  const [values, setValues] = useState(initialState);

  const [access, setAccess] = useState(accessValues);


  const [info, setInfo] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [desCat, setDesCat] = useState("");
  const [isConsulted, setIsConsulted] = useState(false);
  const [leader, setLeaders] = useState([]);

  //Acess
  const [leadersCredentials, setLeadersCredentials] = useState([]);
  const [company, setCompany] = useState([]);

  const [isUpdated, setIsUpdated] = useState(false);

  const [leaderNam, setLeaderNam] = useState("");

  const [company_name, setCompanyName] = useState("");


  const [identification, setIdentification] = useState("");
  const [idLeader, setIdLeader] = useState("");

  //state combo box
  const [value, setValue] = useState(company[0]);
  const [inputValue, setInputValue] = useState('');


  const [display, setDisplay] = useState(false);
  const [options, setOptions] = useState([]);
  const [search, setSearch] = useState("");

  const wrapperRef = useRef(null);

  const setCompanyDex = comp => {
    setSearch(comp);
    setDisplay(false);

  }

  /* useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    }
    
  }, []); */

  const handleClickOutside = event => {
    const {current: wrap} = wrapperRef;
    if (wrap && !wrap.contains(event.target)) {
      setDisplay(false);
    }
  }



  

  const api = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };
  //GetLeaders
  const getLeaders = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/leaders/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        setLeaders(json.results);
        console.log(json.results);
      })
      .catch((error) => console.error(error));
  };
  //GetCompanies
  const getCompanies = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/company/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
          console.log(json.results);
        setCompany(json.results)
        setOptions(json.results)
      
      })

      .catch((error) => console.error(error));
  };

  //Get LeadersCredentials
  const getLeadersCredentials = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/accessLogin/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {

        console.log(json.results);
        setLeadersCredentials(json.results)

        //setOptions(json.results)
      
      })

      .catch((error) => console.error(error));
  };


  //OnChange Inputs

  const handleOnChange = (e) => {
    setAccess({
      ...access,
      [e.target.name]: e.target.value,
    });
    console.log(e.target.value);
    console.log('values acess', access);
  };

  //OnSubmit Inputs

  const handleSubmit = (e) => {
    console.log(values);

    e.preventDefault();

    document.getElementById("form").reset();

    setValues({ ...initialState });
  };

  const handleClickGuardar = () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleClickEliminar = () => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Eliminado!");
      }
    });
  };

  const handleClickConsultar = () => {
    //Swal.fire('Consultando...')
  };

  

  const handleClickTodos = () => {
    getLeaders();
    getLeadersCredentials();
    setIsConsulted(true);
    /*
    if (Object.keys(info).length !== 0){
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'No hay registros',
        showConfirmButton: false,
        timer: 3000
      });
    }*/
  };

  const handleClickLimpiar = () => {
    setIsConsulted(false);
  };

  const handleClickCrear = async () => {
    const token = await api();

    if (Object.keys(access).length !== 0) {
      fetch(`${url.urlBase}/accessLogin/`, {
        method: "POST",
        body: JSON.stringify(access),
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${token}`,
        },
      })
        .then((response) => response.json())
        .then((json) => {
          getLeaders();
          setIsConsulted(true);
          Swal.fire({
            icon: "success",
            title: "Creado con exito",
            showConfirmButton: false,
            timer: 1500,
          });
          setValues({ ...initialState });
          handleClickTodos();
        })
        .catch((error) => console.error(error));
    } else {
      Swal.fire({
        icon: "error",
        title: "Los campos no pueden estar vacios",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };

  const handleClickActualizar = async () => {
    const token = await apiToken();
    const response = await axios.put(
      `${url.urlBase}/accessLogin/${idLeader}/`,
      access,
      { headers: { Authorization: `JWT ${token}` } }
    );
    console.log(response);
    handleClickTodos();

    Swal.fire({
      title: "Actualizado con exito",
      icon: "info",
    });
  };

  const handleClickCancel = () => {
    Swal.fire({
      title: "Cancelando...",
      icon: "warning",
    });
  };

  const handleDelete = async (id) => {
    console.log(id);
    console.log("DELETE ID: ", id);

    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#ccc",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        deleteCompany(id);
        handleClickTodos();
        
        Swal.fire("Eliminado!");
      }
    });
  };

  const updateInfoCat = async (id, email, password) => {
    const values = {
      email: email,
      password: password
      
      
    };
    setIsUpdated(true);
    setIdLeader(id);
    setAccess(values);
    //setCompanyName(company_name);
    console.log("Values: ", values);
  };

 

  

  return (
    <>
      <div className="body col-lg-12 col-md-12 col-sm-9 col-xs-9">
        <div className="logoImg "></div>
        <div className="adminCategoria">
          <h2>
            <strong style={{ fontSize: 24 }}>CREDENCIALES ADMINISTRADOR</strong>{" "}
          </h2>
          <br />
          <form id="form" action="" onSubmit={handleSubmit}>
            {/* <div className="admin">
              <div className="col">
                <label>ID Lider</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  disabled
                  type="number"
                  name="id"
                />
              </div>
            </div> */}

            {/* <div className="admin">
              <div className="col">
                <label>Numero de Identificacion</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="identification"
                  value={values.identification}
                />
              </div>
            </div> */}

            {/* <div className="admin">
              <div className="col">
                <label>Nombre del Lider</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="leaderName"
                  value={values.leaderName}
                />
              </div>
            </div> */}

            <div className="admin">
              <div className="col">
                <label>Seleccionar Lider:</label>
              </div>



              

              {/* <Autocomplete
                
                id="combo-box-demo"
                options={company}
                getOptionLabel={(option) => option.companyName}
                style={{ width: 300 }}
                getOptionSelected={(option) => option.url}
                


                value={value}
                onChange={(event, newValue) => {
                  setValue(newValue);
                }} 

                inputValue={inputValue}
                onInputChange={(event, newInputValue) => {
                  setInputValue(newInputValue);
                }}


                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Selecciona una opción "
                    variant="outlined"
                  />
                )}


              /> */}


              

              
              {/* <div>{`value: ${value !== null ? `'${value}'` : 'null'}`}</div>
              <div>{`inputValue: '${inputValue}'`}</div> */}
              

              <div className="col">
                {isUpdated ? (
                  <select
                    onChange={handleOnChange}
                    className="form-control"
                    name="id_company"
                  >
                    
                    {leader.map((item, i) => (
                      //item.companyName === company_name
                       /*  ?  */<option value={item.url} key={i}> {item.leaderName} </option>
                          /* : null */
                    ))}
                  </select>
                ) : (
                  
                   <select
                    onChange={handleOnChange}
                    className="form-control"
                    name="leader"
                  >
                    <option value={access.idLeader}>Seleccionar</option>
                    {leader.map((item, i) => (
                      <option value={item.url} key={i}>
                        {item.leaderName}
                      </option>
                    ))}
                  </select> 
                )}
              </div>
            </div>

            <hr/>

              
            <div className="admin">
              <div className="col">
                <label>Correo Electrónico: </label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  type="email"
                  name="email"
                  value={access.email}
                />
              </div>
            </div>
            <div className="admin">
              <div className="col">
                <label>Contraseña: </label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  type="text"
                  name="password"
                  value={access.password}
                />
              </div>
            </div>
            


            <div className="botones col-lg-12 col-md-12 col-sm-12">
              

              <div className="col">
                <Button
                  className="btnTodos"
                  id="btnAdmin"
                  onClick={handleClickTodos}
                  type="button"
                >
                  Todos
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btnLimpiar"
                  id="btnAdmin"
                  onClick={handleClickLimpiar}
                  type="button"
                >
                  Limpiar
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btn btn-sucess"
                  id="btnAdmin"
                  onClick={handleClickCrear}
                  type="submit"
                >
                  Crear
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btnActualizar"
                  id="btnAdmin"
                  onClick={handleClickActualizar}
                  type="button"
                >
                  Actualizar
                </Button>
              </div>

              
              <div className="col">
                <Button
                  className="btnGuardar"
                  id="btnAdmin"
                  onClick={handleClickGuardar}
                  type="submit"
                >
                  Guardar
                </Button>
              </div>
            </div>


            {/* <div ref={wrapperRef} className="flex-container flex-column pos-rel mt-3">
              <div className="row">
                <div className="col-4">
                  <input name="id_company" onChange={event => setSearch(event.target.value)} value={search} type="text" onClick={() => setDisplay(!display)}  className="form-control" placeholder="type to search" />
                  {display && (
                    <div  className="autoContainer">
                      {options
                      .filter( ({companyName}) => companyName.indexOf(search.toUpperCase()) > -1 )
                      .map((v, i) => {
                        return <div tabIndex="0" onClick={() => setCompanyDex(v.companyName)}  className="card" key={i}>
                          <span style={{cursor: 'pointer'}}>{v.companyName}</span>
                        </div>
                      })}
                    </div>
                  )}
                </div>
              </div>
            </div> */}



          </form>
          {isConsulted ? (
            <div
              className="card-body ml-5 mr-3 mt-5"
              style={{
                height: "40vh",
                overflow: "scroll",
                overflowX: "hidden",
                top: 80,
                position: "relative",
              }}
            >
              <table
                className="table mt-5"
                style={{ border: "2px solid #004F9E" }}
              >
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Numero de Identificacion</th>
                    <th>Nombre del Lider</th>
                    <th>Empresa</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Opciones</th>

                  </tr>
                </thead>

                <tbody>
                  {leadersCredentials.map((item, i) => (
                    <>
                      {item.leader !== null ? (
                        <>
                          <tr key={i}>
                            <td>{i + 1} </td>
                            <td> {item.leaderData.identification} </td>
                            <td> {item.leaderData.leaderName} </td>
                            <td> {item.leaderData.company_name} </td>
                            <td> {item.email} </td>
                            <td> {item.password} </td>
                            
                            <td>
                              <button
                                onClick={() =>
                                  updateInfoCat(
                                    item.id,
                                    item.email,
                                    item.password,
                                    item.url,
                                  )
                                }
                                className="btn btn-info"
                              >
                                <i class="fas fa-edit"></i>
                              </button>{" "}
                              <button
                                onClick={() => handleDelete(item.id)}
                                className="btn btn-danger"
                              >
                                <i class="fas fa-trash-alt"></i>
                              </button>
                            </td>
                          </tr>
                        </>
                      ) : null

                      }

                    </>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </div>
    </>
  );
};
export default CredencialesAdministrador;

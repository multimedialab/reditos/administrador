import React, { useState, useEffect } from "react";
import axios from "axios";

import {apiToken} from '../Services/apiResponseCompany';

import Swal from "sweetalert2";

import { Form, Button } from "react-bootstrap";

import {url, body} from '../Services/auth';

const Jefes = () => {
  const initialState = {};

  const [values, setValues] = useState(initialState);
  const [info, setInfo] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [desCat, setDesCat] = useState("");
  const [isConsulted, setIsConsulted] = useState(false);

  const [company, setCompany] = useState([]);

  

  const consult = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/bosses/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {setInfo(json.results)
        console.log(info);
      })
      .catch((error) => console.error(error));
  };

  const getCompanies = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/company/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {setCompany(json.results)
        console.log(company);
      })
      .catch((error) => console.error(error));
  };



  //OnChange Inputs

  const handleOnChange = (e) => {
    console.log(e.target.value);
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });

  };

  //OnSubmit Inputs

  const handleSubmit = (e) => {
    console.log(values);

    e.preventDefault();

    setValues({ ...initialState });
  };

  useEffect(() => {
    consult(); 
    getCompanies(); 
  },[])

  
  
  

  

  //Get Companies

  const handleClickGuardar = () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleClickEliminar = () => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Eliminado!");
      }
    });
  };

  const handleClickConsultar = () => {
    //Swal.fire('Consultando...')
  };

  const handleClickTodos = () => {
    consult();
    setIsConsulted(true);
    /*
    if (Object.keys(info).length !== 0){
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'No hay registros',
        showConfirmButton: false,
        timer: 3000
      });
    }*/
  };

  const handleClickLimpiar = () => {
    setIsConsulted(false);
  };

  const handleClickCrear = async () => {
    const token = await apiToken();

    if (Object.keys(values).length !== 0) {
      fetch(`${url.urlBase}/bosses/`, {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${token}`,
        },
      })
        .then((response) => response.json())
        .then((json) => {
          consult();
          setIsConsulted(true);
          Swal.fire({
            icon: "success",
            title: "Creado con exito",
            showConfirmButton: false,
            timer: 1500,
          });
        })
        .catch((error) => console.error(error));
    } else {
      Swal.fire({
        icon: "error",
        title: "Los campos no pueden estar vacios",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };

  const handleClickActualizar = () => {
    Swal.fire({
      title: "Actualizado con exito",
      icon: "info",
    });
  };

  const handleClickCancel = () => {
    Swal.fire({
      title: "Cancelando...",
      icon: "warning",
    });
  };

  return (
    <>
      <div className="body col-lg-12 col-md-12 col-sm-9 col-xs-9">
        <div className="logoImg "></div>
        <div className="adminCategoria">
          <h2>
            <strong style={{ fontSize: 24 }}>ADMINISTRAR JEFES</strong>{" "}
          </h2>
          <br />
          <form action="" onSubmit={handleSubmit}>
            <div className="admin">
              <div className="col">
                <label>ID Jefe</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  disabled
                  type="number"
                  name="id"
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Numero de Identificacion</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="identification"
                  value={values.identification}
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Nombre del Jefe</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="fullName"
                  value={values.fullName}
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>ID Empresa</label>
              </div>
              <div className="col">
                <select onChange={handleOnChange} className="form-control" name="id_company">
                    {company.map((item, i) => (
                      <option value={item.url}  key={i}>
                        
                        {item.companyName}
                      </option>
                    ))}
                </select>
              </div>
            </div>



            <div className="botones col-lg-12 col-md-12 col-sm-12">
              <div className="col">
                <Button className="btnConsulta" id="btnAdmin" type="button">
                  Consultar
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btnTodos"
                  id="btnAdmin"
                  onClick={handleClickTodos}
                  type="button"
                >
                  Todos
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btnLimpiar"
                  id="btnAdmin"
                  onClick={handleClickLimpiar}
                  type="button"
                >
                  Limpiar
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btnCrear"
                  id="btnAdmin"
                  onClick={handleClickCrear}
                  type="submit"
                >
                  Crear
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btnActualizar"
                  id="btnAdmin"
                  onClick={handleClickActualizar}
                  type="button"
                >
                  Actualizar
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btnEliminar"
                  id="btnAdmin"
                  onClick={handleClickEliminar}
                  type="button"
                >
                  Eliminar
                </Button>
              </div>
              <div className="col">
                <Button
                  className="btnGuardar"
                  id="btnAdmin"
                  onClick={handleClickGuardar}
                  type="submit"
                >
                  Guardar
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btnCancelar"
                  id="btnAdmin"
                  onClick={handleClickCancel}
                  type="button"
                >
                  Cancelar
                </Button>
              </div>
            </div>
          </form>
          {isConsulted ? (
            <table
              className="table mt-5"
              style={{ border: "2px solid #004F9E" }}
            >
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Numero de Indentificacion</th>
                  <th>Nombre del Jefe</th>
                  <th>Empresa</th>

                </tr>
              </thead>

              <tbody>
                {info.map((item, i) => (
                  <tr key={i}>
                    <td>{i + 1} </td>
                    <td> {item.identification} </td>
                    <td> {item.fullName} </td>
                    <td> {item.name_company} </td>

                  </tr>
                ))}
              </tbody>
            </table>
          ) : (
            <div></div>
          )}
        </div>
      </div>
    </>
  );
};
export default Jefes;

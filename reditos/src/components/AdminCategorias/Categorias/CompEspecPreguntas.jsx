import React, { useState, useEffect } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import { url, body } from "../Services/auth";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";


const CompEspecPreguntas = () => {
  const [inputList, setInputList] = useState([
    { name_item: "", description: "" ,weight: "", id_value: "" },
  ]);

  const initialState = {
    /*
    nameValue: "",
    weight: "",
    id_category: "",
    */
  };

  const [subcategory, setSubCategory] = useState([]);
  
  const [values, setValues] = useState(initialState);
  const [isConsulted, setIsConsulted] = useState(false);
  
  const handleClickCrear = async () => {
    const token = await getToken();

    console.log(values);
    console.log(inputList);

    for (let i = 0; i < inputList.length; i++){

      

      if (Object.keys(inputList).length !== 0) {
            fetch(`${url.urlBase}/items/`, {
              method: "POST",
              body: JSON.stringify(inputList[i]),
              headers: {
                "Content-Type": "application/json",
                Authorization: `JWT ${token}`,
              },
            })
              .then((response) => response.json())
              .then((json) => {
                getSubCategories();
                setIsConsulted(true);
                Swal.fire({
                  icon: "success",
                  title: "Creado con exito",
                  showConfirmButton: false,
                  timer: 1500,
                });
                
                console.log("Initial State: ", values);
                console.log("Inputs State: ", inputList);
                console.log()
              })
              .catch((error) => console.error(error));
          } else {
            Swal.fire({
              icon: "error",
              title: "Los campos no pueden estar vacios",
              showConfirmButton: false,
              timer: 1500,
            });
          }
      
      
    }

  };

  

  const handleSubmit = (e) => {
    //console.log(values);

    e.preventDefault();
    document.getElementById('form').reset();

    setValues({ ...initialState });
  };

  const handleAddClick = () => {
    setInputList([
      ...inputList,
      { name_item: "", description: "" ,weight: "", id_value: "" },
    ]);
  };

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };

  const handleRemoveClick = (index) => {
    const list = [...inputList];
    list.splice(index, 1);
    setInputList(list);
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
  };

  const getSubCategories = async () => {
    const token = await getToken();
    const respData = axios.get(`${url.urlBase}/values/`, {
      headers: { Authorization: `JWT ${token}` },
    });
    const data = (await respData).data;

    setSubCategory(data.results);
  };

  useEffect(() => {
    getSubCategories();
  }, []);

  return (
    <>
      <div className="body">
        <div className="adminCategoria">
          <h2 className="title" style={{ fontSize: 24 }}>
            {" "}
            <strong style={{ fontSize: 24 }}>
              PREGUNTAS COMPETENCIAS ESPECIFICAS
            </strong>{" "}
          </h2>
          <br />

          <button className="btn btn-primary">
              <Link style={{textDecoration: 'none', color: '#fff'}} to="/dashboard/competencias-especificas">
                Volver
              </Link>
            </button>

          <form id="form" action="" onSubmit={handleSubmit}>
            <div className="admin">
              <div className="col">
                <h4>Agregar items a evaluar por subcategoria</h4>
              </div>
              <div className="col">
                <Form>
                  {/* 
                  <select
                    onChange={}
                    className="form-control"
                    name="id_category"
                    id=""
                  >
                    <option value={inputList.id_category}>Seleccionar</option>
                    {category.map((item, i) => (
                      <option value={item.url} key={i}>
                        {item.categoryName}
                      </option>
                    ))}
                  </select>
                  */}
                  {/* 
                  <select
                    onChange={handleOnChange}
                    className="form-control"
                    name="id_category"
                    id=""
                  >
                    {interf.map((item, i) => (
                      <option value={item.url} key={i}>
                        {item.nameInterfaceField}
                      </option>
                    ))}
                  </select>
                  */}
                </Form>
              </div>
              {/* 
              <img src={lupaLogo} style={{ width: 30, height: 30 }} alt="" />
              */}
            </div>

            <hr />

            <div className="row">
              <div className="col-12 text-center">
                <div className="col-4">
                  <strong>{/*VALORES*/}</strong>
                </div>
                <div className="text-center"></div>
                {/* 
                <div style={{ marginTop: 20 }}>{JSON.stringify(inputList)}</div>
                */}
                {/* 
                {initialState.subcategories.map((subcategory, i) => (
                  <div key={i}>
                    <input type="text" value={subcategory} />
                  </div>
                ))}
                */}

                <br />
                <table
                  className="table"
                  style={{ border: "2px solid #004F9E" }}
                >
                  <thead>
                    <tr>
                      <th># Valor</th>
                      <th>Titulo de la pregunta</th>
                      
                      <th>Peso de la pregunta</th>
                      
                      <th>Subcategoria</th>
                      <th>Agregar / Eliminar</th>
                    </tr>
                  </thead>
                  <tbody>
                    {inputList.map((item, i) => {
                      return (
                        <>
                          <tr>
                            <td>{i + 1}</td>
                            <td>
                              <input
                                className="form-control"
                                type="text"
                                name="name_item"
                                placeholder="Ej: ¿Cumples con las metas?"
                                value={inputList.nameValue}
                                onChange={(e) => handleInputChange(e, i)}
                              />
                            </td>
                            
                            <td>
                              <input
                                className="form-control"
                                type="number"
                                min="0"
                                name="weight"
                                value={inputList.weight}
                                onChange={(e) => handleInputChange(e, i)}
                              />
                            </td>
                            
                            <td>
                              <select
                                onChange={(e) => handleInputChange(e, i)}
                                className="form-control"
                                name="id_value"
                                id=""
                                value={inputList.id_value}
                              >
                                <option value={inputList.id_category}>
                                  Seleccionar
                                </option>
                                {subcategory.map((item, i) => (
                                  item.name_category == 'COMPETENCIAS ESPECIFICAS' ?
                                  <option value={item.url} key={i}>
                                    {item.nameValue}
                                  </option> :
                                  null
                                ))}
                              </select>
                            </td>

                            <td>
                              <div className="row">
                                <div className="col-1 mr-2">
                                  {inputList.length - 1 === i && (
                                    <button
                                      className="btn btn-primary mr-3"
                                      onClick={handleAddClick}
                                    >
                                      <i class="fas fa-plus"></i>
                                    </button>
                                  )}
                                </div>
                                <div className="col-1">
                                  {inputList.length !== 1 && (
                                    <button
                                      onClick={() => handleRemoveClick(i)}
                                      className="btn btn-danger"
                                    >
                                      <i class="fas fa-minus"></i>
                                    </button>
                                  )}
                                </div>
                              </div>
                            </td>
                          </tr>
                        </>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
            <div className="text-center">
              <button onClick={handleClickCrear} className="btn btn-primary">
                <i class="fas fa-check"></i> Guardar
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};
export default CompEspecPreguntas;

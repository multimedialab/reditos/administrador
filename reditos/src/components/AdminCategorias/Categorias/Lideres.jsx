import React, { useState, useEffect, useRef } from "react";
import axios from "axios";

import Swal from "sweetalert2";
import { apiToken, deleteCompany, deleteLeader } from "../Services/apiResponseLeaders";

import { Form, Button } from "react-bootstrap";

import { url, body } from "../Services/auth";

import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

import './styleAdmin.css';



const Lideres = () => {


  const initialState = {
    identification: "",
    leaderName: "",
    idCompany: "",
  };

  const accessValues = {
    email: '',
    password: '',
    //Default Leader
    rol: 1


  }

  const [values, setValues] = useState(initialState);

  const [access, setAccess] = useState(accessValues);


  const [info, setInfo] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [desCat, setDesCat] = useState("");
  const [isConsulted, setIsConsulted] = useState(false);
  const [leader, setLeaders] = useState([]);
  const [company, setCompany] = useState([]);

  const [isUpdated, setIsUpdated] = useState(false);

  const [leaderNam, setLeaderNam] = useState("");

  const [company_name, setCompanyName] = useState("");


  const [identification, setIdentification] = useState("");
  const [idLeader, setIdLeader] = useState("");

  //state combo box
  const [value, setValue] = useState(company[0]);
  const [inputValue, setInputValue] = useState('');


  const [display, setDisplay] = useState(false);
  const [options, setOptions] = useState([]);
  const [search, setSearch] = useState("");

  const wrapperRef = useRef(null);

  const setCompanyDex = comp => {
    setSearch(comp);
    setDisplay(false);

  }

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    }
    
  }, []);

  const handleClickOutside = event => {
    const {current: wrap} = wrapperRef;
    if (wrap && !wrap.contains(event.target)) {
      setDisplay(false);
    }
  }



  

  const api = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };
  //GetLeaders
  const getLeaders = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/leaders/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        setLeaders(json.results);
        console.log(json.results);
      })
      .catch((error) => console.error(error));
  };
  //GetCompanies
  const getCompanies = async () => {
    const token = await apiToken();
    fetch(`${url.urlBase}/company/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
          console.log(json.results);
        setCompany(json.results)
        setOptions(json.results)
      
      })

      .catch((error) => console.error(error));
  };

  //OnChange Inputs

  const handleOnChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
    console.log(e.target.value);
    console.log('values', values);
  };

  //OnSubmit Inputs

  const handleSubmit = (e) => {
    console.log(values);

    e.preventDefault();

    document.getElementById("form").reset();

    setValues({ ...initialState });
  };

  const handleClickGuardar = () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleClickEliminar = () => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Eliminado!");
      }
    });
  };

  const handleClickConsultar = () => {
    //Swal.fire('Consultando...')
  };

  useEffect(() => {
    handleClickTodos();
    getLeaders();
    getCompanies();
  }, []);

  const handleClickTodos = () => {
    getLeaders();
    setIsConsulted(true);
    /*
    if (Object.keys(info).length !== 0){
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'No hay registros',
        showConfirmButton: false,
        timer: 3000
      });
    }*/
  };

  const handleClickLimpiar = () => {
    setIsConsulted(false);
  };

  const handleClickCrear = async () => {
    const token = await api();

    if (Object.keys(values).length !== 0) {
      fetch(`${url.urlBase}/leaders/`, {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${token}`,
        },
      })
        .then((response) => response.json())
        .then((json) => {
          getLeaders();
          setIsConsulted(true);
          Swal.fire({
            icon: "success",
            title: "Creado con exito",
            showConfirmButton: false,
            timer: 1500,
          });
          setValues({ ...initialState });
        })
        .catch((error) => console.error(error));
    } else {
      Swal.fire({
        icon: "error",
        title: "Los campos no pueden estar vacios",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };

  const handleClickActualizar = async () => {
    const token = await apiToken();
    const response = await axios.put(
      `${url.urlBase}/leaders/${idLeader}/`,
      values,
      { headers: { Authorization: `JWT ${token}` } }
    );
    console.log(response);
    handleClickTodos();

    Swal.fire({
      title: "Actualizado con exito",
      icon: "info",
    });
  };

  const handleClickCancel = () => {
    Swal.fire({
      title: "Cancelando...",
      icon: "warning",
    });
  };

  const handleDelete = async (id) => {
    console.log(id);
    console.log("DELETE ID: ", id);

    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#ccc",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        deleteLeader(id);
        Swal.fire("Eliminado!");
      }
    });
  };

  const updateInfoCat = async (id, identify, name, id_company, id_leader, company_name) => {
    const values = {
      identification: identify,
      leaderName: name,
      id_company: id_company,
      id_leader: id_leader,
    };
    setIsUpdated(true);
    setIdLeader(id);
    setValues(values);
    setCompanyName(company_name);
    console.log("Values: ", values);
  };

 

  

  return (
    <>
      <div className="body col-lg-12 col-md-12 col-sm-9 col-xs-9">
        <div className="logoImg "></div>
        <div className="adminCategoria">
          <h2 id="title">
            Lideres
          </h2>
          <br />
          <form id="form" action="" onSubmit={handleSubmit}>
            <div className="admin">
              <div className="col">
                <label>ID Lider</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  disabled
                  type="number"
                  name="id"
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Numero de Identificacion</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="identification"
                  value={values.identification}
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Nombre del Lider</label>
              </div>
              <div className="col">
                <input
                  onChange={handleOnChange}
                  className="form-control"
                  name="leaderName"
                  value={values.leaderName}
                />
              </div>
            </div>

            <div className="admin">
              <div className="col">
                <label>Empresa a la que pertenece:</label>
              </div>



              

              {/* <Autocomplete
                
                id="combo-box-demo"
                options={company}
                getOptionLabel={(option) => option.companyName}
                style={{ width: 300 }}
                getOptionSelected={(option) => option.url}
                


                value={value}
                onChange={(event, newValue) => {
                  setValue(newValue);
                }} 

                inputValue={inputValue}
                onInputChange={(event, newInputValue) => {
                  setInputValue(newInputValue);
                }}


                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Selecciona una opción "
                    variant="outlined"
                  />
                )}


              /> */}


              

              
              {/* <div>{`value: ${value !== null ? `'${value}'` : 'null'}`}</div>
              <div>{`inputValue: '${inputValue}'`}</div> */}
              

              <div className="col">
                {isUpdated ? (
                  <select
                    onChange={handleOnChange}
                    className="form-control"
                    name="id_company"
                  >
                    
                    {company.map((item, i) => (
                      item.companyName === company_name
                        ? <option value={item.url} key={i}> {item.companyName} </option>
                          : null
                    ))}
                  </select>
                ) : (
                  
                   <select
                    onChange={handleOnChange}
                    className="form-control"
                    name="id_company"
                  >
                    <option value={values.idCompany}>Seleccionar</option>
                    {company.map((item, i) => (
                      <option value={item.url} key={i}>
                        {item.companyName}
                      </option>
                    ))}
                  </select> 
                )}
              </div>
            </div>

           

              
            
            


            <div className="botones col-lg-12 col-md-12 col-sm-12">
              

              <div className="col">
                <Button
                  className="btn btn-info"
                  
                  onClick={handleClickTodos}
                  type="button"
                >
                  <i className="fas fa-eye mr-2"></i>
                  VER LISTADO
                </Button>
              </div>

              {/* <div className="col">
                <Button
                  className="btnLimpiar"
                  id="btnAdmin"
                  onClick={handleClickLimpiar}
                  type="button"
                >
                  Limpiar
                </Button>
              </div> */}

              <div className="col">
                <Button
                  className="btn btn-primary"
                  
                  onClick={handleClickCrear}
                  type="submit"
                >
                  <i className="fas fa-check mr-2"></i>
                  GUARDAR
                </Button>
              </div>

              <div className="col">
                <Button
                  className="btn btn-success"
                  
                  onClick={handleClickActualizar}
                  type="button"
                >
                  <i class="fas fa-pencil-alt mr-2"></i>
                  ACTUALIZAR
                </Button>
              </div>

              
              {/* <div className="col">
                <Button
                  className="btnGuardar"
                  id="btnAdmin"
                  onClick={handleClickGuardar}
                  type="submit"
                >
                  Guardar
                </Button>
              </div> */}

            </div>


            {/* <div ref={wrapperRef} className="flex-container flex-column pos-rel mt-3">
              <div className="row">
                <div className="col-4">
                  <input name="id_company" onChange={event => setSearch(event.target.value)} value={search} type="text" onClick={() => setDisplay(!display)}  className="form-control" placeholder="type to search" />
                  {display && (
                    <div  className="autoContainer">
                      {options
                      .filter( ({companyName}) => companyName.indexOf(search.toUpperCase()) > -1 )
                      .map((v, i) => {
                        return <div tabIndex="0" onClick={() => setCompanyDex(v.companyName)}  className="card" key={i}>
                          <span style={{cursor: 'pointer'}}>{v.companyName}</span>
                        </div>
                      })}
                    </div>
                  )}
                </div>
              </div>
            </div> */}



          </form>
          {isConsulted ? (
            <div
              className="card-body ml-5 mr-3 mt-5"
              style={{
                height: "40vh",
                overflow: "scroll",
                overflowX: "hidden",
                top: 80,
                position: "relative",
              }}
            >
              <table
                className="table mt-5"
                style={{ border: "2px solid #004F9E" }}
              >
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Numero de Identificacion</th>
                    <th>Nombre del Lider</th>
                    <th>Empresa</th>
                    <th>Opciones</th>
                  </tr>
                </thead>

                <tbody>
                  {leader.map((item, i) => (
                    <tr key={i}>
                      <td>{i + 1} </td>
                      <td> {item.identification} </td>
                      <td> {item.leaderName} </td>
                      <td> {item.company_name} </td>
                      <td>
                        <button
                          onClick={() =>
                            updateInfoCat(
                              item.id,
                              item.identification,
                              item.leaderName,
                              item.id_company,
                              item.url,
                              item.company_name,
                            )
                          }
                          className="btn btn-info"
                        >
                          <i class="fas fa-edit"></i>
                        </button>{" "}
                        <button
                          onClick={() => handleDelete(item.id)}
                          className="btn btn-danger"
                        >
                          <i class="fas fa-trash-alt"></i>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </div>
    </>
  );
};
export default Lideres;

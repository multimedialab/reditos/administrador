import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";

import Swal from "sweetalert2";

import "./styleAdmin.css";

import { sendEmails } from "../Services/sendNotifications";

import {
  createDate,
  getDate,
  updateData,
  deleteData,
} from "../Services/apiDate";
import SwipeableTemporaryDrawer from "../../Drawer/DrawerMenu";

const Notificaciones = (props) => {

  useEffect(() => {
    console.log('props => ', props)
    getInfo();
  }, []);

  const initialState = {
    emails: [],
  };

  const emails =
    "juan@gmail.com; sebas@gmail.com; gio@gmail.com;   alvarez7@gmail.com";

  const [valuesDate, setValuesDate] = useState("");
  const [dateNotification, setDateNotification] = useState("");
  const [state, setstate] = useState([]);
  const [idDate, setIdDate] = useState("");

  const handleClickGuardar = async () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });

    await createDate(valuesDate);

    console.log(valuesDate);

    getInfo();
  };

  const handleClickActualizar = async () => {
    Swal.fire({
      icon: "success",
      title: "Guardado con exito",
      showConfirmButton: false,
      timer: 1500,
    });

    await updateData(idDate, valuesDate);

    console.log(valuesDate);
  };

  const getInfo = async () => {
    const resp = await getDate();

    setstate(resp);

    console.log("resp => ", resp);
  };

  const updateInfoCat = async (
    id,
    initDateTracingOne,
    endDateTracingOne,
    initDateTracingTwo,
    endDateTracingTwo
  ) => {
    const values = {
      initDateTracingOne: initDateTracingOne,
      endDateTracingOne: endDateTracingOne,
      initDateTracingTwo: initDateTracingTwo,
      endDateTracingTwo: endDateTracingTwo,
    };
    //setIsUpdated(true);
    setIdDate(id);
    setValuesDate(values);
    //setCompanyName(company_name);
    console.log("Values: ", values);
  };

  const handleDelete = async (id) => {
    Swal.fire({
      title: "¿Estas seguro de eliminar?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.value) {
        deleteData(id);

        //Swal.fire("Eliminado!");
        getInfo();
      }
    });
  };

  const handleChange = (e) => {
    setValuesDate(([e.target.name] = e.target.value));
    console.log(valuesDate);
  };

  const convertStringToEmails = async () => {
    var expresionRegular = /\s*;\s*/;
    var listaDeCorreos = valuesDate.split(expresionRegular);
    console.log("correos agrupados => ", listaDeCorreos);

    var dateFormat = dateNotification.split("-").reverse().join("-");

    console.log("date format => ", dateFormat);

    let obj = {
      emails: listaDeCorreos,
      date: dateFormat,
      subject: "",
      type:0
    };

    await sendEmails(obj);
  };

  return (
    <>
      {/* <SwipeableTemporaryDrawer history={props.history}/> */}
      <div className="container p-5 mt-5">
        <h2 id="title">Notificaciones</h2>

        <br />

        <div style={{maxHeight: 50}}>
          <div className="row mt-12">
            <div className="col-12">
              <h5>
                A continuacion ingresa los correos electronicos separados por
                punto y coma (;)
              </h5>
            </div>

            <div className="col-3">
              <Form>
                <textarea
                  style={{ maxHeight: 250 }}
                  onChange={handleChange}
                  name="emails"
                  id=""
                  cols="80"
                  rows="10"
                ></textarea>
              </Form>
            </div>
          </div>

          <div className="row mt-12">
            <div className="col-12">
              <h5>
                Ingresa fecha limite para realizar el seguimiento (Esta fecha
                sera visible en la notificacion)
              </h5>
            </div>

            <div className="col-3">
              <Form>
                <input
                  onChange={(e) => {
                    setDateNotification(e.target.value);
                    console.log("date => ", e.target.value);
                  }}
                  className="form-control"
                  type="date"
                  name=""
                  id=""
                />
              </Form>
            </div>
          </div>

          <div className="row">
            <div className="text-center mt-5">
              <div className="col">
                <Button
                  onClick={/* handleClickGuardar */ convertStringToEmails}
                  className="btn btn-primary"
                >
                  <i class="fas fa-bell mr-2"></i>
                  ENVIAR NOTIFICACIÓN
                </Button>
              </div>
            </div>

            <div className="text-center mt-5">
              {/* <div className="col">
              <Button
                onClick={handleClickActualizar}
                className="btn btn-success"
              >
                <i class="fas fa-pencil-alt mr-2"></i>
                ACTUALIZAR
              </Button>
            </div> */}
            </div>
          </div>
        </div>

        {/* <div
          className="card-body ml-5 mr-3 mt-5"
          style={{
            height: "40vh",
            overflow: "scroll",
            overflowX: "hidden",
            top: 80,
            position: "relative",
          }}
        >
          <table className="table mt-5" style={{ border: "2px solid #004F9E" }}>
            <thead>
              <tr>
                <th>ID</th>
                <th>Fecha Inicio - 1° Seguimiento</th>
                <th>Fecha Fin - 1° Seguimiento</th>
                <th>Fecha Inicio - 2° Seguimiento</th>
                <th>Fecha Fin - 2° Seguimiento</th>
                <th>Opciones</th>
              </tr>
            </thead>

            <tbody>
              {state.map((item, i) => (
                <>
                  {item.leader !== null ? (
                    <>
                      <tr key={i}>
                        <td>{i + 1} </td>
                        <td> {item.initDateTracingOne} </td>
                        <td> {item.endDateTracingOne} </td>
                        <td> {item.initDateTracingTwo} </td>
                        <td> {item.endDateTracingTwo} </td>

                        <td>
                          <button
                            onClick={() =>
                              updateInfoCat(
                                item.id,
                                item.initDateTracingOne,
                                item.endDateTracingOne,
                                item.initDateTracingTwo,
                                item.endDateTracingTwo,
                                item.url
                              )
                            }
                            className="btn btn-info"
                          >
                            <i class="fas fa-edit"></i>
                          </button>{" "}
                          <button
                            onClick={() => handleDelete(item.id)}
                            className="btn btn-danger"
                          >
                            <i class="fas fa-trash-alt"></i>
                          </button>
                        </td>
                      </tr>
                    </>
                  ) : null}
                </>
              ))}
            </tbody>
          </table>
        </div> */}
      </div>
    </>
  );
};
export default Notificaciones;

import React, { useEffect, useState } from "react";

import Swal from "sweetalert2";

import "./styleAdmin.css";

import { getCompanies } from "../Services/apiResponseCompany";
import {getLeadersByCompany, getCollaboratorsByLeader} from '../Services/apiRelationships';

const ReporteRelacion = (props) => {

  const [companies, setCompanies] = useState([]);
  const [leaders, setLeaders] = useState([]);
  const [collaborators, setCollaborators] = useState([]);
  
  const handleGetCompanies = async () => {
    const resp = await getCompanies();
    setCompanies(resp);
  };

  const handleGetLeaderByCompanies = async id_company => {
    const resp = await getLeadersByCompany(id_company);
    setLeaders(resp);
    console.log('leaders for company => ',resp)
  }

  const handleGetCollaboratorsByLeader = async id_leader => {
    const resp = await getCollaboratorsByLeader(id_leader);
    setCollaborators(resp);
    console.log('collaborators for leader => ',resp)
  }





  useEffect(() => {
    handleGetCompanies();
    const { history } = props;

    /* Swal.fire({
      icon: 'warning',
      title: 'Opss...',
      text: 'Este módulo se encuentra actualmente en desarrollo, porfavor intentado mas tarde-',
      
    }).then(() => history.push('/dashboard/configurar-categorias')) */
  }, []);

  const handleClickConsultar = () => {
    Swal.fire({
      icon: "info",
      title: "Consultando...",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleClickLimpiar = () => {
    Swal.fire({
      icon: "info",
      title: "Limpiando...",
      showConfirmButton: false,
      timer: 1500,
    });
  };
  return (
    <>
      <div className="container p-5 mt-5">
        <h2 id="title">Reporte de Relacion de Lideres Y Colaboradores</h2>
        <div className="row mt-5">
          <div className="col-2">
            <h5>Empresa</h5>
          </div>
          <div className="col-5">
            <select onChange={(e) => handleGetLeaderByCompanies(e.target.value)} className="form-control" name="" id="">
              <option value="">Selecciona una opción</option>
              {companies.map((item) => (
                <option value={item.id}> {item.companyName} </option>
              ))}
            </select>
          </div>
        </div>

        <div className="row mt-5">
          <div className="col-2">
            <h5>Lideres</h5>
          </div>
          <div className="col-5">
            <select onChange={(e) => handleGetCollaboratorsByLeader(e.target.value)} className="form-control" name="" id="">
            <option value="">Selecciona una opción</option>

              {leaders.map((item) => (
                <option value={item.id}> {item.leaderName} </option>
              ))}
            </select>
          </div>
        </div>



        

        


        {/* <div className="text-center mt-5">
          <button
            className="btnConsulta"
            id="btnAdmin"
            style={{ borderRadius: 5 }}
            onClick={handleClickConsultar}
          >
            Consultar
          </button>
        </div> */}
        <hr />
        <div className="text-center">
          <h4>COLABORADORES A CARGO</h4>
        </div>

        <div className="row mt-5">
          <table
            className="table"
            style={{ backgroundColor: "white", border: "2px solid #004F9E" }}
          >
            <thead>
              <tr>
                <th> N° </th>
                <th>Identificación</th>
                <th style={{ color: "#004F9E", textAlign: "center" }}>
                  Nombre
                </th>
              </tr>
            </thead>
            <tbody>

              {collaborators.map((item, index) => (
                <>
                  <tr>
                    <td> {index + 1} </td>
                    <td className="text-center"> {item.identification} </td>
                    <td style={{ textAlign: "center" }}> {item.completeName} </td>
                  </tr>

                </>
              ))

              }


              
            </tbody>
          </table>
        </div>

        

      </div>
    </>
  );
};
export default ReporteRelacion;

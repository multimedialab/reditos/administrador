import React from "react";
import Menu from ".";

function Layout(props) {
  return (
    <>
      <div className="col-2">
        <Menu />
      </div>
      <div className="col-10">{props.children}</div>
    </>
  );
}
export default Layout;

import React, { useState, useEffect } from "react";
import "./styleMenu.css";
import { Container, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import axios from "axios";
import { body, url } from "../AdminCategorias/Services/auth";

const Menu = () => {
  /* constructor(props) {
    super(props);

    this.state = []
  } */

  const [state, setstate] = useState([]);

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };

  const getCategories = async () => {
    const token = await getToken();
    const respData = axios.get(`${url.urlBase}/category/`, {
      headers: { Authorization: `JWT ${token}` },
    });
    const data = (await respData).data;
    setstate(data.results);
  };

  /* componentDidMount = () => {
    this.getCategories();
    console.log('Categories Menu: ', this.state)
  } */

  useEffect(() => {
    getCategories();
  }, []);

  

  return (

    
    <div className="background">
      <div>
        
      </div>
      <div className="icon1">
        <img
          src={require("../../assets/1x/iconProfile.png")}
          className="iconProfile mt-5"
        ></img>
      </div>
      <Container className="c">
        <Navbar>
          <div className="mt-5 ml-5">
            <Navbar.Brand className="opcion">

              <div style={{width: '100%'}} className="dropdown d-flex justify-content-center">
                <a
                  className="btn btn-info dropdown-toggle"
                  href="#"
                  role="button"
                  id="dropdownMenuLink"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  CATEGORIAS
                </a>

                <div
                  style={{ backgroundColor: "#fff" }}
                  className="dropdown-menu"
                  aria-labelledby="dropdownMenuLink"
                >
                   {/* {state.map((item, i) => (
                    <>
                      <a key={i} className="dropdown-item" href="#">
                        <Navbar>
                          <Navbar.Brand className="opcion">
                            <Link
                              
                              to="/dashboard/configurar-categorias"
                              style={{ textDecoration: "none" }}
                            >
                              <div
                                style={{ color: "#000", float: "left" }}
                                className="titleMenu"
                              >
                                
                                {item.categoryName !== 'OBJETIVOS' ?
                                  item.categoryName
                                  :
                                  null
                                }
                              </div>
                            </Link>
                          </Navbar.Brand>
                        </Navbar>
                      </a>
                    </>
                  ))} */}
                  <a className="dropdown-item" href="#">
                      <Navbar>
                        <Navbar.Brand className="opcion">
                          <Link
                            to="/dashboard/configurar-categorias"
                            style={{ textDecoration: "none" }}
                          >
                            <div
                              className="titleMenu"
                              style={{ color: "#000", float: "left" }}
                            >
                              VALORES CORPORATIVOS
                            </div>
                          </Link>
                        </Navbar.Brand>
                      </Navbar>
                    </a>



                  <a className="dropdown-item" href="#">
                      <Navbar>
                        <Navbar.Brand className="opcion">
                          <Link
                            to="/dashboard/objetivos"
                            style={{ textDecoration: "none" }}
                          >
                            <div
                              className="titleMenu"
                              style={{ color: "#000", float: "left" }}
                            >
                              OBJETIVOS
                            </div>
                          </Link>
                        </Navbar.Brand>
                      </Navbar>
                    </a>



                  
                    <a className="dropdown-item" href="#">
                      <br />
                      <Navbar>
                        <Navbar.Brand className="opcion">
                          <Link
                            to="/dashboard/competencias-organizacionales"
                            style={{ textDecoration: "none" }}
                          >
                            <div
                              style={{ color: "#000", float: "left" }}
                              className="titleMenu"
                            >
                              COMPETENCIAS ORGANIZACIONALES
                            </div>
                          </Link>
                        </Navbar.Brand>
                      </Navbar>
                    </a>

                    <a className="dropdown-item" href="#">
                      <Navbar>
                        <Navbar.Brand className="opcion">
                          <Link
                            to="/dashboard/competencias-especificas"
                            style={{ textDecoration: "none" }}
                          >
                            <div
                              style={{ color: "#000", float: "left" }}
                              className="titleMenu"
                            >
                              COMPETENCIAS ESPECIFICAS
                            </div>
                          </Link>
                        </Navbar.Brand>
                      </Navbar>
                    </a>

                    
                    <a className="dropdown-item" href="#">
                      <Navbar>
                        <Navbar.Brand className="opcion">
                          <Link
                            to="/dashboard/teletrabajo"
                            style={{ textDecoration: "none" }}
                          >
                            <div
                              style={{ color: "#000", float: "left" }}
                              className="titleMenu"
                            >
                              TELETRABAJO
                            </div>
                          </Link>
                        </Navbar.Brand>
                      </Navbar>
                    </a>

                     
                </div>
              </div>
            </Navbar.Brand>
          </div>
        </Navbar>
        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/administrar-categorias"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">CREAR CATEGORIAS</div>
            </Link>
          </Navbar.Brand>
        </Navbar>

        {/* <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/asignar-categorias"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">
                ASIGNAR CATEGORIAS
              </div>
            </Link>
          </Navbar.Brand>
        </Navbar> */}

        {/* <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/consulta-evaluacion"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">
                CONSULTA DE EVALUACIÓN
              </div>
            </Link>
          </Navbar.Brand>
        </Navbar> */}

        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/administrar-fechas"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">ADMINISTRAR FECHAS</div>
            </Link>
          </Navbar.Brand>
        </Navbar>
        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/crear-preguntas-checklist"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">ADMINISTRAR CHECKLIST</div>
            </Link>
          </Navbar.Brand>
        </Navbar>

        {/* <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/desbloquear-seguimiento"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">
                DESBLOQUEAR SEGUIMIENTO 
              </div>
            </Link>
          </Navbar.Brand>
        </Navbar> */}

        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/reporte-relacion"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">
                REPORTE DE RELACION LIDERES - COLABORADORES
              </div>
            </Link>
          </Navbar.Brand>
        </Navbar>
        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/reporte-checklist"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">REPORTE DE CHECKLIST</div>
            </Link>
          </Navbar.Brand>
        </Navbar>
        <br />

        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link to="/dashboard/empresas" style={{ textDecoration: "none" }}>
              <div className="titleMenu text-center">EMPRESAS</div>
            </Link>
          </Navbar.Brand>
        </Navbar>
        {/* 
                    <br/>
                    <Navbar>
                        <Navbar.Brand className='opcion'>
                            <Link to='/dashboard/jefes' style={{textDecoration: 'none'}}>
                                <div className="titleMenu text-left">
                                Jefes
                                </div>
                                </Link>
                        </Navbar.Brand>
                    </Navbar>
                    */}
        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link to="/dashboard/lideres" style={{ textDecoration: "none" }}>
              <div className="titleMenu text-center">LIDERES</div>
            </Link>
          </Navbar.Brand>
        </Navbar>
        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/colaboradores"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">COLABORADORES</div>
            </Link>
          </Navbar.Brand>
        </Navbar>

        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/cargos"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">CARGOS</div>
            </Link>
          </Navbar.Brand>
        </Navbar>

        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/credenciales-lideres"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">CREDENCIALES LIDERES</div>
            </Link>
          </Navbar.Brand>
        </Navbar>

        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/credenciales-colaboradores"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">CREDENCIALES COLABORADORES</div>
            </Link>
          </Navbar.Brand>
        </Navbar>

        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/notificaciones"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">NOTIFICACIONES</div>
            </Link>
          </Navbar.Brand>
        </Navbar>

        <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/drawer"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">DRAWER</div>
            </Link>
          </Navbar.Brand>
        </Navbar>





        {/* <br />
        <Navbar>
          <Navbar.Brand className="opcion">
            <Link
              to="/dashboard/credenciales-administrador"
              style={{ textDecoration: "none" }}
            >
              <div className="titleMenu text-center">CREDENCIALES ADMINISTRADOR</div>
            </Link>
          </Navbar.Brand>
        </Navbar> */}



        {/* 
                    <Navbar>
                        <Navbar.Brand className='opcion'>
                            <Link to='/' className='txtOpcion'>
                                <div className="text-center">

                                Configurar Personas a Cargo
                                </div>
                                </Link>
                        </Navbar.Brand>
                    </Navbar>
                    */}

        {/* 
                    <Navbar>
                        <Navbar.Brand className='opcion'>
                            <Link to='/' className='txtOpcion'>Cerrar Sesion</Link>
                        </Navbar.Brand>
                    </Navbar>
                    */}
        <br />
      </Container>
      <br />
      <div className="icon2">
        <img
          src={require("../../assets/1x/logo.png")}
          className="iconLogo"
        ></img>
      </div>
    </div>
  );
};

export default Menu;

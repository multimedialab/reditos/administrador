import React, { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { CSSTransition } from "react-transition-group";
import {
  Link,
  Route,
  BrowserRouter,
  Switch,
  withRouter,
} from "react-router-dom";
import "./Dinamic.css";
import {
  setCategories,
  setCategoryName,
  setCategoryUrl,
  setIdCategory,
  setOpen,
} from "../../redux/actions/menuActions";
import { getCategories } from "../AdminCategorias/Services/apiResponseCategory";

function Dinamic(props) {
  //const dispatch = useDispatch();

  return (
    <BrowserRouter>
      <Switch>
        {/* <Navbar> */}
        <NavItem icon="⚙">
          <DropdownMenu props={props} />
        </NavItem>
        {/* </Navbar> */}
      </Switch>
    </BrowserRouter>
  );
}

function Navbar(props) {
  return (
    <nav className="navbar">
      <ul className="navbar-nav"> {props.children} </ul>
    </nav>
  );
}

function NavItem(props, band) {
  const { openRedux } = useSelector((state) => state.menuReducer);
  const dispatch = useDispatch();

  /* const [open, setOpen] = useState(false); */

  return (
    <li className="nav-item">
      <a
        href="#"
        className="icon-button"
        onClick={() => dispatch(setOpen(!openRedux)) /* setOpen(!openRedux) */}
      >
        {props.icon}
      </a>

      {openRedux && props.children}
    </li>
  );
}

function DropdownMenu(props) {
  const { categories } = useSelector((state) => state.menuReducer);

  const getCategoryList = useCallback(async () => {
    const resp = await getCategories();
    dispatch(setCategories(resp));
  });

  useEffect(() => {
    getCategoryList();
  }, []);

  const dispatch = useDispatch();

  const { categoryname } = useSelector((state) => state.menuReducer);
  const { categoryUrl } = useSelector((state) => state.menuReducer);

  const setData = (url, name, id) => {
    dispatch(setCategoryUrl(url));
    dispatch(setCategoryName(name));
    dispatch(setIdCategory(id));

    console.log("params => ", url, name);

    //window.location.replace('/dashboard/configurar-categorias')
    console.log("categoryname => ", categoryname);
    console.log("url category => ", categoryUrl);
    props.props.history.push("/dashboard/configurar-categorias");
  };

  const [activeMenu, setActiveMenu] = useState("main");
  const [menuHeight, setMenuHeight] = useState(null);

  function calcHeight(el) {
    const height = el.offsetHeight;
    setMenuHeight(height);
  }

  function DropdownItem(props) {
    return (
      <a
        style={{ color: "#fff", textDecoration: "none" }}
        href="#"
        className="menu-item"
        onClick={() => props.goToMenu && setActiveMenu(props.goToMenu)}
      >
        <span className="icon-button"> {props.leftIcon} </span>
        {props.children}
        <span className="icon-right"> {props.rightIcon} </span>
      </a>
    );
  }

  return (
    <div className="dropdown" /* style={{height: menuHeight}} */>
      <CSSTransition
        in={activeMenu === "main"}
        unmountOnExit
        timeout={500}
        classNames="menu-primary"
        /* onEnter={calcHeight} */
      >
        <div className="menu">
          <DropdownItem leftIcon="👨‍💻">Bienvenido, Admin</DropdownItem>

          <DropdownItem
            leftIcon="📈"
            rightIcon=""
            goToMenu="categories"
          >
            <p className="itemMenu">Categorías</p>
          </DropdownItem>

          <DropdownItem
            leftIcon="📝"
            rightIcon=""
            goToMenu="tracing"
          >
            <p className="itemMenu">Seguimiento</p>
          </DropdownItem>

          <DropdownItem
            leftIcon="👨‍👨‍👦"
            rightIcon=""
            goToMenu="personal"
          >
            <p className="itemMenu">Gestionando el personal</p>
          </DropdownItem>

          <DropdownItem
            leftIcon="📊"
            rightIcon=""
            goToMenu="resultado"
          >
            <p className="itemMenu">Analizando el resultado</p>
          </DropdownItem>

          <DropdownItem
            leftIcon="✅"
            rightIcon=""
            goToMenu="board"
          >
            <p className="itemMenu">Gestionar Tablero</p>
          </DropdownItem>

          <DropdownItem
            leftIcon="🔧"
            rightIcon=""
            goToMenu="settings"
          >
            <p className="itemMenu">Configuración</p>
          </DropdownItem>

          {
            sessionStorage.getItem('user') ? (
              <DropdownItem
                leftIcon="🔴"
                rightIcon=""
                
              >
                <p onClick={() => { 
                  props.props.history.push('/');
                  sessionStorage.clear();
                }
                  } className="itemMenu">Salir</p>
              </DropdownItem>
            ) : null
          }


          




        </div>
      </CSSTransition>

      <CSSTransition
        in={activeMenu === "categories"}
        unmountOnExit
        timeout={500}
        classNames="menu-secondary"
      >
        <div className="menu">
          <DropdownItem
            goToMenu="main"
            leftIcon="◀"
            rightIcon=""
          ></DropdownItem>



          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/administrar-categorias");
            }}
          >
            <DropdownItem leftIcon="➕ ">
              <p className="itemMenu"> CREAR NUEVA CATEGORIA </p>
            </DropdownItem>
          </div>

          {categories.map((item) => (
            <>
              <div
                style={{ textDecoration: "none" }}
                onClick={() => {
                  dispatch(setOpen(false));
                  setData(item.url, item.categoryName, item.id);
                }}
              >
                {item.categoryName !== "OBJETIVOS" ? (
                  <DropdownItem leftIcon="📢">
                    <p className="itemMenu"> {item.categoryName} </p>
                  </DropdownItem>
                ) : null}
              </div>
            </>
          ))}

          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/objetivos");
            }}
          >
            <DropdownItem leftIcon="📢">
              <p className="itemMenu"> OBJETIVOS </p>
            </DropdownItem>
          </div>
        </div>
      </CSSTransition>

      <CSSTransition
        in={activeMenu === "tracing"}
        unmountOnExit
        timeout={500}
        classNames="menu-secondary"
      >
        <div className="menu">
          <DropdownItem
            goToMenu="main"
            leftIcon="◀"
            rightIcon=""
          ></DropdownItem>

          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/consulta-evaluacion");
            }}
          >
            <DropdownItem leftIcon="🔎">
              <p className="itemMenu">Consulta de Evaluacion</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/administrar-fechas");
            }}
          >
            <DropdownItem leftIcon="📆">
              <p className="itemMenu">Administrar Fechas</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            /*             onClick={() => {dispatch(setOpen(false)); window.location.replace('/dashboard/administrar-checklist');}}
             */
          >
            <DropdownItem goToMenu="checklist" leftIcon="📝">
              <p className="itemMenu">Checklist</p>
            </DropdownItem>
          </div>
          {/* <Link to="">
            <DropdownItem leftIcon="🔐">
              <p className="itemMenu">Desbloquear Seguimiento</p>
            </DropdownItem>
          </Link> */}
        </div>
      </CSSTransition>

      {/* CHECKLIST */}

      <CSSTransition
        in={activeMenu === "checklist"}
        unmountOnExit
        timeout={500}
        classNames="menu-secondary"
      >
        <div className="menu">
          <DropdownItem
            goToMenu="tracing"
            leftIcon="◀"
            rightIcon=""
          ></DropdownItem>

          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/crear-preguntas-checklist");
            }}
          >
            <DropdownItem leftIcon="🔎">
              <p className="itemMenu">Crear Preguntas Checklist</p>
            </DropdownItem>
          </div>
          {/* <Link to="/dashboard/administrar-fechas" style={{ textDecoration: "none" }}>
            <DropdownItem leftIcon="📆">
              <p className="itemMenu">2</p>
            </DropdownItem>
          </Link>
          <Link to="/dashboard/administrar-checklist" style={{ textDecoration: "none" }}>
            <DropdownItem goToMenu="checklist" leftIcon="📝">
              <p className="itemMenu">3</p>
            </DropdownItem>
          </Link> */}
          {/* <Link to="">
            <DropdownItem leftIcon="🔐">
              <p className="itemMenu">Desbloquear Seguimiento</p>
            </DropdownItem>
          </Link> */}
        </div>
      </CSSTransition>

      {/* CHECKLIST */}

      <CSSTransition
        in={activeMenu === "personal"}
        unmountOnExit
        timeout={500}
        classNames="menu-secondary"
      >
        <div className="menu">
          <DropdownItem
            goToMenu="main"
            leftIcon="◀"
            rightIcon=""
          ></DropdownItem>

          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/empresas");
            }}
          >
            <DropdownItem leftIcon="🏬">
              <p className="itemMenu">Empresas</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/lideres");
            }}
          >
            <DropdownItem leftIcon="👨‍💼">
              <p className="itemMenu">Lideres</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/colaboradores");
            }}
          >
            <DropdownItem leftIcon="👨‍🔧">
              <p className="itemMenu">Colaboradores</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/cargos");
            }}
          >
            <DropdownItem leftIcon="💼">
              <p className="itemMenu">Cargos</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/reporte-relacion");
            }}
          >
            <DropdownItem leftIcon="🤼‍♂️">
              <p className="itemMenu">Relacion de Lideres y Colaboradores</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/notificaciones");
            }}
          >
            <DropdownItem leftIcon="🛎">
              <p className="itemMenu">Notificaciones</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/credenciales-colaboradores");
            }}
          >
            <DropdownItem leftIcon="🔑">
              <p className="itemMenu">Credenciales Colaboradores</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/credenciales-lideres");
            }}
          >
            <DropdownItem leftIcon="🔑">
              <p className="itemMenu">Credenciales Lideres</p>
            </DropdownItem>
          </div>
        </div>
      </CSSTransition>

      <CSSTransition
        in={activeMenu === "resultado"}
        unmountOnExit
        timeout={500}
        classNames="menu-secondary"
      >
        <div className="menu">
          <DropdownItem
            goToMenu="main"
            leftIcon="◀"
            rightIcon=""
          ></DropdownItem>

          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/reporte-checklist");
            }}
          >
            <DropdownItem leftIcon="  📣">
              <p className="itemMenu">Reporte de Check List</p>
            </DropdownItem>
          </div>

          

          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/reporte-plan-accion");
            }}
          >
            <DropdownItem leftIcon="📣">
              <p className="itemMenu">Reporte Plan Individual</p>
            </DropdownItem>
          </div>

          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/reporte-tablero");
            }}
          >
            <DropdownItem leftIcon="📣">
              <p className="itemMenu">Reporte Tablero Conversacion de Valor</p>
            </DropdownItem>
          </div>





        </div>



      </CSSTransition>

      <CSSTransition
        in={activeMenu === "board"}
        unmountOnExit
        timeout={500}
        classNames="menu-secondary"
      >
        <div className="menu">
          <DropdownItem
            goToMenu="main"
            leftIcon="◀"
            rightIcon=""
          ></DropdownItem>

          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/Board/LoDulce");
            }}
          >
            <DropdownItem leftIcon="  📣">
              <p className="itemMenu">Lo Dulce</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/Board/LoAmargo");
            }}
          >
            <DropdownItem leftIcon="📣">
              <p className="itemMenu">Exploremos lo amargo</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/Board/Adiciones");
            }}
          >
            <DropdownItem leftIcon="📣">
              <p className="itemMenu">Adiciones</p>
            </DropdownItem>
          </div>
          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/Board/Reconocemos");
            }}
          >
            <DropdownItem leftIcon="📣">
              <p className="itemMenu">Reconocemos</p>
            </DropdownItem>
          </div>
        </div>
      </CSSTransition>

      <CSSTransition
        in={activeMenu === "settings"}
        unmountOnExit
        timeout={500}
        classNames="menu-secondary"
      >
        <div className="menu">
          <DropdownItem
            goToMenu="main"
            leftIcon="◀"
            rightIcon=""
          ></DropdownItem>

          <div
            style={{ textDecoration: "none" }}
            onClick={() => {
              dispatch(setOpen(false));
              window.location.replace("/dashboard/settings/policy&terms");
            }}
          >
            <DropdownItem leftIcon="📜">
              <p className="itemMenu">Aviso de tratamiento de datos</p>
            </DropdownItem>
          </div>

          
          
          
        </div>
      </CSSTransition>





    </div>
  );
}

export default withRouter(Dinamic);

import React, {useEffect, useState} from "react";
import "./styleForget.css";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { getEmailUser } from "./sendEmailService";
import Swal from 'sweetalert2';

const ForgetPassword = (props) => {


  const [state, setState] = useState('')

  const handleOnChange = e => {
    setState(e);
    console.log('event => ', e);
  }

  const sendPassword = () => {

    if (state !== ''){

      getEmailUser(state)
      Swal.fire(
        '',
        'Hemos enviado un correo electrónico con la informacion de acceso',
        'success'
      )
  
      setTimeout(() => {
        props.history.push('/')
      }, 3000);
    } else{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Debes ingresar un email válido',
        
      })
    }


  };


  return (
    <div className="backgroundImgForget">
      <div className="containerForget">
        <div className="credenciales">
          <img
            src={require("../../assets/1x/titleLogin.png")}
            className="titleLogin"
          ></img>
          <div className="formularioForget">
            <Form>
              <Form.Group className="formGroup">
                <img
                  src={require("../../assets/1x/iconoUsuario.png")}
                  className="icon"
                ></img>
                <Form.Control onChange={(e) => handleOnChange(e.target.value)} type="email" id="label" placeholder="Ingresa tu correo electrónico" />
                
              </Form.Group>
              
                <Button onClick={() => sendPassword()} className="btnEnviar">Enviar</Button>
              
            </Form>
          </div>
        </div>
        <div className="logo2">
          <img src={require("../../assets/1x/logos.png")} className="logos" />
        </div>
      </div>
    </div>
  );
};

export default ForgetPassword;

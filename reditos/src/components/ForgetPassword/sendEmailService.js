import axios from "axios";
import { url, body } from "../AdminCategorias/Services/auth";

export const getEmailUser = async (email) => {
  try {
    const respToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await respToken.data.token;

    const dataUser = await axios.get(
      `${url.urlBase}/accessLogin/?email=${email}`,
      {
        headers: { Authorization: `JWT ${token}` },
      }
    );

    console.log("user Email => ", dataUser.data.results[0].email);
    console.log("user Password => ", dataUser.data.results[0].password);

    const emailUser = dataUser.data.results[0].email;
    const password = dataUser.data.results[0].password;

    let sendObj = {
      mail: emailUser,
      pass: password,
      type: 2,
      emails: [emailUser],
    };

    sendPasswordToEmail(sendObj);
  } catch (err) {
    console.log("Error getEmailUser => ", err.code, ": ", err.message);
  }
};

export const sendPasswordToEmail = async (obj) => {
  try {
    await axios.post("https://reditos.multimedialab.dev/Project/s1ils/", obj, {
      headers: { "Content-Type": "application/json" },
    });
  } catch (err) {
    console.log("Error sendPasswordToEmail => ", err.code, ": ", err.message);
  }
};

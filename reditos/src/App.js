import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  BrowserRouter,
} from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

import Login from "./components/Login/index";
import ForgetPassword from "./components/ForgetPassword/index";
import Dashboard from "./components/Dashboard";
import AdministrarCategorias from "./components/AdminCategorias/Categorias/AdministrarCategorias";
import Layout from "./components/Menu/Layout";
import ConfigurarCategorias from "./components/ConfigurarCategorias/ConfigurarCategorias";
import AsignarCategorias from "./components/AdminCategorias/Categorias/AsignarCategorias";
import ConsultaEvaluacion from "./components/AdminCategorias/Categorias/ConsultaEvaluacion";
import AdministrarCheckList from "./components/AdminCategorias/Categorias/AdministrarCheckList";
import AdministrarFechas from "./components/AdminCategorias/Categorias/AdministrarFechas";
import DesbloquearSeguimiento from "./components/AdminCategorias/Categorias/DesbloquearSeguimiento";
import ReporteRelacion from "./components/AdminCategorias/Categorias/ReporteRelacion";
import ReporteCheckList from "./components/AdminCategorias/Categorias/ReporteCheckList";
import SwipeableTemporaryDrawer from "./components/Drawer/DrawerMenu";
import ProtectedRoute from "./components/ProtectedRoute";
import Empresas from "./components/AdminCategorias/Categorias/Empresas";
import Colaboradores from "./components/AdminCategorias/Categorias/Colaboradores";
import Lideres from "./components/AdminCategorias/Categorias/Lideres";
import Jefes from "./components/AdminCategorias/Categorias/Jefes";
import ConfigurarSubcategorias from "./components/AdminCategorias/Categorias/ConfigurarSubcategorias";
import ConfigurarObjetivos from "./components/AdminCategorias/Categorias/ConfigurarObjetivos";
import CompetenciasOrganizacionales from "./components/AdminCategorias/Categorias/CompetenciasOrganizacionales";
import CompetenciasEspecificas from "./components/AdminCategorias/Categorias/CompetenciasEspecificas";
import CompOrganizPreguntas from "./components/AdminCategorias/Categorias/CompOrganizPreguntas";
import CompEspecPreguntas from "./components/AdminCategorias/Categorias/CompEspecPreguntas";
import Teletrabajo from "./components/AdminCategorias/Categorias/Teletrabajo";
import PreguntasTeletrabajo from "./components/AdminCategorias/Categorias/PreguntasTeletrabajo";
import Objetivos from "./components/AdminCategorias/Categorias/ConfigurarObjetivos";
import Questions from "./components/AdminCategorias/Categorias/Questions";
import ViewQuestions from "./components/AdminCategorias/Categorias/ViewQuestions";
import ObjetivosTacticos from "./components/AdminCategorias/Categorias/ObjetivosTacticos";
import ViewCRUD from "./components/AdminCategorias/Categorias/ViewCRUD";
import Cargos from "./components/AdminCategorias/Categorias/Cargos";
import CredencialesLideres from "./components/AdminCategorias/Categorias/CredencialesLideres";
import CredencialesColaboradores from "./components/AdminCategorias/Categorias/CredencialesColaboradores";
import CredencialesAdministrador from "./components/AdminCategorias/Categorias/CredencialesAdministrador";
import Notificaciones from "./components/AdminCategorias/Categorias/Notificaciones";
import LoDulce from "./components/Board/LoDulce";
import LoAmargo from "./components/Board/LoAmargo";
import Adiciones from "./components/Board/Adiciones";
import Reconocemos from "./components/Board/Reconocemos";
import ReportePlanAccion from "./components/AdminCategorias/Categorias/ReportePlanAccion";
import Welcome from "./components/Welcome/Welcome";
import PolicyTerms from "./components/AdminCategorias/Categorias/PolicyTerms";
import ReporteTablero from "./components/AdminCategorias/Categorias/ReporteTablero";

function App() {
  return (
    <BrowserRouter>
      <Router basename={"/admin"}>
        <Switch>
          <Route exact path="/" component={Login} />
          <ProtectedRoute
            exact
            path="/dashboard/administrar-categorias"
            component={AdministrarCategorias}
          />
          <ProtectedRoute
            exact
            path="/dashboard/configurar-categorias"
            component={ConfigurarCategorias}
          />
          <ProtectedRoute
            exact
            path="/dashboard/configurar-subcategorias"
            component={ConfigurarSubcategorias}
          />
          <ProtectedRoute
            exact
            path="/dashboard/asignar-categorias"
            component={AsignarCategorias}
          />
          <ProtectedRoute
            exact
            path="/dashboard/consulta-evaluacion"
            component={ConsultaEvaluacion}
          />

          <ProtectedRoute
            exact
            path="/dashboard/administrar-checklist"
            component={AdministrarCheckList}
          />

          <ProtectedRoute
            exact
            path="/dashboard/ver-preguntas-checklist"
            component={ViewQuestions}
          />

          <ProtectedRoute
            exact
            path="/dashboard/crear-preguntas-checklist"
            component={Questions}
          />

          <ProtectedRoute
            exact
            path="/dashboard/administrar-fechas"
            component={AdministrarFechas}
          />
          <ProtectedRoute
            exact
            path="/dashboard/desbloquear-seguimiento"
            component={DesbloquearSeguimiento}
          />
          <ProtectedRoute
            exact
            path="/dashboard/reporte-relacion"
            component={ReporteRelacion}
          />
          <ProtectedRoute
            exact
            path="/dashboard/reporte-checklist"
            component={ReporteCheckList}
          />

          <ProtectedRoute
            exact
            path="/dashboard/reporte-plan-accion"
            component={ReportePlanAccion}
          />

          <ProtectedRoute
            exact
            path="/dashboard/objetivos"
            component={Objetivos}
          />

          <ProtectedRoute
            exact
            path="/dashboard/objetivos-tacticos"
            component={ObjetivosTacticos}
          />

          <ProtectedRoute
            exact
            path="/dashboard/drawer"
            component={SwipeableTemporaryDrawer}
          />
          <ProtectedRoute
            exact
            path="/dashboard/empresas"
            component={Empresas}
          />
          <ProtectedRoute
            exact
            path="/dashboard/colaboradores"
            component={Colaboradores}
          />
          <ProtectedRoute exact path="/dashboard/lideres" component={Lideres} />

          <ProtectedRoute exact path="/dashboard/jefes" component={Jefes} />

          <ProtectedRoute
            exact
            path="/dashboard/competencias-organizacionales"
            component={CompetenciasOrganizacionales}
          />

          <ProtectedRoute
            exact
            path="/dashboard/competencias-organizacionales-preguntas"
            component={CompOrganizPreguntas}
          />

          <ProtectedRoute
            exact
            path="/dashboard/competencias-especificas"
            component={CompetenciasEspecificas}
          />

          <ProtectedRoute
            exact
            path="/dashboard/competencias-especificas-preguntas"
            component={CompEspecPreguntas}
          />

          <ProtectedRoute
            exact
            path="/dashboard/teletrabajo"
            component={Teletrabajo}
          />

          <ProtectedRoute
            exact
            path="/dashboard/preguntas-teletrabajo"
            component={PreguntasTeletrabajo}
          />

          <ProtectedRoute
            exact
            path="/dashboard/ViewCRUD"
            component={ViewCRUD}
          />

          <ProtectedRoute exact path="/dashboard/cargos" component={Cargos} />

          <ProtectedRoute
            exact
            path="/dashboard/credenciales-lideres"
            component={CredencialesLideres}
          />

          <ProtectedRoute
            exact
            path="/dashboard/credenciales-colaboradores"
            component={CredencialesColaboradores}
          />

          <ProtectedRoute
            exact
            path="/dashboard/credenciales-colaboradores"
            component={CredencialesColaboradores}
          />

          <ProtectedRoute
            exact
            path="/dashboard/credenciales-administrador"
            component={CredencialesAdministrador}
          />

          <ProtectedRoute
            exact
            path="/dashboard/notificaciones"
            component={Notificaciones}
          />

          <ProtectedRoute
            exact
            path="/dashboard/Board/LoDulce"
            component={LoDulce}
          />

          <ProtectedRoute
            exact
            path="/dashboard/Board/LoAmargo"
            component={LoAmargo}
          />

          <ProtectedRoute
            exact
            path="/dashboard/Board/Adiciones"
            component={Adiciones}
          />

          <ProtectedRoute
            exact
            path="/dashboard/Board/Reconocemos"
            component={Reconocemos}
          />

          <ProtectedRoute
            exact
            path="/dashboard/drawer"
            component={SwipeableTemporaryDrawer}
          />

          <ProtectedRoute
            exact
            path="/dashboard/welcome"
            component={Welcome}
          />

          <ProtectedRoute
            exact
            path="/dashboard/settings/policy&terms"
            component={PolicyTerms}
          />

          <ProtectedRoute
            exact
            path="/dashboard/reporte-tablero"
            component={ReporteTablero}
          />







          <Route exact path="/forget" component={ForgetPassword} />
        </Switch>
      </Router>
    </BrowserRouter>
  );
}

export default App;

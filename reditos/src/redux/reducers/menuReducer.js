import { SET_CATEGORIES, SET_CATEGORY_NAME, SET_CATEGORY_URL, SET_ID_CATEGORY, SET_MENU_OPEN } from "../constants/actions-types";

const getInitialState = () => ({
    openRedux: false,
    categories: [],
    categoryUrl: '',
    categoryname: '',
    idCategory: '',
});

const menuReducer = (state = getInitialState(), action) => {
    switch (action.type){
        case SET_MENU_OPEN:
            return { ...state, openRedux: action.openRedux };
        case SET_CATEGORIES:
            return {...state, categories: action.categories};
        case SET_CATEGORY_URL:
            return {...state, categoryUrl: action.categoryUrl};
        case SET_CATEGORY_NAME:
            return {...state, categoryname: action.categoryname};
        case SET_ID_CATEGORY:
            return {...state, idCategory: action.idCategory};
        default:
            return state;
    }
}
export default menuReducer;
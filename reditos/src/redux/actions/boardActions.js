import axios from "axios";
import {body, url} from '../../components/AdminCategorias/Services/auth';
import { SET_BOARD_COLLABORATORS } from "../constants/actions-types";

export const setBoardCollaborators = (boardCollaborators) => ({
  type: SET_BOARD_COLLABORATORS,
  boardCollaborators,
});

export const searchColaborator = (
    year,
    period,
    idLeader,
    checked,
    searchName
  ) => {
    return async (dispatch) => {
      try {
        const responseToken = await axios.post(url.urlToken, body, {
          headers: { "Content-Type": "application/json" },
        });
  
        const token = await responseToken.data.token;
  
        const resp = await axios.get(
          `${url.urlBase}/requiredBoard/?year=${year}&period=${period}&leader=${idLeader}&check=${checked}&search=${searchName}`,
          {
            headers: { Authorization: `JWT ${token}` },
          }
        );
        console.log(resp);
        if (resp.data.count > 0) {
          console.log(resp.data.results);
          dispatch(setBoardCollaborators(resp.data.results));
          return resp.data.results;
        } else {
          dispatch(setBoardCollaborators([]));
          return [];
        }
      } catch (error) {
        /* alert('Error in boardActions => getCollaborators', error.message); */
        console.log("Error in boardActions => getCollaborators", error.message);
        /* throw Error(error); */
      }
    };
};

export const getBoardCollaborators = (year, period, idLeader, checked) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
      const token = await responseToken.data.token;
      const resp = await axios.get(
        `${url.urlBase}/requiredBoard/?year=${year}&period=${period}&leader=${idLeader}&check=${checked}`,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
      console.log(resp);
      if (resp.data.count > 0) {
        /* filter(x => x.id_leader === JSON.parse(atob(sessionStorage.getItem('uld')))) */
        console.log(resp.data.results);
        dispatch(setBoardCollaborators(resp.data.results));
        return resp.data.results;
      } else {
        dispatch(setBoardCollaborators([]));
        return [];
      }
    } catch (error) {
      /* alert('Error in boardActions => getCollaborators', error.message); */
      console.log("Error in boardActions => getCollaborators", error.message);
      /* throw Error(error); */
    }
  };
};

export const getBoardColUnlimited= (year, idLeader, checked) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
      const token = await responseToken.data.token;
      const resp = await axios.get(
        `${url.urlBase}/boards/?year=${year}&leader=${idLeader}&check=${checked}`,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
      console.log('resp unlimited',resp);
      if (resp.data.count > 0) {
        /* filter(x => x.id_leader === JSON.parse(atob(sessionStorage.getItem('uld')))) */
        console.log(resp.data.results);
        dispatch(setBoardCollaborators(resp.data.results));
        return resp.data.results;
      } else {
        dispatch(setBoardCollaborators([]));
        return [];
      }
    } catch (error) {
      /* alert('Error in boardActions => getCollaborators', error.message); */
      console.log("Error in boardActions => getCollaborators", error.message);
      /* throw Error(error); */
    }
  };
};
import { SET_CATEGORIES, SET_CATEGORY_NAME, SET_CATEGORY_URL, SET_ID_CATEGORY, SET_MENU_OPEN } from "../constants/actions-types";


export const setOpen = openRedux => ({
    type: SET_MENU_OPEN,
    openRedux,
});

export const setCategories = categories => ({
    type: SET_CATEGORIES,
    categories,
});

export const setCategoryUrl = categoryUrl => ({
    type: SET_CATEGORY_URL,
    categoryUrl,
});

export const setCategoryName = categoryname => ({
    type: SET_CATEGORY_NAME,
    categoryname,
});

export const setIdCategory = idCategory => ({
    type: SET_ID_CATEGORY,
    idCategory,
});